# Update the list of products
apt update

#Install wget
apt install wget

# Download the Microsoft repository GPG keys
wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb

# Register the Microsoft repository GPG keys
dpkg -i packages-microsoft-prod.deb

apt update

# Install PowerShell
apt install -y powershell
