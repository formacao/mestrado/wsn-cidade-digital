﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreInfrastructure.Definitions
{
    public class AlertDefinitions : IEntityTypeConfiguration<AlertModel>
    {
        public void Configure(EntityTypeBuilder<AlertModel> builder)
        {
            builder.HasKey(a => a.Id);
            builder
                .Property(a => a.MonitoringId)
                .IsRequired();

            builder
                .Property(a => a.AlertTypeId)
                .IsRequired();

        }
    }
}
