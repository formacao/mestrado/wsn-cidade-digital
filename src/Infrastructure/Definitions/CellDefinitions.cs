﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreInfrastructure.Definitions
{
    public class CellDefinitions : IEntityTypeConfiguration<CellModel>
    {
        public void Configure(EntityTypeBuilder<CellModel> builder)
        {
            builder.HasKey(c => c.Id);
            builder
                .Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(200);
            builder
                .Property(c => c.Address)
                .IsRequired()
                .HasMaxLength(300);
            builder
                .Property(c => c.Latitude)
                .IsRequired();
            builder
                .Property(c => c.Longitude)
                .IsRequired();
        }
    }
}
