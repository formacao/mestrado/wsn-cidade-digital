﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreInfrastructure.Definitions
{
    class MonitoringScheduleDefinitions : IEntityTypeConfiguration<MonitoringScheduleModel>
    {
        public void Configure(EntityTypeBuilder<MonitoringScheduleModel> builder)
        {
            builder.HasKey(ms => ms.Id);
            builder
                .Property(ms => ms.MonitoringDateTime)
                .IsRequired();
        }
    }
}
