﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreInfrastructure.Definitions
{
    public class AlertTypeDefinitions : IEntityTypeConfiguration<AlertTypeModel>
    {
        public void Configure(EntityTypeBuilder<AlertTypeModel> builder)
        {
            builder.HasKey(at => at.Id);
            builder
                .Property(at => at.Name)
                .IsRequired()
                .HasMaxLength(200);
        }
    }
}
