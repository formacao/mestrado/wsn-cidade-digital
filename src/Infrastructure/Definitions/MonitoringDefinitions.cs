﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreInfrastructure.Definitions
{
    public class MonitoringDefinitions : IEntityTypeConfiguration<MonitoringModel>
    {
        public void Configure(EntityTypeBuilder<MonitoringModel> builder)
        {
            builder.HasKey(m => m.Id);

            builder
                .Property(m => m.MonitoringScheduleId)
                .IsRequired();

            builder
                .Property(m => m.CellId)
                .IsRequired();

            builder
                .Property(m => m.Temperature)
                .IsRequired();

            builder
                .Property(m => m.Humidity)
                .IsRequired();

            builder
                .Property(m => m.Fire)
                .IsRequired();

            builder
                .Property(m => m.Smoke)
                .IsRequired();

            builder
                .Property(m => m.UvRadiation)
                .IsRequired();

            builder
                .Property(m => m.ToxicGas)
                .IsRequired();
        }
    }
}
