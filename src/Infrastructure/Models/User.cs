﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace EFCoreInfrastructure.Models
{
    public class User : IdentityUser
    {
        [Required, MaxLength(200)]
        public string Name { get; set; }
    }
}
