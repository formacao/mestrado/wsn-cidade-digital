﻿using Domain.Models;
using EFCoreInfrastructure.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using EFCoreInfrastructure.Extensions;
using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.Singletons;

namespace EFCoreInfrastructure
{

    public class Context : IdentityDbContext<User>, IUnitOfWork
    {
        public DbSet<AlertModel> Alerts { get; set; }
        public DbSet<AlertTypeModel> AlertTypes { get; set; }
        public DbSet<CellModel> Cells { get; set; }
        public DbSet<MonitoringModel> MonitoringData { get; set; }
        public DbSet<MonitoringScheduleModel> MonitoringSchedules { get; set; }

        public Context()
        {
        }

        public Context(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured) return;

            var environment = new DebugEnvironmentSingleton();
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlServer(environment.ConnectionString);
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder
                .AddModelConfigurations()
                .AddAlertTypes();
        }
    }
}
