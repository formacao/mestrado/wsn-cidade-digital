﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCoreInfrastructure.Migrations
{
    public partial class DeleteColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "MonitoringSchedules",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "MonitoringData",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "Cells",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "AlertTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "Alerts",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "MonitoringSchedules");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "MonitoringData");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "AlertTypes");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "Alerts");
        }
    }
}
