﻿using Domain.Enums;
using Domain.Models;
using EFCoreInfrastructure.Definitions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCoreInfrastructure.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder AddAlertTypes(this ModelBuilder builder)
        {
            builder.Entity<AlertTypeModel>().HasData
                (
                    new AlertTypeModel { Id = (int)AlertTypesEnum.HighTemperature, Name = "Temperatura Elevada" },
                    new AlertTypeModel { Id = (int)AlertTypesEnum.LowHumidity, Name = "Baixa Umidade" },
                    new AlertTypeModel { Id = (int)AlertTypesEnum.FireDetected, Name = "Fogo detectado" },
                    new AlertTypeModel { Id = (int)AlertTypesEnum.HighUvRadiation, Name = "Radiação UV Elevada" },
                    new AlertTypeModel { Id = (int)AlertTypesEnum.SmokeDetected, Name = "Fumaça Detectada" },
                    new AlertTypeModel { Id = (int)AlertTypesEnum.ToxicGasDetected, Name = "Gases Tóxicos Detectados" }
                );

            return builder;
        }

        public static ModelBuilder AddModelConfigurations(this ModelBuilder builder)
        {
            builder
                .ApplyConfiguration(new AlertDefinitions())
                .ApplyConfiguration(new AlertTypeDefinitions())
                .ApplyConfiguration(new CellDefinitions())
                .ApplyConfiguration(new MonitoringDefinitions())
                .ApplyConfiguration(new MonitoringScheduleDefinitions());

            return builder;
        }
    }
}
