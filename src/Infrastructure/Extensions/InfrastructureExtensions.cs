﻿using EFCoreInfrastructure.Models;
using EFCoreInfrastructure.Repositories;
using EFCoreInfrastructure.Repositories.Interfaces;
using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.Singletons;
using Infrastructure.Domain.Singletons.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace EFCoreInfrastructure.Extensions
{
    public static class InfrastructureExtensions
    {
        public static IServiceCollection AddInfrastructureSingletons(this IServiceCollection services)
        {
#if DEBUG
            services.AddSingleton<IEnvironmentSingleton, DebugEnvironmentSingleton>();
#else
            services.AddSingleton<IEnvironmentSingleton, ProductionEnvironmentSingleton>();
#endif

            return services;
        }

        public static IServiceCollection AddDatabase(this IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();
            var environment = serviceProvider.GetRequiredService<IEnvironmentSingleton>();

            services.AddDbContext<Context>(options => options.UseSqlServer(environment.ConnectionString));

            return services;
        }

        public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork>(sp => sp.GetRequiredService<Context>());

            return services;
        }

        public static IServiceCollection ConfigureDatabase(this IServiceCollection services)
        {

            var serviceProvider = services.BuildServiceProvider();
            var context = serviceProvider.GetRequiredService<Context>();
            var environment = serviceProvider.GetRequiredService<IEnvironmentSingleton>();

            context.Database.EnsureCreated();

            var cellRole = CreateRole(context, RolesSingleton.Cell);
            var adminRole = CreateRole(context, RolesSingleton.Admin);
            CreateUser(context, UsersSingleton.Cell, UsersSingleton.Cell.ToLower(), cellRole, environment.CellPassword);
            CreateUser(context, UsersSingleton.Admin, UsersSingleton.Admin.ToLower(), adminRole, environment.AdminPassword);

            context.SaveChanges();

            services
                .AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<Context>();

            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ICellRepository, CellRepository>();
            services.AddScoped<IMonitoringScheduleRepository, MonitoringScheduleRepository>();
            services.AddScoped<IMonitoringRepository, MonitoringRepository>();
            services.AddScoped<IAlertRepository, AlertRepository>();

            services.AddScoped<ICheckCellExistenceRepository>(sp => sp.GetRequiredService<ICellRepository>());

            return services;
        }

        private static IdentityRole CreateRole(Context context, string roleName)
        {
            var role = context.Roles.Where(r => r.Name == roleName).FirstOrDefault();
            if (role != null) return role;

            var newRole = new IdentityRole
            {
                Name = roleName,
                NormalizedName = roleName.ToLower()
            };

            var roleStore = new RoleStore<IdentityRole>(context);
            roleStore.CreateAsync(newRole).Wait();

            return newRole;
        }

        private static void CreateUser(Context context, string name, string userName, IdentityRole role, string password)
        {
            var user = context.Users.Where(u => u.UserName == userName).FirstOrDefault();
            if (user != null) return;

            var newUser = new User
            {
                Name = name,
                UserName = userName,
                NormalizedUserName = userName,
                EmailConfirmed = true,
                LockoutEnabled = false,
                SecurityStamp = new Guid().ToString()
            };
            newUser.PasswordHash = new PasswordHasher<User>().HashPassword(newUser, password);

            var userStore = new UserStore<User>(context);

            userStore.CreateAsync(newUser).Wait();
            userStore.AddToRoleAsync(newUser, role.NormalizedName).Wait();

        }
    }
}
