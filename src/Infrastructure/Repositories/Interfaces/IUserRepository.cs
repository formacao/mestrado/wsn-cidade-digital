﻿using EFCoreInfrastructure.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EFCoreInfrastructure.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<(User, IList<string>)> AuthenticateAsync(string username, string password, CancellationToken cancellationToken = default);
        //Task<bool> SignUpAsync(IUser user, string password, CancellationToken cancellationToken = default);
        //Task<IUser> GetByUsernameAsync(string username, CancellationToken cancellationToken = default);

    }
}
