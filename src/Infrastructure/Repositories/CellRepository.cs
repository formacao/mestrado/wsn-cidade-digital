﻿using Domain.Models;
using Infrastructure.Domain.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreInfrastructure.Repositories
{
    public class CellRepository : ICellRepository
    {
        private readonly Context _context;

        public IUnitOfWork UnitOfWork { get; }

        public CellRepository(Context context, IUnitOfWork unitOfWork)
        {
            _context = context;
            UnitOfWork = unitOfWork;
        }

        public CellModel Add(CellModel entity)
        {
            _context.Cells.Add(entity);
            return entity;
        }

        public CellModel Delete(CellModel entity)
        {
            entity.Deleted = true;
            return Update(entity);
        }

        public async Task<IEnumerable<CellModel>> GetAsync() => await _context.Cells.AsNoTracking().Where(c => !c.Deleted).OrderBy(c => c.Id).ToArrayAsync();

        public async Task<CellModel> GetAsync(int id) => await _context.Cells.AsNoTracking().Where(c => c.Id == id && !c.Deleted).FirstOrDefaultAsync();

        public CellModel Update(CellModel entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public async Task<bool> CheckExistence(int id) => await GetAsync(id) != null;
    }
}
