﻿using Domain.Models;
using Infrastructure.Domain.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreInfrastructure.Repositories
{
    public class MonitoringRepository : IMonitoringRepository
    {
        private readonly Context _context;

        public IUnitOfWork UnitOfWork { get; }

        public MonitoringRepository(Context context, IUnitOfWork unitOfWork)
        {
            _context = context;
            UnitOfWork = unitOfWork;
        }

        public MonitoringModel Add(MonitoringModel model)
        {
            var existingMonitoring = _context.MonitoringData.AsNoTracking().Where(m => m.MonitoringScheduleId == model.MonitoringScheduleId && m.CellId == model.CellId).FirstOrDefault();
            if (existingMonitoring != null) 
                return null;

            _context.MonitoringData.Add(model);

            return model;

        }

        public async Task<IEnumerable<MonitoringModel>> GetAsync() => await _context.MonitoringData.AsNoTracking().ToArrayAsync();

        public async Task<MonitoringModel> GetAsync(int id) => await _context.MonitoringData.AsNoTracking().Where(m => m.Id == id).FirstOrDefaultAsync();
    }
}
