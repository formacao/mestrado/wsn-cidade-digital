﻿using Domain.Models;
using Infrastructure.Domain.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreInfrastructure.Repositories
{
    public class MonitoringScheduleRepository : IMonitoringScheduleRepository
    {
        private readonly Context _context;

        public IUnitOfWork UnitOfWork { get; }

        public MonitoringScheduleRepository(Context context, IUnitOfWork unitOfWork)
        {
            _context = context;
            UnitOfWork = unitOfWork;
        }

        public MonitoringScheduleModel Add(MonitoringScheduleModel model)
        {
            _context.MonitoringSchedules.Add(model);
            return model;
        }

        public async Task<bool> CheckExistenceAsync(int id) => await _context.MonitoringSchedules.Where(ms => ms.Id == id).CountAsync() == 1;

        public async Task<IEnumerable<MonitoringScheduleModel>> GetAsync() 
            => await _context
                        .MonitoringSchedules
                        .AsNoTracking()
                        .OrderByDescending(s => s.MonitoringDateTime)
                        .ToArrayAsync();

        public async Task<MonitoringScheduleModel> GetAsync(int id) => await _context.MonitoringSchedules
            .AsNoTracking()
            .Where(ms => ms.Id == id)
            .Include(ms => ms.Monitorings)
            .FirstAsync();

        public async Task<MonitoringScheduleModel> GetLastAsync() => await _context.MonitoringSchedules
            .AsNoTracking()
            .Include(ms => ms.Monitorings)
            .OrderByDescending(ms => ms.Id)
            .FirstAsync(); 
    }
}
