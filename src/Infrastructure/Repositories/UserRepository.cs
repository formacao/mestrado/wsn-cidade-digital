﻿using EFCoreInfrastructure.Models;
using EFCoreInfrastructure.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EFCoreInfrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;

        public UserRepository(SignInManager<User> signInManager, UserManager<User> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public async Task<(User, IList<string>)> AuthenticateAsync(string username, string password, CancellationToken cancellationToken = default)
        {
            var user = await _userManager.Users.Where(u => u.NormalizedUserName == username).FirstOrDefaultAsync();
            if (user == null) return (null, null);

            var result = await _signInManager.PasswordSignInAsync(user, password, false, false);
            if (!result.Succeeded) return (null, null);

            var roles = await _userManager.GetRolesAsync(user);

            return (user, roles);
        }


        //public async Task<IUser> GetByUsernameAsync(string username, CancellationToken cancellationToken = default) => await _userManager.Users.Where(u => u.UserName == username).FirstOrDefaultAsync(cancellationToken);

        //public async Task<bool> SignUpAsync(IUser user, string password, CancellationToken cancellationToken = default)
        //{
        //    var userObject = user as User;
        //    var result = await _userManager.CreateAsync(userObject, password);

        //    return result.Succeeded;
        //}
    }
}
