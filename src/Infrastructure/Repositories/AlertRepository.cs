﻿using Domain.Enums;
using Domain.Models;
using Domain.Singletons;
using Infrastructure.Domain.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EFCoreInfrastructure.Repositories
{
    public class AlertRepository : IAlertRepository
    {
        private readonly Context _context;

        public IUnitOfWork UnitOfWork { get;}

        public AlertRepository(Context context, IUnitOfWork unitOfWork)
        {
            _context = context;
            UnitOfWork = unitOfWork;
        }

        public IEnumerable<AlertTypesEnum> Add(MonitoringModel model)
        {
            var alerts = CheckAlerts(model);
            if (alerts.Count() > 0)
            {
                foreach (var alert in alerts)
                {
                    var alertToInsert = new AlertModel
                    {
                        AlertTypeId = (int)alert,
                        Monitoring = model
                    };

                    _context.Alerts.Add(alertToInsert);
                }
            }

            return alerts;
        }

        private IEnumerable<AlertTypesEnum> CheckAlerts(MonitoringModel model)
        {
            var alerts = new List<AlertTypesEnum>();

            if (model.Temperature >= AlertLimitsSingleton.MaxTemperature) alerts.Add(AlertTypesEnum.HighTemperature);
            if (model.Humidity <= AlertLimitsSingleton.MinHumidity) alerts.Add(AlertTypesEnum.LowHumidity);
            if (model.UvRadiation >= AlertLimitsSingleton.MaxUvRadiation) alerts.Add(AlertTypesEnum.HighUvRadiation);
            if (model.Fire >= AlertLimitsSingleton.MaxFire) alerts.Add(AlertTypesEnum.FireDetected);
            if (model.Smoke >= AlertLimitsSingleton.MaxSmoke) alerts.Add(AlertTypesEnum.SmokeDetected);
            if (model.ToxicGas >= AlertLimitsSingleton.MaxToxicGas) alerts.Add(AlertTypesEnum.ToxicGasDetected);

            return alerts;
        }
    }
}
