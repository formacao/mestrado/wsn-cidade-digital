using EFCoreInfrastructure;
using EFCoreInfrastructure.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WSN.Cells.Extensions;

namespace WSN.Cells
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureInfrastructure(services);
            ConfigureCellApplication(services);

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public virtual void ConfigureInfrastructure(IServiceCollection services)
        {
            services
                .AddInfrastructureSingletons()
                .AddDatabase()
                .AddUnitOfWork()
                .ConfigureDatabase()
                .AddRepositories();
        }

        public virtual void ConfigureCellApplication(IServiceCollection services)
        {
            services
                .AddCellApplicationSingletons()
                .AddCellApplicationMappers()
                .AddCellApplicationServices()
                .AddCellApplicationAuthenticationConfigurations();
        }
    }
}
