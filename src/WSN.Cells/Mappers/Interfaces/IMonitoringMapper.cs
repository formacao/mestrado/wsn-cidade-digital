﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Cells.RequestModels;
using WSN.Cells.ResponseModels;

namespace WSN.Cells.Mappers.Interfaces
{
    public interface IMonitoringMapper
    {
        MonitoringModel Map(SendMonitoringRequestModel request);
    }
}
