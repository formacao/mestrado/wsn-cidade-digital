﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Cells.Mappers.Interfaces;
using WSN.Cells.RequestModels;

namespace WSN.Cells.Mappers
{
    public class MonitoringMapper : IMonitoringMapper
    {
        public MonitoringModel Map(SendMonitoringRequestModel request) => new MonitoringModel
        {
            CellId = request.CellId,
            MonitoringScheduleId = request.ScheduleId,
            Temperature = request.Temperature,
            Fire = request.Fire,
            Humidity = request.Humidity,
            UvRadiation = request.UvRadiation,
            Smoke = request.Smoke,
            ToxicGas = request.ToxicGas
        };
    }
}
