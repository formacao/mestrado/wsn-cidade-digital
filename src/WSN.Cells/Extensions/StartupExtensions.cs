﻿using EFCoreInfrastructure;
using EFCoreInfrastructure.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using WSN.Cells.Mappers;
using WSN.Cells.Mappers.Interfaces;
using WSN.Cells.Services;
using WSN.Cells.Services.Interfaces;
using WSN.Cells.Singletons;
using WSN.Cells.Singletons.Interfaces;

namespace WSN.Cells.Extensions
{
    public static class StartupExtensions
    {

        public static IServiceCollection AddCellApplicationSingletons(this IServiceCollection services)
        {
            services.AddSingleton<IAuthenticationSingleton, AuthenticationSingleton>();
            return services;
        }

        public static IServiceCollection AddCellApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IMonitoringService, MonitoringService>();

            return services;
        }

        public static IServiceCollection AddCellApplicationMappers(this IServiceCollection services)
        {
            services.AddSingleton<IMonitoringMapper, MonitoringMapper>();

            return services;
        }

        public static IServiceCollection AddCellApplicationAuthenticationConfigurations(this IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();
            var authenticationSingleton = serviceProvider.GetRequiredService<IAuthenticationSingleton>();

            services
                .AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authenticationSingleton.LoginKey)),
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidIssuer = authenticationSingleton.ValidIssuer,
                        ValidAudience = authenticationSingleton.ValidAudience
                    };
                });

            return services;
        }
    }
}
