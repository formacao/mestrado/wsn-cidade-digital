﻿using System.Net;
using System.Threading.Tasks;
using ASPInfrastructure.Filters;
using Infrastructure.Domain.Singletons;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WSN.Cells.RequestModels;
using WSN.Cells.Services.Interfaces;

namespace WSN.Cells.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = RolesSingleton.Cell)]
    public class MonitoringController : ControllerBase
    {

        private readonly IMonitoringService _monitoringService;

        public MonitoringController(IMonitoringService monitoringService)
        {
            _monitoringService = monitoringService;
        }

        [HttpGet, Route("{cellId:int}"), CellMustExist]
        public async Task<IActionResult> Last(int cellId) => Ok(await _monitoringService.GetLastScheduleAsync(cellId));

        [HttpPost, CellMustExist(Order = 1), MonitoringScheduleMustExist(Order = 2)]
        public async Task<IActionResult> SendMonitoring(SendMonitoringRequestModel request)
        {
            if (await _monitoringService.PostAsync(request)) return StatusCode((int)HttpStatusCode.Created);
            return BadRequest();
        }
    }
}
