﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSN.Cells.RequestModels;
using WSN.Cells.Services.Interfaces;

namespace WSN.Cells.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IUserService _userService;

        public LoginController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous, HttpPost]
        public async Task<IActionResult> LoginAsync(LoginRequestModel request)
        {
            var result = await _userService.SignInAsync(request);
            if (result?.Token == null) return NotFound();

            return Ok(result);
        }
    }
}
