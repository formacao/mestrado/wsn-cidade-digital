﻿using Infrastructure.Domain.RequestModels.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace WSN.Cells.RequestModels
{
    public class LoginRequestModel : ILoginRequestModel
    {
        [Required, MaxLength(20), JsonPropertyName("user")]
        public string User { get; set; }

        [Required, MaxLength(20), JsonPropertyName("password")]
        public string Password { get; set; }
    }
}
