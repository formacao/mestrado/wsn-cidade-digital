﻿using Infrastructure.Domain.RequestModels.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace WSN.Cells.RequestModels
{
    public class SendMonitoringRequestModel : IScheduleIdRequestModel, ICellIdRequestModel
    {
        [Required, JsonPropertyName("temperature")]
        public float Temperature { get; set; }

        [Required, JsonPropertyName("humidity")]
        public float Humidity { get; set; }

        [Required, JsonPropertyName("toxicGas")]
        public float ToxicGas { get; set; }

        [Required, JsonPropertyName("smoke")]
        public float Smoke { get; set; }

        [Required, JsonPropertyName("fire"), Range(0, 1)]
        public float Fire { get; set; }

        [Required, JsonPropertyName("uvRadiation")]
        public float UvRadiation { get; set; }

        [Required, JsonPropertyName("cellId")]
        public int CellId { get; set; }

        [Required, JsonPropertyName("scheduleId")]
        public int ScheduleId { get; set; }
    }
}
