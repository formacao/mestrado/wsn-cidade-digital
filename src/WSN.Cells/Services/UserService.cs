﻿using EFCoreInfrastructure.Models;
using EFCoreInfrastructure.Repositories.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WSN.Cells.RequestModels;
using WSN.Cells.ResponseModels;
using WSN.Cells.Services.Interfaces;
using WSN.Cells.Singletons.Interfaces;

namespace WSN.Cells.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IAuthenticationSingleton _authenticationSingleton;

        public UserService(IUserRepository userRepository, IAuthenticationSingleton authenticationSingleton)
        {
            _userRepository = userRepository;
            _authenticationSingleton = authenticationSingleton;
        }

        public async Task<TokenResponseModel> SignInAsync(LoginRequestModel request)
        {
            var result = await _userRepository.AuthenticateAsync(request.User, request.Password);
            var token = CreateToken(result.Item1, result.Item2);

            return new TokenResponseModel { Token = token };
        }

        private string CreateToken(User user, IList<string> roles)
        {
            if (user == null || roles == null) return null;

            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier, user.UserName),
                new Claim(ClaimTypes.Name, user.Name)
            };

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(_authenticationSingleton.LoginKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddHours(_authenticationSingleton.HoursExpiration),
                Issuer = _authenticationSingleton.ValidIssuer,
                Audience = _authenticationSingleton.ValidAudience,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
