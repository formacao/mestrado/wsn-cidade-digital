﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Cells.RequestModels;
using WSN.Cells.ResponseModels;

namespace WSN.Cells.Services.Interfaces
{
    public interface IUserService
    {
        Task<TokenResponseModel> SignInAsync(LoginRequestModel request);
    }
}
