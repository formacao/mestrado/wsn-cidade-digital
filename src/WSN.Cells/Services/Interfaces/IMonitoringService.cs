﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Cells.RequestModels;
using WSN.Cells.ResponseModels;

namespace WSN.Cells.Services.Interfaces
{
    public interface IMonitoringService
    {
        Task<MonitoringScheduleResponseModel> GetLastScheduleAsync(int cellId);
        Task<bool> PostAsync(SendMonitoringRequestModel request);
    }
}
