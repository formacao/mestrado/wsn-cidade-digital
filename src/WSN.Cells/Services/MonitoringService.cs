﻿using Domain.Models;
using Infrastructure.Domain.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Cells.Mappers.Interfaces;
using WSN.Cells.RequestModels;
using WSN.Cells.ResponseModels;
using WSN.Cells.Services.Interfaces;

namespace WSN.Cells.Services
{
    public class MonitoringService : IMonitoringService
    {
        private readonly IMonitoringRepository _monitoringRepository;
        private readonly IMonitoringScheduleRepository _monitoringScheduleRepository;
        private readonly IAlertRepository _alertsRepository;
        private readonly IMonitoringMapper _monitoringMapper;

        public MonitoringService(IMonitoringRepository monitoringRepository, IMonitoringScheduleRepository monitoringScheduleRepository, IAlertRepository alertsRepository, IMonitoringMapper monitoringMapper)
        {
            _monitoringRepository = monitoringRepository;
            _monitoringScheduleRepository = monitoringScheduleRepository;
            _alertsRepository = alertsRepository;
            _monitoringMapper = monitoringMapper;
        }

        public async Task<MonitoringScheduleResponseModel> GetLastScheduleAsync(int cellId)
        {
            var last = await _monitoringScheduleRepository.GetLastAsync();
            bool dataSent = VerifyCellAlreadySentData(last.Monitorings, cellId);

            return new MonitoringScheduleResponseModel
            {
                DataSent = dataSent,
                Id = last.Id
            };
        }

        public async Task<bool> PostAsync(SendMonitoringRequestModel request)
        {
            var schedule = await _monitoringScheduleRepository.GetAsync(request.ScheduleId);
            if (VerifyCellAlreadySentData(schedule.Monitorings, request.CellId)) return false;

            var model = _monitoringMapper.Map(request);

            _monitoringRepository.Add(model);
            var alerts = _alertsRepository.Add(model);
            if (await _monitoringRepository.UnitOfWork.SaveChangesAsync() == (1 + alerts.Count())) return true;

            return false;
        }

        private bool VerifyCellAlreadySentData(IEnumerable<MonitoringModel> monitorings, int cellId)
        {
            if (monitorings == null) return false;
            return monitorings.Where(m => m.CellId == cellId).Count() == 1;
        }
    }
}
