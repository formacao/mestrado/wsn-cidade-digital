﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WSN.Cells.ResponseModels
{
    public class MonitoringScheduleResponseModel
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("dataSent")]
        public bool DataSent { get; set; }
    }
}
