﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WSN.Cells.ResponseModels
{
    public class TokenResponseModel
    {
        [JsonPropertyName("token")]
        public string Token { get; set; }
    }
}
