﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Cells.Singletons.Interfaces;

namespace WSN.Cells.Singletons
{
    public class AuthenticationSingleton : IAuthenticationSingleton
    {
        public string LoginKey => Environment.GetEnvironmentVariable("LoginKey", EnvironmentVariableTarget.Process);
        public string ValidIssuer => Environment.GetEnvironmentVariable("Issuer", EnvironmentVariableTarget.Process);
        public string ValidAudience => Environment.GetEnvironmentVariable("Audience", EnvironmentVariableTarget.Process);
        public int HoursExpiration => int.Parse(Environment.GetEnvironmentVariable("HoursExpiration", EnvironmentVariableTarget.Process));
    }
}
