﻿namespace WSN.Cells.Singletons.Interfaces
{
    public interface IAuthenticationSingleton
    {
        int HoursExpiration { get; }
        string LoginKey { get; }
        string ValidAudience { get; }
        string ValidIssuer { get; }
    }
}