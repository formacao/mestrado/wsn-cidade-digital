﻿using ASPInfrastructure.Exceptions;
using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.RequestModels.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using System.Threading.Tasks;

namespace ASPInfrastructure.Filters
{
    public class CellMustExistFilter : IAsyncActionFilter
    {
        private readonly ICheckCellExistenceRepository _repository;
        private readonly string _viewName;

        public CellMustExistFilter(ICheckCellExistenceRepository repository, string viewName)
        {
            _repository = repository;
            _viewName = viewName;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            int id;
            if(context.RouteData.Values.ContainsKey("cellId"))
            {
                id = int.Parse(context.RouteData.Values["cellId"].ToString());
            }
            else
            {
                if (!(context.ActionArguments["request"] is ICellIdRequestModel bodyObject))
                    throw new NoParameterRequestException();

                id = bodyObject.CellId;
            }

            if(!await _repository.CheckExistence(id))
            {
                if(_viewName == "")
                {
                    context.Result = new ContentResult
                    {
                        StatusCode = (int)HttpStatusCode.NotFound,
                        Content = string.Empty
                    };
                }
                else
                {
                    context.Result = new ViewResult
                    {
                        StatusCode = (int)HttpStatusCode.NotFound,
                        ViewName = _viewName
                    };
                }
            }
            else
            {
                await next();
            }

        }
    }

    public class CellMustExistAttribute : TypeFilterAttribute
    {
        public CellMustExistAttribute(string viewName = "") : base(typeof(CellMustExistFilter))
        {
            Arguments = new object[]
            {
                viewName
            };
        }
    }
}
