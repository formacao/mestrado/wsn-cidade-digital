﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ASPInfrastructure.Filters
{
    public class ModelStateMustBeValidFilter : IAsyncActionFilter
    {
        private readonly string _viewName;

        public ModelStateMustBeValidFilter(string viewName)
        {
            _viewName = viewName;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var modelState = context.ModelState;
            if (!modelState.IsValid)
            {
                var bodyObject = context.ActionArguments["request"];

                var viewData = new ViewDataDictionary(new EmptyModelMetadataProvider(), modelState)
                {
                    Model = bodyObject
                };

                context.Result = new ViewResult
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    ViewName = _viewName,
                    ViewData = viewData
                };
            }
            else
            {
                await next();
            }
        }
    }

    public class ModelStateMustBeValidAttribute : TypeFilterAttribute
    {
        public ModelStateMustBeValidAttribute(string viewName) : base(typeof(ModelStateMustBeValidFilter))
        {

            if (string.IsNullOrEmpty(viewName)) throw new ArgumentNullException(nameof(viewName));

            Arguments = new object[]
            {
                viewName
            };
        }
    }
}
