﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using System.Threading.Tasks;
using ASPInfrastructure.Exceptions;
using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.RequestModels.Interfaces;

namespace ASPInfrastructure.Filters
{
    public class MonitoringScheduleMustExistFilter : IAsyncActionFilter
    {
        private readonly IMonitoringScheduleRepository _monitoringSchedule;

        public MonitoringScheduleMustExistFilter(IMonitoringScheduleRepository monitoringSchedule)
        {
            _monitoringSchedule = monitoringSchedule;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!(context.ActionArguments["request"] is IScheduleIdRequestModel monitoringIds))
                throw new NoParameterRequestException();

            if (await _monitoringSchedule.CheckExistenceAsync(monitoringIds.ScheduleId))
                await next();

            else
                context.Result = new ContentResult
                {
                    StatusCode = (int)HttpStatusCode.NotFound
                };
        }

    }

    public class MonitoringScheduleMustExistAttribute : TypeFilterAttribute
    {
        public MonitoringScheduleMustExistAttribute() : base(typeof(MonitoringScheduleMustExistFilter))
        {
        }
    }
}
