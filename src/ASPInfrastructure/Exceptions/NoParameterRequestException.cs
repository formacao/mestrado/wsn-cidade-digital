﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPInfrastructure.Exceptions
{
    public class NoParameterRequestException : Exception
    {
        private const string _baseMessage = "No id could be found! Remember de name the body model as [request]";
        public NoParameterRequestException() : base(_baseMessage)
        {
        }
    }
}
