﻿using Infrastructure.Domain.Singletons.Interfaces;
using System;

namespace Infrastructure.Domain.Singletons
{
    public class DebugEnvironmentSingleton : IEnvironmentSingleton
    {
        public string SQLServer => Environment.GetEnvironmentVariable("ServidorSQLServer", EnvironmentVariableTarget.User);
        public string SQLUser => Environment.GetEnvironmentVariable("UsuarioSQLServer", EnvironmentVariableTarget.User);
        public string SQLPassword => Environment.GetEnvironmentVariable("SenhaSQLServer", EnvironmentVariableTarget.User);
        public string CellPassword => Environment.GetEnvironmentVariable("CellPassword", EnvironmentVariableTarget.Process);
        public string AdminPassword => Environment.GetEnvironmentVariable("AdminPassword", EnvironmentVariableTarget.Process);

        public string ConnectionString => $"Server={SQLServer};Database=WSN;User Id={SQLUser};Password={SQLPassword};";
    }
}
