﻿using System;
using Infrastructure.Domain.Singletons.Interfaces;

namespace Infrastructure.Domain.Singletons
{
    public class ProductionEnvironmentSingleton : IEnvironmentSingleton
    {
        public string SQLServer => Environment.GetEnvironmentVariable("ServidorSQLServer", EnvironmentVariableTarget.Process);
        public string SQLUser => Environment.GetEnvironmentVariable("UsuarioSQLServer", EnvironmentVariableTarget.Process);
        public string SQLPassword => Environment.GetEnvironmentVariable("SenhaSQLServer", EnvironmentVariableTarget.Process);
        public string CellPassword => Environment.GetEnvironmentVariable("CellPassword", EnvironmentVariableTarget.Process);
        public string AdminPassword => Environment.GetEnvironmentVariable("AdminPassword", EnvironmentVariableTarget.Process);

        public string ConnectionString => $"Server={SQLServer};Database=WSN;User Id={SQLUser};Password={SQLPassword};";
    }
}