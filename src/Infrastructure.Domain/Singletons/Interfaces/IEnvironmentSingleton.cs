﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Domain.Singletons.Interfaces
{
    public interface IEnvironmentSingleton
    {
        string SQLServer { get; }
        string SQLUser { get;}
        string SQLPassword { get; }
        string CellPassword { get; }
        string AdminPassword { get; }
        string ConnectionString { get; }
    }
}
