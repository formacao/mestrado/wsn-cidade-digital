﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Domain.Singletons
{
    public static class RolesSingleton
    {
        public const string Cell = "Cell";
        public const string Admin = "Admin";
    }
}
