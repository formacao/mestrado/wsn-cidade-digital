﻿using Infrastructure.Domain.ResponseModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Domain.Repositories.Interfaces
{
    public interface ICellDataRepository: ICheckCellExistenceRepository
    {
        Task<IEnumerable<SearchCellModel>> GetSearchResultAsync(string search);
        Task<RealTimeMeasurementsModel> GetLastMeasurementAsync(int id);
        Task<IEnumerable<TimeSeriesModel>> GetTimeSeriesAsync(int cellId, DateTime start, DateTime end, string columnName);
        Task<IEnumerable<AverageByDayNightModel>> GetAverageByDayNight(int cellId, DateTime start, DateTime end);
        Task<OverallAverageModel> GetOverallAverage(int cellId, DateTime start, DateTime end);
    }
}
