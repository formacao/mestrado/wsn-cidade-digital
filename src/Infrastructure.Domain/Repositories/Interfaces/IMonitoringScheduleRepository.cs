﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Domain.Repositories.Interfaces
{
    public interface IMonitoringScheduleRepository : IRepositoryWithUnitOfWork
    {
        Task<IEnumerable<MonitoringScheduleModel>> GetAsync();
        Task<MonitoringScheduleModel> GetAsync(int id);
        Task<MonitoringScheduleModel> GetLastAsync();
        Task<bool> CheckExistenceAsync(int id); 
        MonitoringScheduleModel Add(MonitoringScheduleModel model);
    }
}
