﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Domain.Repositories.Interfaces
{
    public interface IMonitoringRepository : IRepositoryWithUnitOfWork
    {

        MonitoringModel Add(MonitoringModel model);
        Task<IEnumerable<MonitoringModel>> GetAsync();
        Task<MonitoringModel> GetAsync(int id);
    }
}
