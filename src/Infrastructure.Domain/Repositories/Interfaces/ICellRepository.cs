﻿using Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Domain.Repositories.Interfaces
{
    public interface ICellRepository : IRepositoryWithUnitOfWork, ICheckCellExistenceRepository
    {

        Task<IEnumerable<CellModel>> GetAsync();
        Task<CellModel> GetAsync(int id);
        CellModel Add(CellModel entity);
        CellModel Update(CellModel entity);
        CellModel Delete(CellModel entity);

    }
}
