﻿using Infrastructure.Domain.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Domain.Repositories.Interfaces
{
    public interface IAlertStatisticsRepository
    {
        Task<IEnumerable<AlertDataModel>> GetOverall();
    }
}
