﻿using Infrastructure.Domain.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Domain.Repositories.Interfaces
{
    public interface ICityStatisticsRepository
    {
        Task<OverallAverageModel> GetOverallAverageAsync();
        Task<IEnumerable<AverageByDayNightModel>> GetAverageByDayNightAsync();
    }
}
