﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Domain.Repositories.Interfaces
{
    public interface ICheckCellExistenceRepository
    {
        Task<bool> CheckExistence(int id);
    }
}
