﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Domain.Repositories.Interfaces
{
    public interface IRepositoryWithUnitOfWork
    {
        public IUnitOfWork UnitOfWork { get; }
    }
}
