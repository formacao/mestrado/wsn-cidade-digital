﻿using Domain.Enums;
using Domain.Models;
using System.Collections.Generic;

namespace Infrastructure.Domain.Repositories.Interfaces
{
    public interface IAlertRepository : IRepositoryWithUnitOfWork
    {
        IEnumerable<AlertTypesEnum> Add(MonitoringModel model);
    }
}
