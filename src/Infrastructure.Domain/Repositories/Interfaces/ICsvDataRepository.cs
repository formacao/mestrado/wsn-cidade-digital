﻿using Infrastructure.Domain.RequestModels;
using Infrastructure.Domain.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Domain.Repositories.Interfaces
{
    public interface ICsvDataRepository
    {
        Task<IEnumerable<CsvRowModel>> GetCsvDataAsync(CsvDataRequestModel request);
    }
}
