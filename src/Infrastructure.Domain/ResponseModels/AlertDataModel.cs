﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infrastructure.Domain.ResponseModels
{
    public class AlertDataModel
    {
        public string AlertType { get; set; }

        public string Address { get; set; }
    }
}
