﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infrastructure.Domain.ResponseModels
{
    public class RealTimeMeasurementsModel
    {
        public float Temperature { get; set; }
        public float Humidity { get; set; }
        public float UvRadiation { get; set; }
        public float Fire { get; set; }
        public float Smoke { get; set; }
        public float ToxicGas { get; set; }

    }
}
