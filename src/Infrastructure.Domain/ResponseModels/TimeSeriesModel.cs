﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infrastructure.Domain.ResponseModels
{
    public class TimeSeriesModel
    {
        public DateTime DateTime { get; set; }
        public float Value { get; set; }

        public TimeSeriesModel ConvertDateTimeFromUtcToLocal()
        {
            DateTime = TimeZoneInfo.ConvertTime(DateTime, TimeZoneInfo.Utc, TimeZoneInfo.Local);

            return this;
        }
    }
}
