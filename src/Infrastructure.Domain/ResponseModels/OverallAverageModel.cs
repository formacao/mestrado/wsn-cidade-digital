﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infrastructure.Domain.ResponseModels
{
    public class OverallAverageModel
    {
        public float AvgTemperature { get; set; }
        public float AvgFire { get; set; }
        public float AvgHumidity { get; set; }
        public float AvgSmoke { get; set; }
        public float AvgToxicGas { get; set; }
        public float AvgUv { get; set; }
    }
}
