﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Domain.ResponseModels
{
    public class CsvRowModel
    {
        public float Temperature  { get; set; }
        public float Fire { get; set; }
        public float Humidity { get; set; }
        public float Smoke { get; set; }
        public float ToxicGas { get; set; }
        public float UvRadiation { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public DateTime MonitoringMoment { get; set; }
    }
}
