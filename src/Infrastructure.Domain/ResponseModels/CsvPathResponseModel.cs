﻿namespace Infrastructure.Domain.ResponseModels
{
    public class CsvPathResponseModel
    {
        public string FileName { get; set; }
    }
}