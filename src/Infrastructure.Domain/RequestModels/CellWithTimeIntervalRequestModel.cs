﻿using Infrastructure.Domain.RequestModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Domain.RequestModels
{
    public class CellWithTimeIntervalRequestModel : ICellIdRequestModel
    {
        public int CellId { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }
    }
}
