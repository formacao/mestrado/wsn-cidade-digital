﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Domain.RequestModels.Interfaces
{
    public interface IScheduleIdRequestModel
    {
        int ScheduleId { get; }
    }
}
