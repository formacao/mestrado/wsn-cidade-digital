﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Domain.RequestModels.Interfaces
{
    public interface ICellIdRequestModel
    {
        int CellId { get; }
    }
}
