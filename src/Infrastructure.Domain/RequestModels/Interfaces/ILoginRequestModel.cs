﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Domain.RequestModels.Interfaces
{
    public interface ILoginRequestModel
    {
        string User { get; }
        string Password { get; }
    }
}
