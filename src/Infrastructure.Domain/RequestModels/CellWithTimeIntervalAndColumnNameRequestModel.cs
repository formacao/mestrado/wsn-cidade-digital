﻿using Infrastructure.Domain.RequestModels.Interfaces;
using System;

namespace Infrastructure.Domain.RequestModels
{
    public class CellWithTimeIntervalAndColumnNameRequestModel : ICellIdRequestModel
    {
        public int CellId { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public string ColumnName { get; set; }
    }
}
