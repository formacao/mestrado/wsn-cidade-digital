﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Domain.RequestModels
{
    public class CsvDataRequestModel
    {
        public IEnumerable<int> Ids { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}
