﻿using Infrastructure.Domain.ResponseModels;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System.Threading.Tasks;
using WSN.User.SPA.Services.Interfaces;

namespace WSN.User.SPA.Components
{
    public partial class RealTimeByCell
    {
        private bool _loadingData = false;
        private bool _noData = false;
        private RealTimeMeasurementsModel _data = null;

        private AutocompleteSearchCell _autocompleteSearchCell;

        [Inject]
        public ICellService CellService { get; set; }

        [Inject]
        public IJSRuntime JavaScript { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (_loadingData)
            {
                _data = await CellService.GetRealTimeMeasurementsAsync(_autocompleteSearchCell.SelectedCell);
                _noData = _data == null;
                _loadingData = false;
                StateHasChanged();
            }
            else
            {
                if (!_noData && _data != null)
                {
                    await JavaScript.InvokeVoidAsync("PlotRealTimeMeasurements", _data);
                }
            }

        }

        private async Task GetCellDataAsync()
        {
            if (_autocompleteSearchCell.SelectedCell <= 0) return;
            _loadingData = true;
            StateHasChanged();
        }
    }
}
