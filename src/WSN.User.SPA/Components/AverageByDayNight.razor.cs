﻿using Infrastructure.Domain.ResponseModels;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.User.SPA.DTOs;
using WSN.User.SPA.Factories.Interfaces;
using WSN.User.SPA.Helpers;
using WSN.User.SPA.Services.Interfaces;

namespace WSN.User.SPA.Components
{
    public partial class AverageByDayNight
    {

        private IEnumerable<AverageByDayNightModel> _dayNight;
        private OverallAverageModel _overall;
        private bool _loadingData = true;

        [Inject] public ICityStatisticsService CityService { get; set; }
        [Inject] public IJSRuntime JavaScript { get; set; }
        [Inject] public IServiceProvider ServiceProvider  { get; set; }

        protected override async Task OnInitializedAsync()
        {
            _dayNight = await CityService.GetByDayNightAsync();
            _overall = await CityService.GetOverallAsync();
            _loadingData = false;
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            var dto = GenerateDTOFromDatabaseData(_dayNight, _overall);
            if(!_loadingData && dto != null)
                await JavaScript.InvokeVoidAsync("GenerateByDayNight", dto);
        }

        private AverageByDayNightDTO GenerateDTOFromDatabaseData(IEnumerable<AverageByDayNightModel> dayNight, OverallAverageModel overall)
        {
            var factory = ServiceProvider.GetRequiredService<IAverageByDayNightDTOFactory>();
            return AverageByDayNightDTOHelper.GenerateDTOWithDivsIds(dayNight, overall, factory, "temperature", "humidity", "smoke", "fire", "uv", "gas");
        }
    }
}
