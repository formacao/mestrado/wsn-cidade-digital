﻿using Infrastructure.Domain.ResponseModels;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.User.SPA.Services.Interfaces;

namespace WSN.User.SPA.Components
{
    public partial class TimeSeriesCell
    {
        private bool _loadingData = false;
        private bool _noData = false;
        private IEnumerable<TimeSeriesModel> _data = null;

        private AutocompleteSearchCell _autocompleteSearchCell;
        private DateTime _startDateTime = DateTime.Now.AddDays(-7);
        private DateTime _endDateTime = DateTime.Now;

        private string _selectedColumn = "Temperature";

        [Inject]
        public IJSRuntime JavaScript { get; set; }

        [Inject]
        public ICellService CellService { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if(_loadingData)
            {
                _data = await CellService.GetTimeSeriesAsync(_autocompleteSearchCell.SelectedCell, _startDateTime, _endDateTime, _selectedColumn);
                _noData = (_data == null) || _data.Count() == 0;
                _loadingData = false;
                StateHasChanged();
            }
            else
            {
                if (!_noData && _data != null)
                    await JavaScript.InvokeVoidAsync("PlotTimeSeries", _data);
            }
        }

        private void GetData()
        {
            if (_autocompleteSearchCell.SelectedCell == 0) return;

            _loadingData = true;
            StateHasChanged();
        }
    }
}
