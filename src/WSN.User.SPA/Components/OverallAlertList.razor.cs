﻿using Infrastructure.Domain.ResponseModels;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Threading.Tasks;
using WSN.User.SPA.Services.Interfaces;

namespace WSN.User.SPA.Components
{
    public partial class OverallAlertList
    {
        private IEnumerable<AlertDataModel> _alerts = new List<AlertDataModel>();
        private bool _loadingData = true;

        [Inject] public IAlertsStatisticsService AlertsService { get; set; }

        protected override async Task OnInitializedAsync()
        {
            _alerts = await AlertsService.GetAlertsAsync();
            _loadingData = false;
        }

    }
}
