﻿using Infrastructure.Domain.ResponseModels;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.User.SPA.DTOs;
using WSN.User.SPA.Factories.Interfaces;
using WSN.User.SPA.Helpers;
using WSN.User.SPA.Services.Interfaces;



namespace WSN.User.SPA.Components
{
    public partial class AverageValueCell
    {
        private AutocompleteSearchCell _autocompleteSearchCell;
        private DateTime _beginDateTime = DateTime.Now.AddDays(-7);
        private DateTime _endDateTime = DateTime.Now;

        private IEnumerable<AverageByDayNightModel> _dayNightData;
        private OverallAverageModel _overallAverageData;

        private bool _firstRun = true;
        private bool _noData;
        private bool _loadingData;

        [Inject]
        public ICellService CellService { get; set; }
        [Inject]
        public IJSRuntime JS { get; set; }
        [Inject]
        public IServiceProvider ServiceProvider { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (_loadingData)
            {
                //Pegar dados
                _dayNightData = await CellService.GetAverageByDayNightAsync(_autocompleteSearchCell.SelectedCell, _beginDateTime, _endDateTime);
                _overallAverageData = await CellService.GetOverallAverageAsync(_autocompleteSearchCell.SelectedCell, _beginDateTime, _endDateTime);
                _noData = _dayNightData?.Count() == 0 || _overallAverageData == null;
                _firstRun = false;
                _loadingData = false;
                StateHasChanged();
            }
            else
            {
                if(!_firstRun)
                {
                    if(!_noData)
                    {
                        var dto = GenerateDTOFromDatabaseData(_dayNightData, _overallAverageData);
                        await JS.InvokeVoidAsync("GenerateByDayNight", dto);
                    }
                }
            }
        }

        private AverageByDayNightDTO GenerateDTOFromDatabaseData(IEnumerable<AverageByDayNightModel> dayNight, OverallAverageModel overall)
        {
            var factory = ServiceProvider.GetRequiredService<IAverageByDayNightDTOFactory>();
            return AverageByDayNightDTOHelper.GenerateDTOWithDivsIds(dayNight, overall, factory, "cellTemperature", "cellHumidity", "cellSmoke", "cellFire", "cellUv", "cellGas");
        }

        private void GetData()
        {
            if (_autocompleteSearchCell.SelectedCell == 0) return;

            _loadingData = true;
            StateHasChanged();
        }


    }
}
