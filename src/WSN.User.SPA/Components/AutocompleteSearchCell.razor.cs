﻿using Infrastructure.Domain.ResponseModels;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.User.SPA.Services.Interfaces;

namespace WSN.User.SPA.Components
{
    public partial class AutocompleteSearchCell
    {
        private IEnumerable<SearchCellModel> _cells = new List<SearchCellModel>();

        public int SelectedCell { get; set; }

        [Inject]
        public ICellService CellService { get; set; }

        public SearchCellModel GetSelectCellObject() => SelectedCell > 0 ? _cells.First(x => x.Id == SelectedCell) : null;
   

        private void CellChanged(object selected)
        {
            SelectedCell = (int)selected;
        }

        private async Task SearchChangedAsync(object search)
        {
            if (search == null) return;
            SelectedCell = 0;

            _cells = await CellService.GetSearchResultAsync(search.ToString());
        }
    }
}
