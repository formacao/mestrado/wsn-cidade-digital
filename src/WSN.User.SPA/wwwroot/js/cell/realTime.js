﻿var alertColor = "rgba(255, 97, 97, 1)";
var okColor = "rgba(97, 255, 121, 1)";
var barColor = "orange";
var thresholdColor = "black";

function PlotRealTimeMeasurements(plotData) {

    PlotTemperatureGauge(plotData.temperature);
    PlotHumidityGauge(plotData.humidity);
    PlotUvGauge(plotData.uvRadiation);

    PlotFireGauge(plotData.fire);
    PlotSmokeGauge(plotData.smoke);
    PlotToxicGauge(plotData.toxicGas);

}

function PlotTemperatureGauge(value) {
    var data = GetData(value, 20, 60, 35);
    PlotGauge(data, "temperature", "Temperatura [°C]");
}

function PlotHumidityGauge(value) {
    var data = GetData(value, 0, 100, 30, true);
    PlotGauge(data, "humidity", "Umidade [%]");
}

function PlotUvGauge(value) {
    var data = GetData(value, 0, 12, 4);
    PlotGauge(data, "uv", "Radiação UV [UVi]");
}

function PlotFireGauge(value) {
    var data = GetData(value, 0, 1, 0.8);
    PlotGauge(data, "fire", "Incêndio");
}

function PlotSmokeGauge(value) {
    var data = GetData(value, 0, 2000, 1000);
    PlotGauge(data, "smoke", "Fumaça [ppm]");
}

function PlotToxicGauge(value) {
    var data = GetData(value, 0, 2000, 1000);
    PlotGauge(data, "gas", "Gases Tóxicos [ppm]");
}

function GetData(value, min, max, threshold, inverted = false) {
    var steps;

    if (inverted) {
        steps = [
            { range: [min, threshold], color: alertColor },
            { range: [threshold, max], color: okColor }
        ]
    }
    else {
        steps = [
            {range: [min, threshold], color: okColor},
            {range: [threshold, max], color: alertColor}
        ]
    }

    var data = [
        {
            type: "indicator",
            mode: "gauge+number",
            value: value,
            gauge: {
                axis: {
                    range: [min, max],
                },
                steps: steps,
                threshold: {
                    line: { color: thresholdColor, width: 4 },
                    thickness: 1,
                    value: threshold
                },
                bar: {
                    color: barColor
                }
            }
        }
    ];

    return data;
}

function PlotGauge(data, divId, title) {

    var layout = {
        title:
        {
            text: title,
            y: 0.9
        },
        margin: {
            t: 20,
            l: 30,
            r: 40,
            b: 0
        }
    };

    var config = {
        responsive: true
    }

    Plotly.newPlot(divId, data, layout, config);
}
