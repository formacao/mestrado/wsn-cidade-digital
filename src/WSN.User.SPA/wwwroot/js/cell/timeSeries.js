﻿function PlotTimeSeries(plotData) {
    var x = []
    var y = []

    plotData.forEach(function (element, index, array) {
        x.push(element.dateTime)
        y.push(element.value)
    });

    var data = [
        {
            type: "scatter",
            mode: "lines",
            x: x,
            y: y
        }
    ];

    var layout = {
        title: "Séries Temporais"
    };

    var config = {
        responsive: true
    };

    Plotly.newPlot("timeseries", data, layout, config);
}