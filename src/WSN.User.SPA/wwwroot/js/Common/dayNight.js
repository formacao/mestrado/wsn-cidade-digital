﻿const nightColor = "rgba(50, 0, 112, 1)";
const dayColor = "rgba(255, 111, 15, 1)";
const overallColor = "rgba(49, 148, 0, 1)";

function GenerateByDayNight(dayNightDto) {

    console.log(dayNightDto);

    if (dayNightDto == null) {
        console.log("Average data not found!")
        return;
    }

    var data = dayNightDto.measurementData;

    var temperatureTrace = GetTrace(data.temperature.dayAverage, data.temperature.nightAverage, data.temperature.overallAverage);
    var humidityTrace = GetTrace(data.humidity.dayAverage, data.humidity.nightAverage, data.humidity.overallAverage);
    var uvTrace = GetTrace(data.uv.dayAverage, data.uv.nightAverage, data.uv.overallAverage);
    var smokeTrace = GetTrace(data.smoke.dayAverage, data.smoke.nightAverage, data.smoke.overallAverage);
    var gasTrace = GetTrace(data.toxicGas.dayAverage, data.toxicGas.nightAverage, data.toxicGas.overallAverage);
    var fireTrace = GetTrace(data.fire.dayAverage, data.fire.nightAverage, data.fire.overallAverage);

    PlotGroupBars(temperatureTrace, data.temperature.divId, "Temperatura", "°C");
    PlotGroupBars(humidityTrace, data.humidity.divId, "Umidade", "%");
    PlotGroupBars(uvTrace, data.uv.divId, "Radiação UV", "UVi");
    PlotGroupBars(smokeTrace, data.smoke.divId, "Fumaça", "ppm");
    PlotGroupBars(gasTrace, data.toxicGas.divId, "Gases Tóxicos", "ppm");
    PlotGroupBars(fireTrace, data.fire.divId, "Incêndio", "%");
}

function GetTrace(dayValue, nightValue, overallValue) {
    return {
        x: ["Dia", "Noite", "Geral"],
        y: [dayValue, nightValue, overallValue],
        marker: {
            color: [dayColor, nightColor, overallColor]
        },
        type: "bar"
    };
}

function PlotGroupBars(trace, divId, title, yAxisTitle) {
    var data = [trace];
    var layout = {
        barmode: "group",
        title: {
            text: title,
            y: 0.9
        },
        yaxis: {
            title: {
                text: yAxisTitle
            }
        },
        margin: {
            l: 50,
            r: 50,
            b: 50,
            t: 50
        },
    };

    var config = {
        responsive: true
    }

    Plotly.newPlot(divId, data, layout, config);
}