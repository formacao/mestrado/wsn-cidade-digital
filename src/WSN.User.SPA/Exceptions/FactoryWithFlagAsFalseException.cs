﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSN.User.SPA.Exceptions
{
    public class FactoryWithFlagAsFalseException : Exception
    {
        private const string TEMPLATE_MESSAGE = "The factory {0} has the flag {1} as false. It need to be true to proceed";
        public FactoryWithFlagAsFalseException(string factoryName, string flagName) : base(string.Format(TEMPLATE_MESSAGE, factoryName, flagName))
        {
        }
    }
}
