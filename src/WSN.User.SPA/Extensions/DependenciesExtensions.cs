﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using WSN.User.SPA.Factories;
using WSN.User.SPA.Factories.Interfaces;
using WSN.User.SPA.Services;
using WSN.User.SPA.Services.Interfaces;
using WSN.User.SPA.Singletons;
using WSN.User.SPA.Singletons.Interfaces;

namespace WSN.User.SPA.Extensions
{
    public static class DependenciesExtensions
    {
        public static IServiceCollection AddDotEnv(this IServiceCollection services)
        {
            #if DEBUG
            services.AddSingleton<IUserSpaEnvironmentSingleton, DebugUserSpaEnvironmentSingleton>();
            #else
            services.AddSingleton<IUserSpaEnvironmentSingleton, ReleaseUserSpaEnvironmentSingleton>();
            #endif
            
            return services;
        }

        public static IServiceCollection AddApiHttpClient(this IServiceCollection services)
        {
            using var serviceProvider = services.BuildServiceProvider();
            var environment = serviceProvider.GetRequiredService<IUserSpaEnvironmentSingleton>();
            services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(environment.ApiBaseAddress) });

            return services;
        }

        public static IServiceCollection AddUserSpaServices(this IServiceCollection services)
        {
            services
                .AddScoped<ICityStatisticsService, CityStatisticsService>()
                .AddScoped<IAlertsStatisticsService, AlertsStatisticsService>()
                .AddScoped<ICellService, CellService>()
                .AddScoped<ICsvService, CsvService>();
            

            return services;
        }

        public static IServiceCollection AddUserSpaFactories(this IServiceCollection services)
        {
            services
                .AddTransient<IAverageByDayNightDTOFactory, AverageByDayNightDTOFactory>();

            return services;
        }
    }
}
