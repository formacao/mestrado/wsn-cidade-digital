﻿using Infrastructure.Domain.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.User.SPA.DTOs;

namespace WSN.User.SPA.Factories.Interfaces
{
    public interface IAverageByDayNightDTOFactory
    {
        IAverageByDayNightDTOFactory WithTemperatureData(float dayAverage, float nightAverage, float overallAverage, string divId);
        IAverageByDayNightDTOFactory WithHumidtyData(float dayAverage, float nightAverage, float overallAverage, string divId);
        IAverageByDayNightDTOFactory WithToxicGasData(float dayAverage, float nightAverage, float overallAverage, string divId);
        IAverageByDayNightDTOFactory WithSmokeData(float dayAverage, float nightAverage, float overallAverage, string divId);
        IAverageByDayNightDTOFactory WithFireData(float dayAverage, float nightAverage, float overallAverage, string divId);
        IAverageByDayNightDTOFactory WithUvData(float dayAverage, float nightAverage, float overallAverage, string divId);

        AverageByDayNightDTO Make();
    }
}
