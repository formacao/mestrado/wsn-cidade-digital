﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.User.SPA.DTOs;
using WSN.User.SPA.Exceptions;
using WSN.User.SPA.Factories.Interfaces;

namespace WSN.User.SPA.Factories
{
    public class AverageByDayNightDTOFactory : IAverageByDayNightDTOFactory
    {
        private const string TEMPERATURE = "temperature";
        private const string HUMIDITY = "humidity";
        private const string SMOKE = "smoke";
        private const string TOXIC_GAS = "toxicGas";
        private const string UV = "uv";
        private const string FIRE = "fire";

        private IDictionary<string, bool> FactoryFlags;

        private float _temperatureDayAverage;
        private float _humidityDayAverage;
        private float _smokeDayAverage;
        private float _toxicGasDayAverage;
        private float _uvDayAverage;
        private float _fireDayAverage;

        private float _temperatureNightAverage;
        private float _humidityNightAverage;
        private float _smokeNightAverage;
        private float _toxicGasNightAverage;
        private float _uvNightAverage;
        private float _fireNightAverage;

        private float _temperatureOverallAverage;
        private float _humidityOverallAverage;
        private float _smokeOverallAverage;
        private float _toxicGasOverallAverage;
        private float _uvOverallAverage;
        private float _fireOverallAverage;

        private string _temperatureDivId;
        private string _humidityDivId;
        private string _smokeDivId;
        private string _toxixGasDivId;
        private string _uvDivId;
        private string _fireDivId;


        public AverageByDayNightDTOFactory()
        {
            FactoryFlags = new Dictionary<string, bool>
            {
                { TEMPERATURE, false },
                { HUMIDITY, false },
                { SMOKE, false },
                { UV, false },
                { TOXIC_GAS, false },
                { FIRE, false }
            };
        }


        public AverageByDayNightDTO Make()
        {
            foreach (var pair in FactoryFlags)
            {
                if (!pair.Value)
                    throw new FactoryWithFlagAsFalseException(nameof(AverageByDayNightDTOFactory), pair.Key);
            }

            return new AverageByDayNightDTO(_temperatureDayAverage, _temperatureNightAverage, _temperatureOverallAverage, _temperatureDivId, _humidityDayAverage, _humidityNightAverage, _humidityOverallAverage,
                _humidityDivId, _toxicGasDayAverage, _toxicGasNightAverage, _toxicGasOverallAverage, _toxixGasDivId, _smokeDayAverage, _smokeNightAverage, _smokeOverallAverage, _smokeDivId, _fireDayAverage,
                _fireNightAverage, _fireOverallAverage, _fireDivId, _uvDayAverage, _uvNightAverage, _uvOverallAverage, _uvDivId);
        }

        public IAverageByDayNightDTOFactory WithFireData(float dayAverage, float nightAverage, float overallAverage, string divId)
        {
            _fireDayAverage = dayAverage;
            _fireNightAverage = nightAverage;
            _fireOverallAverage = overallAverage;
            _fireDivId = divId;

            FactoryFlags[FIRE] = true;
            return this;
        }

        public IAverageByDayNightDTOFactory WithHumidtyData(float dayAverage, float nightAverage, float overallAverage, string divId)
        {
            _humidityDayAverage = dayAverage;
            _humidityNightAverage = nightAverage;
            _humidityOverallAverage = overallAverage;
            _humidityDivId = divId;

            FactoryFlags[HUMIDITY] = true;
            return this;
        }

        public IAverageByDayNightDTOFactory WithSmokeData(float dayAverage, float nightAverage, float overallAverage, string divId)
        {
            _smokeDayAverage = dayAverage;
            _smokeNightAverage = nightAverage;
            _smokeOverallAverage = overallAverage;
            _smokeDivId = divId;

            FactoryFlags[SMOKE] = true;
            return this;
        }

        public IAverageByDayNightDTOFactory WithTemperatureData(float dayAverage, float nightAverage, float overallAverage, string divId)
        {
            _temperatureDayAverage = dayAverage;
            _temperatureNightAverage = nightAverage;
            _temperatureOverallAverage = overallAverage;
            _temperatureDivId = divId;

            FactoryFlags[TEMPERATURE] = true;
            return this;
        }

        public IAverageByDayNightDTOFactory WithToxicGasData(float dayAverage, float nightAverage, float overallAverage, string divId)
        {
            _toxicGasDayAverage = dayAverage;
            _toxicGasNightAverage = nightAverage;
            _toxicGasOverallAverage = overallAverage;
            _toxixGasDivId = divId;

            FactoryFlags[TOXIC_GAS] = true;
            return this;
        }

        public IAverageByDayNightDTOFactory WithUvData(float dayAverage, float nightAverage, float overallAverage, string divId)
        {
            _uvDayAverage = dayAverage;
            _uvNightAverage = nightAverage;
            _uvOverallAverage = overallAverage;
            _uvDivId = divId;

            FactoryFlags[UV] = true;
            return this;
        }
    }
}
