﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace WSN.User.SPA.Pages
{
    public partial class Cell
    {
        private const string _realTimeTabName = "realTime";
        private const string _timeSeriesTabName = "timeSeries";
        private const string _averageDataTabName = "average";

        private string selectedTab = _realTimeTabName;

        public void OnSelectedTabChanged(string tabName)
        {
            selectedTab = tabName;
        }
    }
}
