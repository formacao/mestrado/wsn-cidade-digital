﻿using Infrastructure.Domain.RequestModels;
using Infrastructure.Domain.ResponseModels;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using WSN.User.SPA.Components;
using WSN.User.SPA.Services.Interfaces;
using WSN.User.SPA.Singletons.Interfaces;

namespace WSN.User.SPA.Pages
{
    public partial class Csv
    {
        private bool _allCellsSelected;
        private DateTime _start = DateTime.Now.AddDays(-7);
        private DateTime _end = DateTime.Now;
        private AutocompleteSearchCell _autocompleteSearchCell;
        private bool _noCell = false;
        private bool _noData = false;

        private List<SearchCellModel> _selectedCells = new List<SearchCellModel>();

        [Inject]
        public ICsvService CsvService { get; set; }
        
        [Inject]
        public IJSRuntime JS { get; set; }

        [Inject]
        public IUserSpaEnvironmentSingleton Environment { get; set; }
        private void AddCell()
        {
            var selectedCell = _autocompleteSearchCell.GetSelectCellObject();
            if (selectedCell == null || _allCellsSelected) return;
            if (_selectedCells.FirstOrDefault(x => x.Id == selectedCell.Id) != null)
                return;

            _selectedCells.Add(selectedCell);
            _noCell = false;
            StateHasChanged();

        }

        private async Task GenerateCsv()
        {
            if (!_selectedCells.Any() && !_allCellsSelected)
            {
                _noCell = true;
                _noData = false;
                StateHasChanged();
                return;
            }
            _noCell = false;
            
            var request = new CsvDataRequestModel
            {
                Ids = _selectedCells.Select(x => x.Id),
                Start = _start,
                End =  _end
            };
            
            var (hasFile, fileName) = await CsvService.GetCsvLocationAsync(request);
            if (!hasFile)
            {
                _noData = true;
                StateHasChanged();
                return;
            }

            _noData = false;
            var csvUrl = Path.Combine(Environment.CsvBaseAddress, fileName);
            await JS.InvokeAsync<object>("open", csvUrl, "_blank");
            _selectedCells = new List<SearchCellModel>();
            _allCellsSelected = false;
            StateHasChanged();
        }
    }
}
