﻿using Infrastructure.Domain.ResponseModels;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using WSN.User.SPA.Services.Interfaces;

namespace WSN.User.SPA.Services
{
    public class AlertsStatisticsService : IAlertsStatisticsService
    {
        private readonly HttpClient _client;
        private readonly JsonSerializerOptions _jsonOptions;

        public AlertsStatisticsService(HttpClient client)
        {
            _client = client;
            _jsonOptions = new JsonSerializerOptions();
            _jsonOptions.PropertyNameCaseInsensitive = true;
        }

        public async Task<IEnumerable<AlertDataModel>> GetAlertsAsync() => await _client.GetFromJsonAsync<IEnumerable<AlertDataModel>>("api/city/alerts", _jsonOptions);
    }
}
