﻿using Infrastructure.Domain.ResponseModels;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using WSN.User.SPA.Services.Interfaces;

namespace WSN.User.SPA.Services
{
    public class CityStatisticsService : ICityStatisticsService
    {
        private readonly HttpClient _client;
        private readonly JsonSerializerOptions _jsonOptions;

        public CityStatisticsService(HttpClient client)
        {
            _client = client;
            _jsonOptions = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
        }

        public async Task<IEnumerable<AverageByDayNightModel>> GetByDayNightAsync() => await _client.GetFromJsonAsync<IEnumerable<AverageByDayNightModel>>("api/city/daynight", _jsonOptions);
        public async Task<OverallAverageModel> GetOverallAsync() => await _client.GetFromJsonAsync<OverallAverageModel>("api/city/overall", _jsonOptions);

    }
}
