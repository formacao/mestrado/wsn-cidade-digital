﻿using Infrastructure.Domain.RequestModels;
using Infrastructure.Domain.ResponseModels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using WSN.User.SPA.Services.Interfaces;

namespace WSN.User.SPA.Services
{
    public class CellService : ICellService
    {
        private readonly HttpClient _http;
        private readonly JsonSerializerOptions _jsonOptions;

        public CellService(HttpClient http)
        {
            _http = http;
            _jsonOptions = new JsonSerializerOptions();
            _jsonOptions.PropertyNameCaseInsensitive = true;
        }

        public async Task<IEnumerable<AverageByDayNightModel>> GetAverageByDayNightAsync(int cellId, DateTime startDateTime, DateTime endDateTime)
        {
            var request = new CellWithTimeIntervalRequestModel
            {
                CellId = cellId,
                Start = startDateTime,
                End = endDateTime
            };
            var result = await _http.PostAsJsonAsync("api/cell/averagebydaynight", request);
            if (result.StatusCode == HttpStatusCode.NoContent) return null;

            result.EnsureSuccessStatusCode();

            var stringResult = await result.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<IEnumerable<AverageByDayNightModel>>(stringResult, _jsonOptions);
        }

        public async Task<OverallAverageModel> GetOverallAverageAsync(int cellId, DateTime startDateTime, DateTime endDateTime)
        {
            var request = new CellWithTimeIntervalRequestModel
            {
                CellId = cellId,
                Start = startDateTime,
                End = endDateTime
            };
            var result = await _http.PostAsJsonAsync("api/cell/overallaverage", request);
            if (result.StatusCode == HttpStatusCode.NoContent) return null;

            result.EnsureSuccessStatusCode();

            var stringResult = await result.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<OverallAverageModel>(stringResult, _jsonOptions);
        }

        public async Task<RealTimeMeasurementsModel> GetRealTimeMeasurementsAsync(int cellId) => await _http.GetFromJsonAsync<RealTimeMeasurementsModel>($"api/cell/last/{cellId}", _jsonOptions);
        public async Task<IEnumerable<SearchCellModel>> GetSearchResultAsync(string search) => await _http.GetFromJsonAsync<IEnumerable<SearchCellModel>>($"api/cell/search/{search}", _jsonOptions);

        public async Task<IEnumerable<TimeSeriesModel>> GetTimeSeriesAsync(int cellId, DateTime startDateTime, DateTime endDateTime, string columnName)
        {
            var request = new CellWithTimeIntervalAndColumnNameRequestModel
            {
                CellId = cellId,
                Start = startDateTime,
                End = endDateTime,
                ColumnName = columnName
            };
            var result = await _http.PostAsJsonAsync("api/cell/timeseries", request, _jsonOptions);

            if (result.StatusCode == HttpStatusCode.NoContent) return null;

            result.EnsureSuccessStatusCode();

            var stringResult = await result.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<IEnumerable<TimeSeriesModel>>(stringResult, _jsonOptions);
        }
    }
}
