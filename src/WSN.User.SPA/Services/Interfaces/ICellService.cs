﻿using Infrastructure.Domain.ResponseModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WSN.User.SPA.Services.Interfaces
{
    public interface ICellService
    {
        Task<IEnumerable<SearchCellModel>> GetSearchResultAsync(string search);
        Task<RealTimeMeasurementsModel> GetRealTimeMeasurementsAsync(int cellId);
        Task<IEnumerable<TimeSeriesModel>> GetTimeSeriesAsync(int cellId, DateTime startDateTime, DateTime endDateTime, string columnName);
        Task<IEnumerable<AverageByDayNightModel>> GetAverageByDayNightAsync(int cellId, DateTime startDateTime, DateTime endDateTime);
        Task<OverallAverageModel> GetOverallAverageAsync(int cellId, DateTime startDateTime, DateTime endDateTime);
    }
}
