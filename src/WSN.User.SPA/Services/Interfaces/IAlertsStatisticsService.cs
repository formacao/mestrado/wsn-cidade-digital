﻿using Infrastructure.Domain.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WSN.User.SPA.Services.Interfaces
{
    public interface IAlertsStatisticsService
    {
        Task<IEnumerable<AlertDataModel>> GetAlertsAsync();
    }
}
