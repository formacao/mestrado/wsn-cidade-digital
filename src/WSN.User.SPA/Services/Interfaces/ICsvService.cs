﻿using Infrastructure.Domain.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSN.User.SPA.Services.Interfaces
{
    public interface ICsvService
    {
        Task<(bool hasFile, string filePath)> GetCsvLocationAsync(CsvDataRequestModel request);
    }
}
