﻿using Infrastructure.Domain.RequestModels;
using Infrastructure.Domain.ResponseModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using WSN.User.SPA.Services.Interfaces;
using WSN.User.SPA.Singletons.Interfaces;

namespace WSN.User.SPA.Services
{
    public class CsvService : ICsvService
    {
        private readonly HttpClient _http;
        private readonly JsonSerializerOptions _jsonOptions;
        private readonly ILogger<CsvService> _logger;

        public CsvService(HttpClient http, ILogger<CsvService> logger)
        {
            _http = http;
            _jsonOptions = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _logger = logger;
        }

        public async Task<(bool hasFile, string filePath)> GetCsvLocationAsync(CsvDataRequestModel request)
        {
            var serializedRequest = JsonSerializer.Serialize(request);
            var encodedRequest = new StringContent(serializedRequest, Encoding.UTF8, "application/json");
            var result = await _http.PostAsync("api/csv/data", encodedRequest);
            var stringResult = await result.Content.ReadAsStringAsync();

            _logger?.LogDebug($"Resultado da requisição : {stringResult}");
            
            if (result.StatusCode == HttpStatusCode.NoContent)
                return (false, string.Empty);

            result.EnsureSuccessStatusCode();

            var deserializedResult = JsonSerializer.Deserialize<CsvPathResponseModel>(stringResult, _jsonOptions);
            return (true, deserializedResult.FileName);
        }
    }
}
