﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSN.User.SPA.DTOs
{
    public class SingleMeasuramentByDayNightDTO
    {
        public string DivId { get; set; }
        public float DayAverage { get; set; }
        public float NightAverage { get; set; }
        public float OverallAverage { get; set; }
    }
}
