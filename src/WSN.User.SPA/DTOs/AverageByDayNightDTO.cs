﻿using Infrastructure.Domain.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSN.User.SPA.DTOs
{
    public class AverageByDayNightDTO
    {
        public IDictionary<string, SingleMeasuramentByDayNightDTO> MeasurementData { get; set; }

        public AverageByDayNightDTO(float averageTemperatureDay, float averageTemperatureNight, float overallAverageTemperature, string temperatureDivId, float averageHumidityDay, float averageHumidityNight, 
            float overallAverageHumidity, string humidityDivId, float averageToxicGasDay, float averageToxicGasNight, float overallAverageToxicGas, string toxicGasDivId, float averageSmokeDay, 
            float averageSmokeNight, float overallAverageSmoke, string smokeDivId, float averageFireDay, float averageFireNight, float overallAverageFire, string fireDivId, float averageUvDay, 
            float averageUvNight, float overallAverageUv, string UvDivId)
        {
            MeasurementData = new Dictionary<string, SingleMeasuramentByDayNightDTO>();

            var temperatureData = new SingleMeasuramentByDayNightDTO { DayAverage = averageTemperatureDay, NightAverage = averageTemperatureNight, OverallAverage = overallAverageTemperature, 
                DivId = temperatureDivId };

            var humidityData = new SingleMeasuramentByDayNightDTO { DayAverage = averageHumidityDay, NightAverage = averageHumidityNight, OverallAverage = overallAverageHumidity, DivId = humidityDivId };
            var toxicGasData = new SingleMeasuramentByDayNightDTO { DayAverage = averageToxicGasDay, NightAverage = averageToxicGasNight, OverallAverage = overallAverageToxicGas, DivId = toxicGasDivId };
            var smokeData = new SingleMeasuramentByDayNightDTO { DayAverage = averageSmokeDay, NightAverage = averageSmokeNight, OverallAverage = overallAverageSmoke, DivId = smokeDivId };
            var fireData = new SingleMeasuramentByDayNightDTO { DayAverage = averageFireDay, NightAverage = averageFireNight, OverallAverage= overallAverageFire, DivId = fireDivId };
            var uvData = new SingleMeasuramentByDayNightDTO { DayAverage = averageUvDay, NightAverage = averageUvNight, OverallAverage = overallAverageUv, DivId = UvDivId };

            MeasurementData.Add("temperature", temperatureData);
            MeasurementData.Add("humidity", humidityData);
            MeasurementData.Add("toxicGas", toxicGasData);
            MeasurementData.Add("smoke", smokeData);
            MeasurementData.Add("fire", fireData);
            MeasurementData.Add("uv", uvData);

        }

    }
}
