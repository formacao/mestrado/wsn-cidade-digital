﻿using System;
using WSN.User.SPA.Singletons.Interfaces;

namespace WSN.User.SPA.Singletons
{
    public class ReleaseUserSpaEnvironmentSingleton : IUserSpaEnvironmentSingleton
    {
        public string ApiBaseAddress => throw new InvalidOperationException("Environment not configured");
        public string CsvBaseAddress => throw new InvalidOperationException("Environment not configured");
        public string MapBoxJSAccessToken => "pk.eyJ1IjoiY2ljZXJvLWxhY2VyZGEiLCJhIjoiY2tqZ3lidWg3MTF0bzJycGdkYm5ud3BvMyJ9.vahYlvVyghbL7MUv_EW82g";
    }
}