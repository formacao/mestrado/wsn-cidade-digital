﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSN.User.SPA.Singletons.Interfaces
{
    public interface IUserSpaEnvironmentSingleton
    {
        string ApiBaseAddress { get; }
        string CsvBaseAddress { get; }
        string MapBoxJSAccessToken { get; }
    }
}
