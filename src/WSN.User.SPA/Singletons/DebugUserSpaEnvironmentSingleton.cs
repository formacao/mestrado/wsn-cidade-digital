﻿using WSN.User.SPA.Singletons.Interfaces;

namespace WSN.User.SPA.Singletons
{
    public class DebugUserSpaEnvironmentSingleton : IUserSpaEnvironmentSingleton
    {
        public string ApiBaseAddress => "http://localhost:5000";
        public string CsvBaseAddress => "http://localhost:9000/mestrado";
        public string MapBoxJSAccessToken => "pk.eyJ1IjoiY2ljZXJvLWxhY2VyZGEiLCJhIjoiY2tqZ3lidWg3MTF0bzJycGdkYm5ud3BvMyJ9.vahYlvVyghbL7MUv_EW82g";
        
    }
}