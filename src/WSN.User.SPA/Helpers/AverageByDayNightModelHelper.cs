﻿using Infrastructure.Domain.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.User.SPA.DTOs;

namespace WSN.User.SPA.Helpers
{
    public static class AverageByDayNightModelHelper
    {
        public static (AverageByDayNightModel dayData, AverageByDayNightModel nightData) SeparateDayAndNight(IEnumerable<AverageByDayNightModel> averagesByDayNight)
        {
            AverageByDayNightModel firstAverage;
            AverageByDayNightModel secondAverage;

            if(averagesByDayNight.Count() == 0)
            {
                firstAverage = new AverageByDayNightModel();
                secondAverage = new AverageByDayNightModel();
            }
            else if(averagesByDayNight.Count() == 1)
            {
                firstAverage = averagesByDayNight.ElementAt(0);
                secondAverage = new AverageByDayNightModel();
            }
            else
            {
                firstAverage = averagesByDayNight.ElementAt(0);
                secondAverage = averagesByDayNight.ElementAt(1);
            }

            if ("Day".Equals(firstAverage.DayNight))
                return (firstAverage, secondAverage);

            return (secondAverage, firstAverage);
        }
    }
}
