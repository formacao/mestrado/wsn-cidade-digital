﻿using Infrastructure.Domain.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.User.SPA.DTOs;
using WSN.User.SPA.Factories.Interfaces;

namespace WSN.User.SPA.Helpers
{
    public static class AverageByDayNightDTOHelper
    {
        public static AverageByDayNightDTO GenerateDTOWithDivsIds(IEnumerable<AverageByDayNightModel> dayNight, OverallAverageModel overall, IAverageByDayNightDTOFactory factory,
            string temperatureDivId, string humidityDivId, string smokeDivId, string fireDivId, string uvDivId, string toxicGasDivId)
        {
            if (dayNight == null || overall == null) return null;

            var (dayData, nightData) = AverageByDayNightModelHelper.SeparateDayAndNight(dayNight);

            return factory
                .WithFireData(dayData.AvgFire, nightData.AvgFire, overall.AvgFire, fireDivId)
                .WithHumidtyData(dayData.AvgHumidity, nightData.AvgHumidity, overall.AvgHumidity, humidityDivId)
                .WithSmokeData(dayData.AvgSmoke, nightData.AvgSmoke, overall.AvgSmoke, smokeDivId)
                .WithTemperatureData(dayData.AvgTemperature, nightData.AvgTemperature, overall.AvgTemperature, temperatureDivId)
                .WithToxicGasData(dayData.AvgToxicGas, nightData.AvgToxicGas, overall.AvgToxicGas, toxicGasDivId)
                .WithUvData(dayData.AvgUv, nightData.AvgUv, overall.AvgUv, uvDivId)
                .Make();
        }
    }
}
