﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSN.Admin.Extensions
{
    public static class ControllerExtensions
    {
        public static RedirectToActionResult RedirectToForeignAction<T>(this Controller controller, string actionName) where T : Controller
        {
            var controllerName = typeof(T).Name.Replace("Controller", string.Empty);
            return controller.RedirectToAction(actionName, controllerName);
        }
    }
}
