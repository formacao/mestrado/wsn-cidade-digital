﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.DependencyInjection;
using WSN.Admin.Mappers;
using WSN.Admin.Mappers.Interfaces;
using WSN.Admin.Services;
using WSN.Admin.Services.Interfaces;

namespace WSN.Admin.Extensions
{
    public static class StartupExtensions
    {
        public static IServiceCollection AddAdminApplicationServices(this IServiceCollection services)
        {
            services
                .AddScoped<IUserService, UserService>()
                .AddScoped<ICellService, CellService>()
                .AddScoped<IMonitoringScheduleService, MonitoringScheduleService>();

            return services;
        }

        public static IServiceCollection AddAdminApplicationMappers(this IServiceCollection services)
        {
            services
                .AddSingleton<ICellMapper, CellMapper>()
                .AddSingleton<IMonitoringScheduleMapper, MonitoringScheduleMapper>();

            return services;
        }

        public static IServiceCollection AddAdminApplicationAuthenticationConfigurations(this IServiceCollection services)
        {
            services
                .AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                })
                .AddCookie();

            services.ConfigureApplicationCookie(x =>
            {
                x.Cookie.Name = "WSN.Admin";
                x.LoginPath = "/Login/Login";
                x.AccessDeniedPath = "/Login/Denied";
            });

            return services;
        }
    }
}
