﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Admin.RequestModels;
using WSN.Admin.ResponseModels;

namespace WSN.Admin.Mappers.Interfaces
{
    public interface IMonitoringScheduleMapper
    {
        ScheduleResponseModel Map(MonitoringScheduleModel model);
        MonitoringScheduleModel Map(AddScheduleRequestModel request);
    }
}
