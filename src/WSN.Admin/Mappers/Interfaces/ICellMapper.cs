﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Admin.RequestModels;
using WSN.Admin.ResponseModels;

namespace WSN.Admin.Mappers.Interfaces
{
    public interface ICellMapper
    {
        CellTableResponseModel MapTable(CellModel model);
        SeeCellResponseModel MapSee(CellModel model);
        EditCellRequestModel MapFull(CellModel model);
        CellModel Map(AddCellRequestModel request);
        CellModel Map(EditCellRequestModel request);
    }
}
