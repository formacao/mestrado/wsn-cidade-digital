﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Admin.Mappers.Interfaces;
using WSN.Admin.RequestModels;
using WSN.Admin.ResponseModels;

namespace WSN.Admin.Mappers
{
    public class CellMapper : ICellMapper
    {
        public CellTableResponseModel MapTable(CellModel model) => new CellTableResponseModel
        {
            Id = model.Id,
            Name = model.Name,
            Address = model.Address,
            Deleted = model.Deleted
        };

        public CellModel Map(AddCellRequestModel request) => new CellModel
        {
            Name = request.Name,
            Address = request.Address,
            Latitude = request.Latitude,
            Longitude = request.Longitude
        };

        public CellModel Map(EditCellRequestModel request) => new CellModel
        {
            Id = request.CellId,
            Name = request.Name,
            Address = request.Address,
            Latitude = request.Latitude,
            Longitude = request.Longitude
        };

        public EditCellRequestModel MapFull(CellModel model) => new EditCellRequestModel
        {
            CellId = model.Id,
            Name = model.Name,
            Address = model.Address,
            Latitude = model.Latitude,
            Longitude = model.Longitude
        };

        public SeeCellResponseModel MapSee(CellModel model) => new SeeCellResponseModel
        {
            Name = model.Name,
            Address = model.Address,
            Latitude = model.Latitude,
            Longitude = model.Longitude,
            Status = model.Deleted ? "Deletada" : "Ativa"
        };
    }
}
