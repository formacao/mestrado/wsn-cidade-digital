﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Admin.Mappers.Interfaces;
using WSN.Admin.RequestModels;
using WSN.Admin.ResponseModels;

namespace WSN.Admin.Mappers
{
    public class MonitoringScheduleMapper : IMonitoringScheduleMapper
    {
        public ScheduleResponseModel Map(MonitoringScheduleModel model)
        {

            var createdAt = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(model.MonitoringDateTime, TimeZoneInfo.Utc.Id, TimeZoneInfo.Local.Id).ToString();
            return new ScheduleResponseModel
            {
                CreatedAt = createdAt
            };
        }

        public MonitoringScheduleModel Map(AddScheduleRequestModel request) => new MonitoringScheduleModel
        {
            MonitoringDateTime = request.DateTime
        };
    }
}
