﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSN.Admin.ResponseModels
{
    public class MenuResponseModel
    {
        public string MenuSection { get; set; }
        public IEnumerable<string> Colors { get; set; }
        public IEnumerable<string> Titles { get; set; }
        public IEnumerable<string> Descriptions { get; set; }
        public IEnumerable<string> Controllers { get; set; }
        public IEnumerable<string> Actions { get; set; }
    }
}
