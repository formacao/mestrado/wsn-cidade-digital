﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSN.Admin.ResponseModels
{
    public class SeeCellResponseModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
