﻿using Infrastructure.Domain.Singletons;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WSN.Admin.Controllers
{
    [Authorize(Roles = RolesSingleton.Admin)]
    public class PanelController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
