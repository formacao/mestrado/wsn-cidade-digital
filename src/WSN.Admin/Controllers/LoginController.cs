﻿using System.Net;
using System.Threading.Tasks;
using ASPInfrastructure.Filters;
using Infrastructure.Domain.Singletons;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WSN.Admin.Extensions;
using WSN.Admin.RequestModels;
using WSN.Admin.Services.Interfaces;

namespace WSN.Admin.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUserService _userService;

        public LoginController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        public IActionResult Login() => View(new LoginRequestModel());

        [Authorize(Roles = RolesSingleton.Admin)]
        public async Task<IActionResult> Logout()
        {
            await _userService.LogoutAsync();
            return RedirectToAction(nameof(Login));
        }

        [AllowAnonymous]
        public IActionResult Denied() => View();

        [HttpPost, ModelStateMustBeValid(nameof(Login))]
        public async Task<IActionResult> Authenticate(LoginRequestModel request)
        {
            if(await _userService.AuthenticateAsync(request))
            {
                return this.RedirectToForeignAction<PanelController>(nameof(PanelController.Index));
            }

            HttpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;

            ModelState.AddModelError(string.Empty, "User not found");
            return View(nameof(Login));
        }
    }
}
