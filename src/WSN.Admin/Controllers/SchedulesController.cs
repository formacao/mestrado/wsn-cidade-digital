﻿using System;
using System.Net;
using System.Threading.Tasks;
using Infrastructure.Domain.Singletons;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WSN.Admin.RequestModels;
using WSN.Admin.Services.Interfaces;

namespace WSN.Admin.Controllers
{
    [Authorize(Roles = RolesSingleton.Admin)]
    public class SchedulesController : Controller
    {
        private readonly IMonitoringScheduleService _service;

        public SchedulesController(IMonitoringScheduleService service)
        {
            _service = service;
        }

        public async Task<IActionResult> Index() => View(await _service.GetAsync());

        public async Task<IActionResult> New()
        {
            var request = new AddScheduleRequestModel
            {
                DateTime = DateTime.UtcNow
            };

            if (await _service.AddAsync(request) == null)
            {
                HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                ModelState.AddModelError(null, "Unable to create schedule");
            }
            else
            {
                HttpContext.Response.StatusCode = (int)HttpStatusCode.Created;
            }

            return View(nameof(Index), await _service.GetAsync());
        }
    }
}
