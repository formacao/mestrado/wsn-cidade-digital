﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WSN.Admin.RequestModels;
using WSN.Admin.Services.Interfaces;
using ASPInfrastructure.Filters;
using Infrastructure.Domain.Singletons;

namespace WSN.Admin.Controllers
{
    [Authorize(Roles = RolesSingleton.Admin)]
    public class CellsController : Controller
    {

        private readonly ICellService _cellService;

        public CellsController(ICellService cellService)
        {
            _cellService = cellService;
        }

        public async Task<IActionResult> Index() => View(await _cellService.GetTableAsync());

        public IActionResult New() => View(new AddCellRequestModel());

        [HttpPost, ModelStateMustBeValid(nameof(New))]
        public async Task<IActionResult> Add(AddCellRequestModel request)
        {
            if(await _cellService.AddAsync(request) == null)
                return FailedToSaveCell(nameof(New), request);

            HttpContext.Response.StatusCode = (int)HttpStatusCode.Created;
            return View(nameof(Index), await _cellService.GetTableAsync());
        }

        [CellMustExist("_404"), Route("[controller]/[action]/{cellId:int}")]
        public async Task<IActionResult> Edit(int cellId) => View(await _cellService.GetAsync(cellId));

        [HttpPost, CellMustExist("_404", Order = 1), ModelStateMustBeValid(nameof(Edit), Order = 2)]
        public async Task<IActionResult> SaveEdit(EditCellRequestModel request)
        {
            if (await _cellService.UpdateAsync(request) == null)
                return FailedToSaveCell(nameof(Edit), request);

            return RedirectToAction(nameof(Index));

        }

        [CellMustExist("_404"), Route("[controller]/[action]/{cellId:int}")]
        public async Task<IActionResult> See(int cellId) => View(await _cellService.GetSeeAsync(cellId));

        [CellMustExist("_404"), Route("[controller]/[action]/{cellId:int}")]
        public async Task<IActionResult> Delete(int cellId)
        {
            if (await _cellService.DeleteAsync(cellId) == null)
                return FailedToSaveCell("DeleteFailed");

            return RedirectToAction(nameof(Index));
        }

        private IActionResult FailedToSaveCell(string viewName, object model = null)
        {
            HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            ModelState.AddModelError(string.Empty, "Failed while saving cell.");
            if(model != null)
                return View(viewName, model);

            return View(viewName);
        }
    } 
}
