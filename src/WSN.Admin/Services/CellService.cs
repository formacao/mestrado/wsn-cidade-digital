﻿using Domain.Models;
using Infrastructure.Domain.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Admin.Mappers.Interfaces;
using WSN.Admin.RequestModels;
using WSN.Admin.ResponseModels;
using WSN.Admin.Services.Interfaces;

namespace WSN.Admin.Services
{
    public class CellService : ICellService
    {

        private readonly ICellRepository _repository;
        private readonly ICellMapper _mapper;

        public CellService(ICellRepository repository, ICellMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<CellModel> AddAsync(AddCellRequestModel request)
        {
            var cell = _mapper.Map(request);
            _repository.Add(cell);

            if (await _repository.UnitOfWork.SaveChangesAsync() == 1) 
                return cell;

            return null;
        }

        public async Task<IEnumerable<CellTableResponseModel>> GetTableAsync()
        {
            var cells = await _repository.GetAsync();
            return cells.Select(_mapper.MapTable);
        }

        public async Task<EditCellRequestModel> GetAsync(int id)
        {
            var cell = await _repository.GetAsync(id);
            return _mapper.MapFull(cell);
        }

        public async Task<CellModel> UpdateAsync(EditCellRequestModel request)
        {
            var cell = _mapper.Map(request);

            _repository.Update(cell);
            if (await _repository.UnitOfWork.SaveChangesAsync() == 1)
                return cell;

            return null;
        }

        public async Task<SeeCellResponseModel> GetSeeAsync(int id)
        {
            var cell = await _repository.GetAsync(id);
            return _mapper.MapSee(cell);
        }

        public async Task<CellModel> DeleteAsync(int id)
        {
            var cell = await _repository.GetAsync(id);
            if (cell == null) return null;

            _repository.Delete(cell);
            if (await _repository.UnitOfWork.SaveChangesAsync() == 1)
                return cell;

            return null;
        }
    } 
}
