﻿using Infrastructure.Domain.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Admin.Mappers.Interfaces;
using WSN.Admin.RequestModels;
using WSN.Admin.ResponseModels;
using WSN.Admin.Services.Interfaces;

namespace WSN.Admin.Services
{
    public class MonitoringScheduleService : IMonitoringScheduleService
    {
        private readonly IMonitoringScheduleRepository _repository;
        private readonly IMonitoringScheduleMapper _mapper;

        public MonitoringScheduleService(IMonitoringScheduleRepository repository, IMonitoringScheduleMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<ScheduleResponseModel> AddAsync(AddScheduleRequestModel request)
        {
            var schedule = _mapper.Map(request);
            _repository.Add(schedule);
            if (await _repository.UnitOfWork.SaveChangesAsync() == 1)
                return _mapper.Map(schedule);

            return null;
        }

        public async Task<IEnumerable<ScheduleResponseModel>> GetAsync()
        {
            var result = await _repository.GetAsync();
            return result.Select(_mapper.Map);
        }
    }
}
