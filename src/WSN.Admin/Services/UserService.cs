﻿using EFCoreInfrastructure.Models;
using EFCoreInfrastructure.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WSN.Admin.RequestModels;
using WSN.Admin.Services.Interfaces;

namespace WSN.Admin.Services
{
    public class UserService : IUserService
    {

        private readonly IUserRepository _userRepository;
        private readonly HttpContext _context;
        public UserService(IUserRepository userRepository, IHttpContextAccessor httpAcessor)
        {
            _userRepository = userRepository;
            _context = httpAcessor.HttpContext;
        }

        public async Task<bool> AuthenticateAsync(LoginRequestModel request)
        {
            var result = await _userRepository.AuthenticateAsync(request.User, request.Password);
            if (result == (null, null)) return false;

            await CreateCookie(result.Item1, result.Item2);
            return true;
        }

        public async Task LogoutAsync() => await _context.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

        private async Task CreateCookie(User user, IList<string> roles)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.NameIdentifier, user.NormalizedUserName)
            };

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var claimsIdentity = new ClaimsIdentity(claims, "Login");
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
            var properties = new AuthenticationProperties
            {
                IsPersistent = true,
                ExpiresUtc = DateTime.UtcNow.AddHours(24),
                AllowRefresh = true
            };

            await _context.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal, properties);
        }
    }
}
