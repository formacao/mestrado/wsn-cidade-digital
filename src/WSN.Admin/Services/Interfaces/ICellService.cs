﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Admin.RequestModels;
using WSN.Admin.ResponseModels;

namespace WSN.Admin.Services.Interfaces
{
    public interface ICellService
    {
        Task<CellModel> AddAsync(AddCellRequestModel request);
        Task<IEnumerable<CellTableResponseModel>> GetTableAsync();
        Task<EditCellRequestModel> GetAsync(int id);
        Task<SeeCellResponseModel> GetSeeAsync(int id);
        Task<CellModel> UpdateAsync(EditCellRequestModel request);
        Task<CellModel> DeleteAsync(int id);
    }
}
