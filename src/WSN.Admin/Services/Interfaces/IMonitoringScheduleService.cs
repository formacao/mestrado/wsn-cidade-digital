﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Admin.RequestModels;
using WSN.Admin.ResponseModels;

namespace WSN.Admin.Services.Interfaces
{
    public interface IMonitoringScheduleService
    {
        Task<IEnumerable<ScheduleResponseModel>> GetAsync();
        Task<ScheduleResponseModel> AddAsync(AddScheduleRequestModel request);
    }
}
