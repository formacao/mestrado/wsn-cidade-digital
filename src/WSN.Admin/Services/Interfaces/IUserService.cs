﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Admin.RequestModels;

namespace WSN.Admin.Services.Interfaces
{
    public interface IUserService
    {
        Task<bool> AuthenticateAsync(LoginRequestModel request);
        Task LogoutAsync();
    }
}
