﻿using Infrastructure.Domain.RequestModels.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace WSN.Admin.RequestModels
{
    public class LoginRequestModel : ILoginRequestModel
    {
        [Required, MaxLength(20)]
        public string User { get; set; }

        [Required, MaxLength(20)]
        public string Password { get; set; }
    }
}
