﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSN.Admin.RequestModels
{
    public class AddScheduleRequestModel
    {
        public DateTime DateTime { get; set; }
    }
}
