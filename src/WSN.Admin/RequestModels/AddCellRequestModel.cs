﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Validations;

namespace WSN.Admin.RequestModels
{
    public class AddCellRequestModel
    {
        [Required, MaxLength(200)]
        public string Name { get; set; }

        [Required, MaxLength(300)]
        public string Address { get; set; }

        [Required]
        public double Latitude { get; set; }

        [Required]
        public double Longitude { get; set; }
    }
}
