﻿using Infrastructure.Domain.RequestModels.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace WSN.Admin.RequestModels
{
    public class EditCellRequestModel : ICellIdRequestModel
    {
        public int CellId { get; set; }

        [Required, MaxLength(200)]
        public string Name { get; set; }

        [Required, MaxLength(300)]
        public string Address { get; set; }

        [Required]
        public double Latitude { get; set; }

        [Required]
        public double Longitude { get; set; }
    }
}
