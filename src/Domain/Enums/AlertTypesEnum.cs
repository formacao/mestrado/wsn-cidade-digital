﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums
{
    public enum AlertTypesEnum
    {
        HighTemperature = 1,
        LowHumidity,
        FireDetected,
        HighUvRadiation,
        SmokeDetected,
        ToxicGasDetected
    }
}
