﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Interfaces
{
    public interface IBaseModel
    {
        public int Id { get; set; }
        public bool Deleted { get; set; }
    }
}
