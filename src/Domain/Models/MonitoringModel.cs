﻿using Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class MonitoringModel : IBaseModel
    {
        public int Id { get; set; }

        public int MonitoringScheduleId { get; set; }
        public MonitoringScheduleModel MonitoringSchedule { get; set; }

        public int CellId { get; set; }
        public CellModel Cell { get; set; }

        public float Temperature { get; set; }
        public float Fire { get; set; }
        public float Humidity { get; set; }
        public float Smoke { get; set; }
        public float ToxicGas { get; set; }
        public float UvRadiation { get; set; }
        public bool Deleted { get; set; }


        public ICollection<AlertModel> Alerts { get; set; }
    }
}
