﻿using Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class AlertModel : IBaseModel
    {
        public int Id { get; set; }

        public int MonitoringId { get; set; }
        public MonitoringModel Monitoring { get; set; }

        public int AlertTypeId { get; set; }
        public AlertTypeModel AlertType { get; set; }

        public bool Deleted { get; set; }

    }
}
