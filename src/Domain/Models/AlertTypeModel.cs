﻿using Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class AlertTypeModel : IBaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<AlertModel> Alerts { get; set; }
        public bool Deleted { get; set; }

    }
}
