﻿using Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class MonitoringScheduleModel : IBaseModel
    {
        public int Id { get; set; }
        public DateTime MonitoringDateTime { get; set; }
        public bool Deleted { get; set; }


        public ICollection<MonitoringModel> Monitorings { get; set; }

    }
}
