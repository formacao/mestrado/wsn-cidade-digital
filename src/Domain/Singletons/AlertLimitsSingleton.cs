﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Singletons
{
    public static class AlertLimitsSingleton
    {
        public const float MaxTemperature = 35;
        public const float MinHumidity = 30;
        public const float MaxUvRadiation = 4;
        public const float MaxFire = 100;
        public const float MaxSmoke = 1000;
        public const float MaxToxicGas = 1000;
    }
}
