﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using CsvHelper;
using WSN.User.API.Services.Interfaces;
using WSN.User.API.Singleton.Interface;

namespace WSN.User.API.Services
{
    public class CsvFileHandlerService : ICsvFileHandlerService
    {

        private readonly IUserApiEnvironmentSingleton _environment;

        public CsvFileHandlerService(IUserApiEnvironmentSingleton environment)
        {
            _environment = environment;
        }

        public async Task WriteCsvAsync<T>(IEnumerable<T> rows, string fileName)
        {
            var pathToSave = Path.Combine(_environment.TempCsvDirectory, fileName);
            await using var writer = new StreamWriter(pathToSave);
            await using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);
            await csvWriter.WriteRecordsAsync(rows);     
        }
    }
}