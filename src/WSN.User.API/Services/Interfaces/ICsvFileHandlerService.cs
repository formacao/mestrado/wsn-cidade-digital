﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WSN.User.API.Services.Interfaces
{
    public interface ICsvFileHandlerService
    {
        Task WriteCsvAsync<T>(IEnumerable<T> rows, string pathToSave);
    }
}