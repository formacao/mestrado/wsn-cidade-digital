﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using WSN.User.API.Factories;
using WSN.User.API.Services;
using WSN.User.API.Services.Interfaces;
using WSN.User.API.Singleton;
using WSN.User.API.Singleton.Interface;

namespace WSN.User.API.Extensions
{
    public static class UserApiExtensions
    {
        public const string BlazorCorsPolicyName = "BlazorPolicy";

        public static IServiceCollection AddCorsPolicies(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(name: BlazorCorsPolicyName, builder =>
                {
                    builder
                    .WithOrigins("http://localhost:5010")
                    .AllowAnyHeader();
                });
            });

            return services;
        }

        public static IServiceCollection AddApiServices(this IServiceCollection services)
        {
            services
                .AddSingleton<ICsvFileHandlerService, CsvFileHandlerService>();
            return services;
        }

        public static IServiceCollection AddDotEnv(this IServiceCollection services)
        {
            services.AddSingleton<IUserApiEnvironmentSingleton>(sp => UserApiEnvironmentSingletonFactory.EnvironmentSingleton);
            return services;
        }
        
    }
}
