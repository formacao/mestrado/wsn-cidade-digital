﻿namespace WSN.User.API.Singleton.Interface
{
    public interface IUserApiEnvironmentSingleton
    {
        string TempCsvDirectory { get;  }
    }
}