﻿using System;
using WSN.User.API.Singleton.Interface;

namespace WSN.User.API.Singleton
{
    public class UserApiEnvironmentSingleton : IUserApiEnvironmentSingleton
    {
        public string TempCsvDirectory { get; set; }
    }
    
}