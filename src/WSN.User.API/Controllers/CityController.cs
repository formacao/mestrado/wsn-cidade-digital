﻿using System.Threading.Tasks;
using Infrastructure.Domain.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace WSN.User.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly ICityStatisticsRepository _cityRepository;
        private readonly IAlertStatisticsRepository _alertRepository;

        public CityController(ICityStatisticsRepository cityRepository, IAlertStatisticsRepository alertRepository)
        {
            _cityRepository = cityRepository;
            _alertRepository = alertRepository;
        }

        [HttpGet, Route("[action]")]
        public async Task<IActionResult> DayNight() => Ok(await _cityRepository.GetAverageByDayNightAsync());

        [HttpGet, Route("[action]")]
        public async Task<IActionResult> Overall() => Ok(await _cityRepository.GetOverallAverageAsync());

        [HttpGet, Route("[action]")]
        public async Task<IActionResult> Alerts() => Ok(await _alertRepository.GetOverall());
    }
}
