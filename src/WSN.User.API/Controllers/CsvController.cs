﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.RequestModels;
using Infrastructure.Domain.ResponseModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSN.User.API.Services.Interfaces;
using WSN.User.API.Singleton.Interface;

namespace WSN.User.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CsvController : ControllerBase
    {
        public CsvController(ICsvDataRepository csvDataRepository, ICsvFileHandlerService csvFileHandlerService)
        {
            _csvDataRepository = csvDataRepository;
            _csvFileHandlerService = csvFileHandlerService;
        }

        private readonly ICsvDataRepository _csvDataRepository;
        private readonly ICsvFileHandlerService _csvFileHandlerService;

        [HttpPost, Route("[action]")]
        public async Task<IActionResult> Data(CsvDataRequestModel request)
        {
           var data =  await _csvDataRepository.GetCsvDataAsync(request);
           if (!data.Any())
               return NoContent();

           try
           {
               var fileName = $"{Guid.NewGuid()}.csv";
               await _csvFileHandlerService.WriteCsvAsync(data, fileName);
               var response = new CsvPathResponseModel {FileName = fileName};
               return Ok(response);
           }
           catch (Exception e)
           {
               throw;
           }
        }
            
    }
}
