﻿using System.Threading.Tasks;
using ASPInfrastructure.Filters;
using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.RequestModels;
using Microsoft.AspNetCore.Mvc;

namespace WSN.User.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CellController : ControllerBase
    {
        private readonly ICellDataRepository _cellDataRepository;

        public CellController(ICellDataRepository cellDataRepository)
        {
            _cellDataRepository = cellDataRepository;
        }

        [HttpGet, Route("[action]/{search}")]
        public async Task<IActionResult> Search(string search) => Ok(await _cellDataRepository.GetSearchResultAsync(search));

        [HttpGet, Route("[action]/{cellId:int}"), CellMustExist]
        public async Task<IActionResult> Last(int cellId)
        {
            var measurement = await _cellDataRepository.GetLastMeasurementAsync(cellId);

            if (measurement == null)
                return NoContent();

            return Ok(measurement);
        }

        [HttpPost, Route("[action]"), CellMustExist]
        public async Task<IActionResult> TimeSeries(CellWithTimeIntervalAndColumnNameRequestModel request) => 
            Ok(await _cellDataRepository.GetTimeSeriesAsync(request.CellId, request.Start, request.End, request.ColumnName));

        [HttpPost, Route("[action]"), CellMustExist]
        public async Task<IActionResult> AverageByDayNight(CellWithTimeIntervalRequestModel request) =>
            Ok(await _cellDataRepository.GetAverageByDayNight(request.CellId, request.Start, request.End));

        [HttpPost, Route("[action]"), CellMustExist]
        public async Task<IActionResult> OverallAverage(CellWithTimeIntervalRequestModel request) =>
            Ok(await _cellDataRepository.GetOverallAverage(request.CellId, request.Start, request.End));


    }
}
