﻿using System.IO;
using System.Text.Json;
using WSN.User.API.Singleton;
using WSN.User.API.Singleton.Interface;

namespace WSN.User.API.Factories
{
    public static class UserApiEnvironmentSingletonFactory
    {
        private static IUserApiEnvironmentSingleton _environmentSingleton;

        public static IUserApiEnvironmentSingleton EnvironmentSingleton
        {
            get
            {
                if (_environmentSingleton == null) _environmentSingleton = CreateInstance();
                return _environmentSingleton;
            }
        }

        private static IUserApiEnvironmentSingleton CreateInstance()
        {
            using var dotEnvReader = new StreamReader(".env.json");
            var serializedDotEnv = dotEnvReader.ReadToEnd();
            return JsonSerializer.Deserialize<UserApiEnvironmentSingleton>(serializedDotEnv);
        }
    }
}