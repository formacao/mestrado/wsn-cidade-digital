USE [master]
GO
/****** Object:  Database [WSN]    Script Date: 11/11/2020 07:52:52 ******/
CREATE DATABASE [WSN]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'WSN', FILENAME = N'/var/opt/mssql/data/WSN.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'WSN_log', FILENAME = N'/var/opt/mssql/data/WSN_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [WSN] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [WSN].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [WSN] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [WSN] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [WSN] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [WSN] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [WSN] SET ARITHABORT OFF 
GO
ALTER DATABASE [WSN] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [WSN] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [WSN] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [WSN] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [WSN] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [WSN] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [WSN] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [WSN] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [WSN] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [WSN] SET  DISABLE_BROKER 
GO
ALTER DATABASE [WSN] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [WSN] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [WSN] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [WSN] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [WSN] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [WSN] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [WSN] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [WSN] SET RECOVERY FULL 
GO
ALTER DATABASE [WSN] SET  MULTI_USER 
GO
ALTER DATABASE [WSN] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [WSN] SET DB_CHAINING OFF 
GO
ALTER DATABASE [WSN] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [WSN] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [WSN] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [WSN] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [WSN] SET QUERY_STORE = OFF
GO
USE [WSN]
GO
/****** Object:  UserDefinedFunction [dbo].[DayNight]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[DayNight](@datetime DateTime)
returns varchar(100)
begin
	declare @hour int
	set @hour = DATEPART(HOUR, @datetime)
	if(@hour < 9 or @hour > 21)
		return 'Night'
	return 'Day'
end
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Alerts]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alerts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MonitoringId] [int] NOT NULL,
	[AlertTypeId] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Alerts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AlertTypes]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AlertTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_AlertTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cells]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cells](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Address] [nvarchar](300) NOT NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Cells] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MonitoringData]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonitoringData](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MonitoringScheduleId] [int] NOT NULL,
	[CellId] [int] NOT NULL,
	[Temperature] [real] NOT NULL,
	[Fire] [real] NOT NULL,
	[Humidity] [real] NOT NULL,
	[Smoke] [real] NOT NULL,
	[ToxicGas] [real] NOT NULL,
	[UvRadiation] [real] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_MonitoringData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MonitoringSchedules]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonitoringSchedules](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MonitoringDateTime] [datetime2](7) NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_MonitoringSchedules] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200921182352_Beginning', N'3.1.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200923175016_NewModels', N'3.1.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200925154243_DeleteColumn', N'3.1.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201018175312_AlertRefactored', N'3.1.8')
GO
SET IDENTITY_INSERT [dbo].[Alerts] ON 

INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (9, 3066, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (10, 3066, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (11, 3068, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (12, 3070, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (13, 3067, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (14, 3069, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (15, 3067, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (16, 3070, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (17, 3069, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (18, 3069, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (19, 3071, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (20, 3072, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (21, 3072, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (22, 3076, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (23, 3073, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (24, 3074, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (25, 3075, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (26, 3073, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (27, 3079, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (28, 3077, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (29, 3078, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (30, 3079, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (31, 3078, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (32, 3078, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (33, 3081, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (34, 3080, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (35, 3082, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (36, 3080, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (37, 3083, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (38, 3086, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (39, 3084, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (40, 3085, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (41, 3084, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (42, 3087, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (43, 3088, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (44, 3088, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (45, 3089, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (46, 3089, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (47, 3090, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (48, 3091, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (49, 3092, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (50, 3093, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (51, 3091, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (52, 3094, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (53, 3096, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (54, 3095, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (55, 3097, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (56, 3100, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (57, 3099, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (58, 3098, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (59, 3099, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (60, 3100, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (61, 3098, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (62, 3101, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (63, 3102, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (64, 3103, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (65, 3102, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (66, 3102, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (67, 3105, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (68, 3104, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (69, 3106, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (70, 3107, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (71, 3105, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (72, 3108, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (73, 3109, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (74, 3109, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (75, 3108, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (76, 3108, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (77, 3114, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (78, 3110, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (79, 3111, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (80, 3113, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (81, 3112, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (82, 3110, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (83, 3111, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (84, 3113, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (85, 3112, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (86, 3111, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (87, 3112, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (88, 3115, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (89, 3115, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (90, 3116, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (91, 3117, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (92, 3119, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (93, 3118, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (94, 3120, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (95, 3118, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (96, 3121, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (97, 3122, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (98, 3122, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (99, 3124, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (100, 3126, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (101, 3127, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (102, 3125, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (103, 3126, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (104, 3128, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (105, 3123, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (106, 3128, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (107, 3131, 1, 0)
GO
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (108, 3130, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (109, 3129, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (110, 3130, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (111, 3131, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (112, 3129, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (113, 3132, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (114, 3133, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (115, 3132, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (116, 3134, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (117, 3135, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (118, 3134, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (119, 3135, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (120, 3136, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (121, 3137, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (122, 3136, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (123, 3137, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (124, 3136, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (125, 3139, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (126, 3138, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (127, 3140, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (128, 3139, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (129, 3138, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (130, 3141, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (131, 3139, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (132, 3142, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (133, 3142, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (134, 3143, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (135, 3144, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (136, 3145, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (137, 3146, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (138, 3147, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (139, 3148, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (140, 3146, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (141, 3147, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (142, 3148, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (143, 3149, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (144, 3149, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (145, 3150, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (146, 3150, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (147, 3151, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (148, 3151, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (149, 3150, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (150, 3152, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (151, 3153, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (152, 3155, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (153, 3154, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (154, 3156, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (155, 3154, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (156, 3152, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (157, 3153, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (158, 3154, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (159, 3152, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (160, 3157, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (161, 3158, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (162, 3157, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (163, 3158, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (164, 3159, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (165, 3161, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (166, 3160, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (167, 3162, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (168, 3163, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (169, 3163, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (170, 3164, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (171, 3164, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (172, 3165, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (173, 3166, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (174, 3167, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (175, 3168, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (176, 3168, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (177, 3169, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (178, 3170, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (179, 3170, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (180, 3170, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (181, 3171, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (182, 3172, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (183, 3172, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (184, 3171, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (185, 3174, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (186, 3173, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (187, 3171, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (188, 3173, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (189, 3173, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (190, 3175, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (191, 3176, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (192, 3177, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (193, 3176, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (194, 3177, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (195, 3178, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (196, 3179, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (197, 3178, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (198, 3179, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (199, 3180, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (200, 3181, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (201, 3183, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (202, 3182, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (203, 3181, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (204, 3184, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (205, 3183, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (206, 3182, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (207, 3184, 4, 0)
GO
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (208, 3185, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (209, 3186, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (210, 3187, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (211, 3188, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (212, 3189, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (213, 3190, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (214, 3188, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (215, 3191, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (216, 3189, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (217, 3190, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (218, 3191, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (219, 3192, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (220, 3193, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (221, 3192, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (222, 3193, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (223, 3194, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (224, 3196, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (225, 3195, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (226, 3198, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (227, 3197, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (228, 3195, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (229, 3196, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (230, 3197, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (231, 3197, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (232, 3199, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (233, 3200, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (234, 3199, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (235, 3200, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (236, 3201, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (237, 3203, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (238, 3202, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (239, 3204, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (240, 3201, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (241, 3205, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (242, 3202, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (243, 3206, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (244, 3206, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (245, 3208, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (246, 3208, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (247, 3207, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (248, 3207, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (249, 3209, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (250, 3210, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (251, 3211, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (252, 3212, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (253, 3211, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (254, 3212, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (255, 3213, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (256, 3213, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (257, 3216, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (258, 3218, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (259, 3217, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (260, 3218, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (261, 3216, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (262, 3219, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (263, 3218, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (264, 3220, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (265, 3221, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (266, 3220, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (267, 3222, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (268, 3224, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (269, 3221, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (270, 3222, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (271, 3221, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (272, 3224, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (273, 3225, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (274, 3225, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (275, 3226, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (276, 3225, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (277, 3226, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (278, 3227, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (279, 3229, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (280, 3228, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (281, 3229, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (282, 3231, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (283, 3230, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (284, 3233, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (285, 3232, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (286, 3231, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (287, 3233, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (288, 3232, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (289, 3234, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (290, 3235, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (291, 3237, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (292, 3236, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (293, 3238, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (294, 3236, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (295, 3235, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (296, 3235, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (297, 3239, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (298, 3240, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (299, 3240, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (300, 3241, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (301, 3241, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (302, 3242, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (303, 3242, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (304, 3244, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (305, 3245, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (306, 3243, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (307, 3247, 1, 0)
GO
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (308, 3246, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (309, 3244, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (310, 3245, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (311, 3247, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (312, 3245, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (313, 3249, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (314, 3248, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (315, 3249, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (316, 3248, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (317, 3248, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (318, 3251, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (319, 3252, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (320, 3254, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (321, 3253, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (322, 3251, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (323, 3254, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (324, 3253, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (325, 3255, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (326, 3255, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (327, 3256, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (328, 3257, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (329, 3258, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (330, 3259, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (331, 3260, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (332, 3257, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (333, 3258, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (334, 3259, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (335, 3261, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (336, 3261, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (337, 3262, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (338, 3262, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (339, 3264, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (340, 3263, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (341, 3265, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (342, 3263, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (343, 3266, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (344, 3267, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (345, 3268, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (346, 3267, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (347, 3269, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (348, 3270, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (349, 3273, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (350, 3272, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (351, 3275, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (352, 3271, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (353, 3274, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (354, 3273, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (355, 3277, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (356, 3277, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (357, 3278, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (358, 3277, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (359, 3282, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (360, 3279, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (361, 3280, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (362, 3281, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (363, 3279, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (364, 3280, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (365, 3281, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (366, 3279, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (367, 3283, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (368, 3284, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (369, 3283, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (370, 3285, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (371, 3285, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (372, 3285, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (373, 3286, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (374, 3287, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (375, 3288, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (376, 3287, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (377, 3289, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (378, 3291, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (379, 3290, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (380, 3292, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (381, 3291, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (382, 3290, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (383, 3295, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (384, 3293, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (385, 3294, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (386, 3295, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (387, 3296, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (388, 3293, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (389, 3294, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (390, 3296, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (391, 3297, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (392, 3298, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (393, 3299, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (394, 3298, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (395, 3300, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (396, 3301, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (397, 3302, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (398, 3301, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (399, 3302, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (400, 3303, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (401, 3303, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (402, 3304, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (403, 3305, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (404, 3306, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (405, 3305, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (406, 3306, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (407, 3306, 4, 0)
GO
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (408, 3307, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (409, 3308, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (410, 3310, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (411, 3311, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (412, 3311, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (413, 3311, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (414, 3312, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (415, 3314, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (416, 3313, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (417, 3315, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (418, 3314, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (419, 3313, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (420, 3316, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (421, 3317, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (422, 3318, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (423, 3319, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (424, 3319, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (425, 3320, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (426, 3321, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (427, 3322, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (428, 3323, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (429, 3320, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (430, 3324, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (431, 3321, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (432, 3323, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (433, 3325, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (434, 3325, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (435, 3325, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (436, 3327, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (437, 3326, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (438, 3328, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (439, 3330, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (440, 3330, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (441, 3333, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (442, 3334, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (443, 3333, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (444, 3336, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (445, 3337, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (446, 3336, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (447, 3338, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (448, 3338, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (449, 3339, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (450, 3343, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (451, 3342, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (452, 3345, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (453, 3344, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (454, 3342, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (455, 3344, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (456, 3346, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (457, 3347, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (458, 3348, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (459, 3348, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (460, 3350, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (461, 3349, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (462, 3351, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (463, 3349, 2, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (464, 3353, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (465, 3353, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (466, 3354, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (467, 3355, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (468, 3354, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (469, 3356, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (470, 3356, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (471, 3358, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (472, 3357, 1, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (473, 3357, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (474, 3358, 4, 0)
INSERT [dbo].[Alerts] ([Id], [MonitoringId], [AlertTypeId], [Deleted]) VALUES (475, 3359, 4, 0)
SET IDENTITY_INSERT [dbo].[Alerts] OFF
GO
SET IDENTITY_INSERT [dbo].[AlertTypes] ON 

INSERT [dbo].[AlertTypes] ([Id], [Name], [Deleted]) VALUES (1, N'Temperatura Elevada', 0)
INSERT [dbo].[AlertTypes] ([Id], [Name], [Deleted]) VALUES (2, N'Baixa Umidade', 0)
INSERT [dbo].[AlertTypes] ([Id], [Name], [Deleted]) VALUES (3, N'Fogo detectado', 0)
INSERT [dbo].[AlertTypes] ([Id], [Name], [Deleted]) VALUES (4, N'Radiação UV Elevada', 0)
INSERT [dbo].[AlertTypes] ([Id], [Name], [Deleted]) VALUES (5, N'Fumaça Detectada', 0)
INSERT [dbo].[AlertTypes] ([Id], [Name], [Deleted]) VALUES (6, N'Gases Tóxicos Detectados', 0)
SET IDENTITY_INSERT [dbo].[AlertTypes] OFF
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'466f78ad-aaa7-4fd2-bf86-4a5f35a45900', N'Cell', N'cell', N'9adebe8c-2a14-44d3-a2eb-2c9ec13ae4d9')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'9a3e5363-e228-4aaa-929f-4454e52c7fcf', N'Admin', N'admin', N'3246056c-ea67-4293-abd2-322033f2e555')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'd9a1d3f6-b967-4bdd-a203-21e18ce32d00', N'466f78ad-aaa7-4fd2-bf86-4a5f35a45900')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2f8e07ca-5443-4bea-8dcf-c6621cdf619a', N'9a3e5363-e228-4aaa-929f-4454e52c7fcf')
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Name]) VALUES (N'2f8e07ca-5443-4bea-8dcf-c6621cdf619a', N'admin', N'admin', NULL, NULL, 1, N'AQAAAAEAACcQAAAAEBqTIA3RGPKcIC6VCaQApo9gbU8GRs60mzu0jcPcW4s2HzUF+r22tQXnzfq+p/4gig==', N'00000000-0000-0000-0000-000000000000', N'456b83cf-cc2a-4eab-b2a6-5610047023aa', NULL, 0, 0, NULL, 0, 0, N'Admin')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Name]) VALUES (N'd9a1d3f6-b967-4bdd-a203-21e18ce32d00', N'cell', N'cell', NULL, NULL, 1, N'AQAAAAEAACcQAAAAENFkWhyVZ50cssZFojGT76nLjD+IWC8NPUuORMtugWMt7ed7E4MtQvkvmLg/oX0cjQ==', N'00000000-0000-0000-0000-000000000000', N'eb8d0532-0f5e-4c27-865c-af644f1c0af4', NULL, 0, 0, NULL, 0, 0, N'Cell')
GO
SET IDENTITY_INSERT [dbo].[Cells] ON 

INSERT [dbo].[Cells] ([Id], [Name], [Address], [Latitude], [Longitude], [Deleted]) VALUES (1, N'Teste Editado de novo', N'Mew Mure Mur Mew Editado', 12.25, -0.87, 1)
INSERT [dbo].[Cells] ([Id], [Name], [Address], [Latitude], [Longitude], [Deleted]) VALUES (2, N'Com filtro Model State', N'Testando com filtro de Model State [Editado]', 14.57, -13857, 0)
INSERT [dbo].[Cells] ([Id], [Name], [Address], [Latitude], [Longitude], [Deleted]) VALUES (1002, N'Repositório Refatorado [Editado]', N'Refatorado', 18, 23.589, 1)
INSERT [dbo].[Cells] ([Id], [Name], [Address], [Latitude], [Longitude], [Deleted]) VALUES (2002, N'Célula Virtual 01', N'Endereço Virtual 01', 18, 12, 0)
INSERT [dbo].[Cells] ([Id], [Name], [Address], [Latitude], [Longitude], [Deleted]) VALUES (2003, N'Célula Virtual 02', N'Endereço Virtual 02', 0, 0, 0)
INSERT [dbo].[Cells] ([Id], [Name], [Address], [Latitude], [Longitude], [Deleted]) VALUES (2004, N'Célula Virtual 03', N'Endereço Virtual 03', 12, -15, 0)
INSERT [dbo].[Cells] ([Id], [Name], [Address], [Latitude], [Longitude], [Deleted]) VALUES (2005, N'Célula Virtual 04', N'Endereço Virtual 04', 0, 0, 0)
INSERT [dbo].[Cells] ([Id], [Name], [Address], [Latitude], [Longitude], [Deleted]) VALUES (2006, N'Célula Virtual 05', N'Endereço Virtual 05', 0, 0, 0)
INSERT [dbo].[Cells] ([Id], [Name], [Address], [Latitude], [Longitude], [Deleted]) VALUES (2007, N'Célula Virtual 06', N'Endereço Virtual 06', 12, -8, 0)
SET IDENTITY_INSERT [dbo].[Cells] OFF
GO
SET IDENTITY_INSERT [dbo].[MonitoringData] ON 

INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1041, 2004, 2004, 40.67735, 0.819702864, 43.7687454, 31, 870, 4.511494, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1042, 2004, 2002, 29.68228, 0.9690731, 40.8774834, 441, 762, 6.76786661, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1043, 2004, 2, 41.8247032, 0.06446725, 47.4304657, 557, 296, 7.1143856, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1044, 2004, 2003, 42.19142, 0.6261899, 46.9514732, 178, 521, 7.162546, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1045, 2004, 2007, 37.80077, 0.3372948, 30.3335342, 950, 327, 4.75934935, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1046, 2004, 2006, 32.00022, 0.4042883, 33.7540855, 23, 219, 6.0163393, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1047, 2004, 2005, 40.5426445, 0.100937612, 47.5140839, 838, 637, 7.85865927, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1048, 2005, 2, 38.49802, 0.4486133, 41.4280434, 316, 592, 7.202619, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1049, 2005, 2006, 47.67847, 0.3371975, 32.3543625, 330, 871, 5.05954266, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1050, 2005, 2004, 47.31067, 0.8598602, 28.7086468, 866, 635, 6.649368, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1051, 2005, 2007, 39.28098, 0.581414044, 45.6528664, 818, 275, 4.388055, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1052, 2005, 2002, 40.9577637, 0.3384903, 27.0968647, 956, 842, 7.42908, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1053, 2005, 2005, 42.9217377, 0.923860133, 43.26414, 77, 987, 6.792119, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1054, 2005, 2003, 39.74125, 0.44430086, 29.9824066, 106, 275, 6.106738, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1055, 2006, 2, 49.12493, 0.90661937, 33.8920441, 763, 164, 6.07405138, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1056, 2006, 2003, 27.4868412, 0.8718464, 33.98096, 721, 68, 6.58559847, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1057, 2006, 2007, 36.00033, 0.3310826, 36.614727, 633, 555, 4.3837924, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1058, 2006, 2002, 31.806797, 0.4498114, 33.24046, 483, 752, 5.33424139, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1059, 2006, 2006, 47.53049, 0.118473805, 41.08223, 821, 279, 7.40570927, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1060, 2006, 2004, 44.40393, 0.8020851, 34.6713943, 370, 215, 7.372257, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1061, 2006, 2005, 35.31653, 0.06174543, 30.23172, 301, 613, 6.45403957, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1062, 2007, 2, 37.325676, 0.182621419, 36.48828, 666, 732, 5.66518, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1063, 2007, 2002, 32.42134, 0.93474555, 41.5839272, 666, 159, 4.90418148, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1064, 2007, 2005, 37.5177536, 0.04043017, 35.78983, 465, 143, 7.900679, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1065, 2007, 2006, 33.48576, 0.424805462, 29.1816959, 52, 260, 6.549659, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1066, 2007, 2004, 45.9760361, 0.260117859, 31.28276, 462, 799, 6.549659, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1067, 2007, 2007, 28.159956, 0.2524861, 39.37871, 738, 644, 4.470353, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1068, 2007, 2003, 43.1955643, 0.185951635, 42.3584442, 549, 688, 4.647665, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1069, 2008, 2, 45.3851, 0.269700468, 31.3593235, 854, 209, 7.352612, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1070, 2008, 2005, 28.3394928, 0.29406634, 27.3671741, 438, 308, 6.89805, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1071, 2008, 2002, 27.4067039, 0.86835444, 48.5895576, 215, 699, 5.65446138, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1072, 2008, 2004, 36.1387024, 0.784795046, 36.8422775, 271, 439, 6.4466815, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1073, 2008, 2006, 40.7007332, 0.410829425, 33.88493, 215, 77, 7.043318, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1074, 2008, 2007, 27.4067039, 0.133586541, 40.4039955, 716, 457, 4.687505, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1075, 2008, 2003, 48.355217, 0.87489444, 41.0985031, 251, 966, 5.463232, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1076, 2009, 2005, 35.7646675, 0.500700355, 47.24969, 994, 152, 5.552257, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1077, 2009, 2006, 39.3466263, 0.181297675, 27.5984612, 589, 160, 7.123985, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1078, 2009, 2004, 45.4929161, 0.728562653, 26.3826447, 815, 636, 7.36258936, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1079, 2009, 2007, 39.541008, 0.373152524, 33.7851524, 295, 940, 5.74127, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1080, 2009, 2, 31.7509727, 0.769224465, 39.5774956, 843, 609, 4.830484, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1081, 2009, 2002, 41.7800331, 0.553390741, 32.84475, 803, 335, 7.23307371, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1082, 2009, 2003, 28.2803726, 0.578285933, 34.2323723, 316, 765, 5.56730843, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1083, 2010, 2007, 29.7873688, 0.391968757, 31.5254364, 561, 678, 7.679834, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1084, 2010, 2006, 29.9305515, 0.3186656, 36.37485, 191, 233, 6.86919069, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1085, 2010, 2004, 32.86983, 0.37387386, 26.8401928, 837, 701, 6.76573563, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1086, 2010, 2002, 33.90644, 0.186923653, 27.5130577, 163, 480, 4.98459625, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1087, 2010, 2005, 45.8850479, 0.3556052, 40.7703133, 625, 393, 5.407732, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1088, 2010, 2, 35.5592957, 0.3977683, 26.313858, 109, 234, 5.335845, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1089, 2010, 2003, 48.21718, 0.17308718, 35.8594971, 140, 61, 4.43586254, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1090, 2011, 2004, 29.5744324, 0.963402, 43.5760651, 747, 315, 6.67028046, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1091, 2011, 2003, 35.8592834, 0.7692163, 47.9042435, 242, 736, 5.990622, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1092, 2011, 2007, 30.5885925, 0.837207, 46.1566544, 305, 290, 5.09106159, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1093, 2011, 2006, 27.9859619, 0.08323155, 32.3432961, 235, 747, 7.11976767, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1094, 2011, 2005, 47.99741, 0.646582663, 45.0453, 549, 915, 4.843112, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1095, 2011, 2002, 37.25204, 0.5762034, 28.4896088, 315, 567, 7.69822359, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1096, 2011, 2, 48.7910423, 0.5086553, 33.16108, 225, 224, 7.252987, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1097, 2012, 2005, 33.877243, 0.454216421, 35.3291779, 372, 206, 6.78424644, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1098, 2012, 2004, 35.8818359, 0.04496968, 38.6727943, 993, 440, 6.081046, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1099, 2012, 2003, 39.1670151, 0.8999363, 35.84334, 602, 544, 6.663676, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1100, 2012, 2002, 48.030983, 0.816123545, 35.52427, 679, 272, 7.080157, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1101, 2012, 2, 39.801075, 0.5755768, 35.67386, 987, 261, 7.13174868, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1102, 2012, 2007, 44.4859543, 0.700402558, 48.328064, 238, 304, 6.9243536, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1103, 2012, 2006, 27.6430454, 0.984688163, 26.3697853, 227, 855, 5.648824, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1104, 2013, 2003, 27.94333, 0.5815022, 43.44246, 639, 902, 7.60553837, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1105, 2013, 2006, 35.78583, 0.2397258, 42.8167076, 152, 811, 8.078585, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1106, 2013, 2004, 42.50159, 0.177261144, 27.0725975, 239, 260, 7.410114, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1107, 2013, 2, 45.95811, 0.4944993, 29.7072144, 191, 821, 6.727951, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1108, 2013, 2002, 38.9264641, 0.381438762, 42.9261169, 397, 608, 5.933389, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1109, 2013, 2005, 34.51459, 0.552033842, 44.767765, 199, 839, 7.796563, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1110, 2013, 2007, 39.9449463, 0.133564055, 39.5146561, 580, 675, 6.747357, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1111, 2014, 2004, 47.4598, 0.2516502, 36.20769, 273, 817, 4.599924, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1112, 2014, 2006, 35.01567, 0.405943334, 47.77504, 200, 192, 6.065338, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1113, 2014, 2002, 27.7807579, 0.093548134, 46.21031, 668, 913, 6.636802, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1114, 2014, 2, 45.2026062, 0.7887997, 27.46196, 317, 13, 5.25419855, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1115, 2014, 2003, 47.4598, 0.175266713, 36.20769, 273, 612, 7.519989, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1116, 2014, 2005, 30.01576, 0.5772371, 35.8592453, 905, 719, 4.74315, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1117, 2014, 2007, 27.1928272, 0.762736261, 44.0006676, 848, 537, 7.214359, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1118, 2015, 2003, 42.39529, 0.422203183, 32.2299538, 477, 565, 6.285112, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1119, 2015, 2005, 28.74068, 0.9965149, 28.6807766, 294, 935, 5.58975124, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1120, 2015, 2006, 31.8802261, 0.926311851, 36.56593, 446, 97, 5.43179846, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1121, 2015, 2, 42.1278343, 0.821056, 34.86427, 887, 653, 4.1061945, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1122, 2015, 2004, 27.7105083, 0.884862959, 47.4986267, 242, 92, 5.26554775, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1123, 2015, 2007, 26.9604321, 0.7145917, 32.93798, 125, 390, 4.77970171, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (1124, 2015, 2002, 36.3001823, 0.8840959, 36.88237, 297, 402, 6.87428331, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2002, 3002, 2006, 28.2435055, 0.6647463, 27.514, 756, 619, 1.75815353E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2003, 3002, 2007, 25.791111, 0.267934561, 48.3996353, 845, 862, 1.60927607E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2004, 3002, 2, 19.4710369, 0.00633496372, 27.7952175, 505, 235, 1.950069E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2005, 3002, 2003, 21.4011459, 0.133098409, 43.1749268, 168, 423, 2.52399764E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2006, 3002, 2002, 28.1657372, 0.247336224, 31.4680462, 62, 373, 2.3492446E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2007, 3002, 2005, 23.9903545, 0.767536342, 27.514, 802, 687, 2.71120371E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2008, 3002, 2004, 22.3479481, 0.4860397, 28.5782452, 59, 796, 1.91191953E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2009, 3003, 2005, 20.6471844, 0.0809925, 30.3516064, 165, 649, 2.21664641E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2010, 3003, 2004, 28.2970753, 0.5518055, 32.3543358, 299, 743, 2.29597231E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2011, 3003, 2002, 23.6462479, 0.186794132, 28.182621, 683, 365, 1.72897182E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2012, 3003, 2, 23.4870262, 0.07073499, 33.0195274, 376, 171, 1.94067934E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2013, 3003, 2003, 22.0686131, 0.524836063, 26.5179234, 928, 923, 2.71017234E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2014, 3003, 2006, 31.1950626, 0.250266433, 29.1021767, 46, 896, 2.59423359E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2015, 3003, 2007, 24.5709343, 0.6801778, 37.6838455, 106, 614, 2.3513187E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2016, 3004, 2002, 22.6777782, 0.5989421, 32.40537, 87, 998, 2.05168817E-06, 0)
GO
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2017, 3004, 2004, 19.1804771, 0.175140321, 33.27035, 685, 213, 2.46494574E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2018, 3004, 2005, 21.5556183, 0.117662556, 48.19167, 327, 670, 2.48614674E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2019, 3004, 2003, 24.2056179, 0.9031643, 48.7206268, 640, 493, 2.10918938E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2020, 3004, 2007, 23.73886, 0.2929073, 30.66969, 81, 38, 2.6805194E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2021, 3004, 2, 19.60202, 0.346714258, 47.6939659, 973, 591, 1.940341E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2022, 3004, 2006, 29.86933, 0.208800241, 31.316267, 801, 905, 1.63864706E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2023, 3005, 2007, 30.3461685, 0.8087206, 30.7120342, 37, 893, 2.82184851E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2024, 3005, 2006, 20.3875332, 0.553731263, 45.1969643, 420, 565, 2.63625475E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2025, 3005, 2005, 24.8129654, 0.09649273, 34.75106, 486, 210, 2.65386961E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2026, 3005, 2, 21.6646481, 0.8907418, 42.9160767, 639, 427, 1.61909418E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2027, 3005, 2004, 18.2628288, 0.195079789, 47.73212, 78, 389, 2.38380676E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2028, 3005, 2003, 24.0971012, 0.9161766, 48.4089622, 383, 265, 2.3363757E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2029, 3005, 2002, 25.78421, 0.6311249, 47.67791, 202, 351, 1.72411137E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2030, 3006, 2, 27.7192955, 0.23646906, 42.1448936, 487, 995, 2.6107E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2031, 3006, 2004, 22.6230545, 0.6372158, 29.7879181, 394, 871, 1.66595635E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2032, 3006, 2007, 19.1579, 0.540377736, 39.8148766, 307, 269, 2.50963171E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2033, 3006, 2002, 27.1525669, 0.6451089, 34.4671631, 455, 865, 2.18884475E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2034, 3006, 2003, 30.6239777, 0.661694, 26.841217, 647, 694, 2.93754852E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2035, 3006, 2006, 30.6239777, 0.531543553, 34.2193336, 647, 694, 2.368177E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2036, 3006, 2005, 26.5924, 0.909007132, 34.8173523, 71, 36, 2.30656269E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2037, 3007, 2006, 18.2912121, 0.179814592, 27.923336, 726, 23, 2.53146186E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2038, 3007, 2004, 21.787466, 0.285782039, 29.6935863, 401, 930, 2.85035071E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2039, 3007, 2003, 29.9070759, 0.129791051, 34.8262672, 64, 70, 1.748715E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2040, 3007, 2005, 31.055069, 0.7360726, 26.72239, 571, 594, 1.93132178E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2041, 3007, 2002, 25.8236141, 0.358187616, 29.6296844, 152, 594, 2.354467E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2042, 3007, 2, 23.5302067, 0.9217463, 39.5588341, 330, 700, 2.660035E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2043, 3007, 2007, 29.1308136, 0.9022764, 43.63055, 333, 387, 2.71747831E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2044, 3008, 2006, 22.2263165, 0.59979707, 48.3903046, 919, 50, 2.9160642E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2045, 3008, 2005, 18.7404861, 0.8106911, 26.2565842, 860, 956, 2.09587574E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2046, 3008, 2002, 18.7404861, 0.0205056723, 48.483696, 860, 956, 2.04203775E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2047, 3008, 2004, 28.05637, 0.849437654, 48.36425, 541, 381, 2.66384541E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2048, 3008, 2007, 19.23396, 0.767093539, 32.72012, 295, 180, 2.76149945E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2049, 3008, 2, 19.4538116, 0.76157105, 46.04474, 201, 740, 2.67163068E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2050, 3008, 2003, 21.24617, 0.04450937, 44.31721, 26, 231, 2.59402418E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2051, 3009, 2002, 27.4366531, 0.204827413, 47.8672371, 89, 477, 2.640882E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2052, 3009, 2005, 26.1946411, 0.38703692, 43.519207, 40, 608, 2.055029E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2053, 3009, 2003, 30.6037312, 0.09354301, 44.0048828, 597, 103, 1.66556515E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2054, 3009, 2007, 24.2229271, 0.80128634, 39.22114, 60, 839, 1.81637529E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2055, 3009, 2004, 18.57459, 0.94942826, 42.5081444, 619, 411, 1.95004986E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2056, 3009, 2006, 31.117506, 0.640761554, 28.227, 860, 484, 2.04790922E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2057, 3009, 2, 28.7576523, 0.180890784, 38.27998, 72, 899, 2.013282E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2058, 3010, 2, 23.99053, 0.292033672, 28.4526367, 81, 366, 1.72206546E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2059, 3010, 2002, 19.8110256, 0.9338428, 42.06699, 556, 506, 1.78694688E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2060, 3010, 2004, 22.490633, 0.133682236, 46.9888077, 85, 237, 2.6211826E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2061, 3010, 2007, 19.279808, 0.0668104738, 35.7436066, 807, 972, 2.91606034E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2062, 3010, 2005, 21.3005238, 0.906513035, 41.37901, 245, 938, 2.91552465E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2063, 3010, 2006, 19.39516, 0.321870983, 48.2879639, 109, 433, 2.65983431E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2064, 3010, 2003, 21.0285988, 0.159549251, 43.7482872, 913, 617, 2.55997338E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2065, 3011, 2007, 23.02906, 0.341883063, 43.4306, 681, 974, 2.57435067E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2066, 3011, 2003, 27.2034512, 0.0305266287, 48.514946, 777, 197, 2.45070532E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2067, 3011, 2006, 24.2975826, 0.361160368, 26.5348415, 754, 134, 2.27668352E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2068, 3011, 2004, 31.1165962, 0.161964461, 39.5886574, 569, 92, 2.7437693E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2069, 3011, 2005, 28.3038063, 0.423924476, 46.23418, 112, 406, 2.8056802E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2070, 3011, 2002, 27.3114452, 0.9509084, 30.26788, 501, 793, 2.84357543E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2071, 3011, 2, 20.9114037, 0.2559023, 36.6301537, 840, 308, 2.08162282E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2072, 3012, 2007, 18.95314, 0.7645598, 33.64017, 688, 71, 2.27837768E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2073, 3012, 2006, 28.0112457, 0.7429318, 47.6648178, 360, 25, 1.71615443E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2074, 3012, 2004, 16.9340343, 0.08871923, 39.59331, 121, 636, 2.057969E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2075, 3012, 2, 27.1688919, 0.620998859, 46.2437668, 784, 573, 2.34601066E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2076, 3012, 2005, 21.3880424, 0.9341996, 44.3812675, 480, 334, 2.52856944E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2077, 3012, 2003, 30.6250229, 0.8882598, 39.12041, 212, 777, 2.2341867E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2078, 3012, 2002, 27.1688919, 0.620998859, 46.2437668, 784, 466, 2.363521E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2079, 3013, 2006, 26.1885967, 0.8656623, 37.1282272, 574, 421, 2.45672732E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2080, 3013, 2, 25.4989643, 0.7237628, 26.4172649, 706, 914, 2.42956116E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2081, 3013, 2005, 26.4294415, 0.4298559, 29.8769569, 783, 185, 1.94179052E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2082, 3013, 2007, 30.7943573, 0.1030121, 27.7817478, 942, 401, 2.38525581E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2083, 3013, 2003, 22.9965324, 0.92598, 29.482029, 116, 53, 1.61001276E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2084, 3013, 2002, 28.6680126, 0.7321274, 32.128437, 112, 205, 1.743656E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2085, 3013, 2004, 19.6029434, 0.286393076, 40.3221474, 1, 989, 1.71062311E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2086, 3014, 2, 17.8428459, 0.291820854, 38.17984, 284, 204, 2.65672338E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2087, 3014, 2007, 19.2116127, 0.5673627, 41.5075836, 675, 490, 2.795722E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2088, 3014, 2004, 21.5464115, 0.5870671, 41.67234, 113, 681, 1.91695381E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2089, 3014, 2006, 17.4970226, 0.292453647, 31.1010437, 265, 319, 2.75872185E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2090, 3014, 2005, 19.0554047, 0.7268707, 47.1350174, 671, 949, 2.641352E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2091, 3014, 2003, 17.5330868, 0.77285105, 46.5493927, 213, 293, 2.07463745E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2092, 3014, 2002, 20.90572, 0.05694032, 47.3231354, 999, 640, 1.651563E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2093, 3015, 2005, 19.8697262, 0.91176194, 41.94139, 575, 184, 2.913347E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2094, 3015, 2, 22.967411, 0.0684920847, 42.07749, 596, 431, 2.227784E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2095, 3015, 2006, 28.4616337, 0.00733427657, 36.63009, 270, 530, 1.72383636E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2096, 3015, 2003, 20.9188538, 0.589617133, 41.4591446, 556, 471, 2.63029665E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2097, 3015, 2002, 30.47939, 0.7041526, 34.6625633, 373, 361, 2.15136629E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2098, 3015, 2007, 28.4616337, 0.328904539, 36.63009, 270, 15, 1.83291093E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2099, 3015, 2004, 18.93344, 0.31408295, 29.0478249, 589, 252, 2.40909731E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2100, 3016, 2005, 17.2469177, 0.763278544, 37.5504723, 65, 573, 1.85523379E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2101, 3016, 2007, 22.1430969, 0.9314864, 39.6977768, 142, 27, 2.92576942E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2102, 3016, 2002, 23.4109478, 0.3891157, 43.29436, 868, 894, 1.87633543E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2103, 3016, 2006, 26.9349232, 0.959987462, 37.02469, 859, 87, 1.63930918E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2104, 3016, 2, 29.245533, 0.167476267, 40.394577, 98, 81, 1.98787529E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2105, 3016, 2004, 20.1779079, 0.5243037, 48.3152847, 854, 831, 1.61995672E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2106, 3016, 2003, 26.6698341, 0.9678229, 37.00309, 406, 386, 2.44704916E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2107, 3017, 2006, 17.3593445, 0.5585812, 46.4306068, 54, 74, 2.68234953E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2108, 3017, 2002, 19.4881783, 0.333104163, 33.1996155, 775, 374, 2.731589E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2109, 3017, 2005, 28.993124, 0.8926431, 42.9605179, 245, 220, 1.79545259E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2110, 3017, 2, 17.3593445, 0.5585812, 46.4306068, 54, 272, 2.77317918E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2111, 3017, 2003, 23.98713, 0.7037105, 46.4471, 390, 260, 1.75749813E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2112, 3017, 2004, 17.7102661, 0.5287016, 28.4079418, 84, 547, 2.688521E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2113, 3017, 2007, 30.23213, 0.4636622, 37.1937065, 620, 42, 1.95052735E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2114, 3018, 2002, 28.7499371, 0.885511041, 46.95661, 673, 778, 2.92420646E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2115, 3018, 2004, 30.7098541, 0.482164234, 37.882885, 355, 934, 2.13622729E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2116, 3018, 2006, 29.9366531, 0.6130732, 30.59712, 495, 958, 2.15108662E-06, 0)
GO
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2117, 3018, 2007, 17.6215172, 0.348001957, 40.05589, 861, 246, 1.89050786E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2118, 3018, 2003, 20.5845242, 0.295672774, 41.054493, 486, 32, 1.89061814E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2119, 3018, 2, 28.9255466, 0.592981, 29.5082455, 473, 900, 2.751899E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2120, 3018, 2005, 19.9728642, 0.0482535921, 27.1532326, 633, 699, 1.913056E-06, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2121, 3019, 2003, 20.71304, 0.8253096, 38.54353, 372, 433, 1.91637528, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2122, 3019, 2007, 18.8898754, 0.911217451, 36.83022, 562, 567, 1.87847972, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2123, 3019, 2, 21.9813118, 0.7054486, 46.89844, 425, 742, 2.15237355, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2124, 3019, 2004, 22.5859985, 0.170079857, 38.9459763, 210, 25, 1.57312489, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2125, 3019, 2006, 23.7239037, 0.0264804438, 31.1276283, 622, 15, 1.67117286, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2126, 3019, 2002, 18.0913429, 0.6572074, 34.1609268, 153, 121, 1.32500052, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2127, 3019, 2005, 15.7581282, 0.2204178, 34.45306, 980, 52, 1.63114786, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2128, 3020, 2002, 21.82217, 0.188902, 48.7482, 244, 417, 1.2877574, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2129, 3020, 2, 22.27875, 0.644518435, 36.081852, 510, 416, 2.2114408, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2130, 3020, 2003, 21.1177483, 0.188956335, 39.2387123, 251, 645, 1.32746911, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2131, 3020, 2004, 17.3526974, 0.209228218, 26.93781, 793, 315, 2.26544452, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2132, 3020, 2007, 22.71789, 0.399789363, 40.51695, 690, 271, 1.23161149, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2133, 3020, 2006, 25.1180553, 0.536672652, 46.4287949, 179, 372, 1.63778329, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2134, 3020, 2005, 18.5413933, 0.466189653, 36.83627, 430, 394, 1.93230355, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2135, 3021, 2007, 21.8176079, 0.373084545, 42.09734, 472, 542, 1.27925444, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2136, 3021, 2002, 23.0886059, 0.0162669476, 37.5091858, 582, 633, 2.033823, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2137, 3021, 2, 25.2091026, 0.450206816, 29.5640755, 973, 716, 1.32977211, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2138, 3021, 2005, 16.23834, 0.6436584, 26.2874374, 468, 610, 1.73859668, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2139, 3021, 2006, 25.7370949, 0.5449211, 31.1532536, 893, 10, 1.23173034, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2140, 3021, 2003, 25.1441746, 0.724865556, 27.3972931, 815, 781, 1.64305723, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2141, 3021, 2004, 18.9878635, 0.719798148, 45.93969, 289, 897, 1.58235538, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2142, 3022, 2004, 16.10599, 0.6768442, 36.8199844, 354, 177, 2.19492817, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2143, 3022, 2, 16.8733177, 0.318468541, 30.81034, 478, 755, 1.57357144, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2144, 3022, 2002, 14.6962729, 0.8731824, 26.9608135, 94, 531, 1.446723, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2145, 3022, 2003, 22.5888424, 0.4787656, 40.5913124, 997, 763, 1.33896291, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2146, 3022, 2006, 24.6799, 0.06362063, 42.6745, 55, 555, 1.22730243, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2147, 3022, 2005, 20.7646542, 0.797397852, 27.0969315, 548, 819, 1.647999, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2148, 3022, 2007, 25.76086, 0.220685348, 31.7694626, 841, 85, 1.95646369, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2149, 3023, 2004, 23.4224262, 0.450944424, 46.6752548, 490, 861, 1.39558244, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2150, 3023, 2007, 14.8734722, 0.238209531, 35.3584251, 505, 17, 1.99373007, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2151, 3023, 2006, 23.66423, 0.5202446, 43.6339722, 138, 269, 1.86572468, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2152, 3023, 2005, 15.8807859, 0.3916966, 32.0282173, 899, 848, 1.7322681, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2153, 3023, 2, 21.1785488, 0.2469984, 32.0758858, 679, 225, 1.37901282, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2154, 3023, 2003, 16.5832748, 0.168566868, 45.43911, 966, 716, 2.26553035, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2155, 3023, 2002, 24.09259, 0.2466293, 26.57349, 539, 571, 1.60447192, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2156, 3024, 2002, 15.8947525, 0.133295134, 27.1069336, 586, 695, 2.169602, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2157, 3024, 2003, 24.0166664, 0.9796627, 44.358387, 151, 436, 1.51725578, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2158, 3024, 2007, 18.2956352, 0.144636363, 38.42052, 298, 233, 1.647625, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2159, 3024, 2005, 24.9757175, 0.419972062, 47.9476128, 575, 708, 1.55417836, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2160, 3024, 2, 18.5990467, 0.761409044, 35.4190331, 685, 123, 2.14146519, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2161, 3024, 2004, 20.9086189, 0.506888747, 44.74342, 84, 467, 1.98652685, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2162, 3024, 2006, 15.4666157, 0.787480533, 26.6845722, 563, 94, 1.92889965, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2163, 3025, 2007, 18.285902, 0.775975, 35.0781136, 225, 622, 1.59526873, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2164, 3025, 2, 20.9107323, 0.924938858, 45.4904671, 893, 998, 2.13075233, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2165, 3025, 2004, 16.072176, 0.912774, 36.3317566, 868, 272, 2.23362422, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2166, 3025, 2005, 18.6436234, 0.570294857, 35.4415932, 409, 329, 1.46541381, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2167, 3025, 2003, 17.1858311, 0.315067321, 48.37246, 251, 127, 1.29947627, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2168, 3025, 2002, 22.6048145, 0.9352948, 45.29245, 26, 279, 1.70429683, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2169, 3025, 2006, 20.827177, 0.7778415, 39.54626, 190, 830, 2.11151958, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2170, 3026, 2003, 20.6783714, 0.101190194, 27.7588787, 822, 751, 1.38990557, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2171, 3026, 2007, 23.0411777, 0.45321852, 41.0441132, 154, 692, 2.226026, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2172, 3026, 2006, 19.1926785, 0.522318, 39.1778221, 514, 241, 1.43648922, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2173, 3026, 2002, 20.567627, 0.681572556, 31.4973183, 349, 277, 1.66772759, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2174, 3026, 2005, 24.4684219, 0.5327926, 26.2538471, 213, 47, 1.69304848, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2175, 3026, 2, 15.4228649, 0.048287794, 33.9501228, 405, 154, 2.05888867, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2176, 3026, 2004, 18.5755157, 0.7643929, 32.0545731, 820, 684, 1.95144761, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2177, 3027, 2003, 19.88004, 0.6896092, 37.8428268, 911, 190, 1.4198736, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2178, 3027, 2006, 22.3966084, 0.3552245, 29.7672443, 777, 412, 2.22854757, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2179, 3027, 2004, 18.6728745, 0.0182489753, 43.563324, 706, 65, 2.036761, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2180, 3027, 2, 22.9206657, 0.09203489, 30.6764317, 738, 662, 2.10071468, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2181, 3027, 2002, 20.6916161, 0.997641861, 37.4353142, 764, 177, 1.58020616, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2182, 3027, 2007, 22.1778355, 0.4906618, 26.82325, 592, 827, 1.833904, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2183, 3027, 2005, 22.8098412, 0.7731045, 43.563324, 24, 308, 1.25004411, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2184, 3028, 2007, 15.3913021, 0.856928766, 32.40895, 932, 18, 1.46490109, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2185, 3028, 2003, 14.1718931, 0.472434342, 29.3495865, 527, 293, 2.051056, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2186, 3028, 2005, 15.7145929, 0.443589747, 27.9742336, 700, 232, 2.01981735, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2187, 3028, 2004, 24.2431526, 0.0134461457, 41.8192635, 690, 571, 1.37934971, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2188, 3028, 2, 23.8389378, 0.605761468, 41.29409, 287, 869, 2.27031255, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2189, 3028, 2002, 19.75258, 0.917162836, 39.3831139, 480, 454, 1.52671444, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2190, 3028, 2006, 14.5319719, 0.0443339, 28.0461464, 858, 688, 2.24330521, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2191, 3029, 2006, 20.25204, 0.214808956, 48.68698, 658, 858, 1.69446015, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2192, 3029, 2007, 16.2587833, 0.8159012, 46.06147, 244, 46, 1.50235963, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2193, 3029, 2, 17.6360073, 0.8878207, 38.24344, 969, 117, 2.05399036, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2194, 3029, 2003, 19.1038132, 0.7232551, 31.88205, 243, 503, 1.70336032, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2195, 3029, 2004, 25.4210777, 0.773985565, 30.6326256, 816, 852, 2.05840158, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2196, 3029, 2002, 15.444068, 0.491695672, 28.2855225, 421, 406, 1.67161644, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2197, 3029, 2005, 14.6251411, 0.4588053, 44.31679, 593, 440, 2.09978628, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2198, 3030, 2003, 21.3263874, 0.241182029, 41.06293, 401, 482, 1.58091056, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2199, 3030, 2004, 14.1326694, 0.111127667, 44.82936, 745, 325, 1.667174, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2200, 3030, 2005, 24.86161, 0.0484949648, 38.2320976, 710, 410, 2.042004, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2201, 3030, 2006, 14.8158274, 0.441764027, 35.50651, 213, 56, 2.0309844, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2202, 3030, 2, 24.86161, 0.338532448, 38.2320976, 853, 710, 1.52002716, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2203, 3030, 2007, 15.3834209, 0.3109834, 34.865345, 778, 703, 1.521967, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2204, 3030, 2002, 18.90824, 0.568244, 26.4021378, 433, 806, 1.32840419, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2205, 3031, 2005, 14.7305717, 0.06055022, 37.6649628, 159, 777, 1.81313264, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2206, 3031, 2, 23.5938988, 0.990621, 33.24519, 431, 937, 2.02992988, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2207, 3031, 2004, 24.76615, 0.344476372, 46.4369431, 78, 0, 1.25455451, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2208, 3031, 2007, 17.8038979, 0.0117247412, 39.11182, 342, 193, 2.0456512, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2209, 3031, 2002, 20.7901211, 0.399227023, 27.9177685, 643, 73, 1.28836322, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2210, 3031, 2003, 22.7982635, 0.5157759, 45.4396439, 249, 969, 2.21114564, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2211, 3031, 2006, 22.7982635, 0.214505509, 45.4396439, 249, 969, 1.93668079, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2212, 3032, 2004, 14.5304756, 0.8808889, 28.2576656, 675, 448, 1.66624641, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2213, 3032, 2003, 25.837698, 0.705703855, 44.9347267, 496, 343, 1.62894356, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2214, 3032, 2005, 24.24429, 0.8617012, 33.46535, 25, 99, 1.493669, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2215, 3032, 2006, 20.1984177, 0.268951625, 34.85275, 213, 60, 1.61171377, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2216, 3032, 2, 17.5686455, 0.18937093, 27.24786, 217, 238, 2.07638049, 0)
GO
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2217, 3032, 2007, 20.21285, 0.7351449, 33.50496, 153, 406, 2.11504531, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2218, 3032, 2002, 16.8387527, 0.578760266, 43.4844, 259, 117, 1.50151932, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2219, 3033, 2002, 21.281044, 0.856763, 37.43354, 981, 443, 1.86838341, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2220, 3033, 2006, 20.4564114, 0.01762787, 41.05558, 350, 642, 1.3334986, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2221, 3033, 2003, 22.8464, 0.35991627, 43.6083565, 368, 353, 2.144173, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2222, 3033, 2, 20.66149, 0.6114552, 41.54004, 167, 877, 1.64321029, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2223, 3033, 2005, 23.7961426, 0.266835451, 35.2726555, 412, 452, 1.82460535, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2224, 3033, 2004, 21.2301235, 0.114938818, 44.612, 475, 160, 2.18473268, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2225, 3033, 2007, 16.4974957, 0.650354266, 30.2524662, 639, 537, 1.822459, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2226, 3034, 2004, 24.1546726, 0.490844429, 46.4963074, 458, 812, 1.451075, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2227, 3034, 2005, 17.6942024, 0.2712191, 39.2858467, 383, 703, 1.71396375, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2228, 3034, 2007, 17.4754219, 0.04490861, 30.52149, 10, 669, 1.55171752, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2229, 3034, 2006, 24.6783829, 0.565537453, 46.1033134, 177, 450, 1.24433672, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2230, 3034, 2, 19.9332867, 0.395194918, 31.7772331, 202, 495, 2.18004441, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2231, 3034, 2002, 19.1898079, 0.06258026, 31.65955, 870, 122, 1.55333436, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2232, 3034, 2003, 17.2301788, 0.1277599, 47.7016029, 62, 234, 1.6652087, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2233, 3035, 2004, 19.3862457, 0.117542394, 30.885767, 553, 402, 1.53435719, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2234, 3035, 2, 16.77187, 0.54531914, 44.1384773, 226, 505, 2.15909171, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2235, 3035, 2003, 18.7523479, 0.188414842, 31.2562847, 601, 804, 1.5601542, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2236, 3035, 2007, 19.78308, 0.0151046691, 42.8486824, 843, 143, 1.64678073, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2237, 3035, 2002, 18.7814178, 0.6745746, 34.3838844, 511, 320, 1.41679513, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2238, 3035, 2005, 25.8117657, 0.865148246, 47.2340126, 365, 449, 2.05892944, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2239, 3035, 2006, 18.5622, 0.0168547742, 29.5612354, 150, 130, 1.64489412, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2240, 3036, 2007, 25.3599949, 0.974573135, 43.5580444, 640, 815, 1.95401382, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2241, 3036, 2006, 20.7963581, 0.8073189, 46.0991173, 669, 509, 1.24000359, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2242, 3036, 2004, 19.3495541, 0.568648458, 26.6619816, 338, 382, 1.7679528, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2243, 3036, 2002, 23.70932, 0.0190624483, 46.68313, 454, 961, 1.64375865, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2244, 3036, 2, 15.3134813, 0.05229043, 34.726223, 233, 493, 1.44692779, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2245, 3036, 2005, 15.6155252, 0.0854177251, 27.1099548, 169, 78, 1.42865443, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2246, 3036, 2003, 14.052124, 0.6509718, 47.9064369, 473, 142, 1.318352, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2247, 3037, 2007, 15.5960722, 0.810049, 39.3864441, 248, 831, 1.806173, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2248, 3037, 2, 16.3149471, 0.93531847, 36.9498, 701, 292, 1.972841, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2249, 3037, 2002, 15.3666029, 0.584111154, 35.267807, 16, 187, 1.7567327, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2250, 3037, 2006, 21.1371346, 0.362114549, 33.4617958, 975, 769, 2.05438352, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2251, 3037, 2003, 14.5428848, 0.374325931, 44.0205269, 438, 300, 1.79654014, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2252, 3037, 2004, 19.06303, 0.127371967, 48.3367462, 522, 890, 1.75878739, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2253, 3037, 2005, 17.6283436, 0.200871661, 46.2253227, 171, 732, 1.78721809, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2254, 3038, 2002, 17.6116219, 0.7737951, 46.70301, 167, 34, 1.46563268, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2255, 3038, 2006, 25.46858, 0.09813454, 35.8941536, 830, 978, 1.718568, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2256, 3038, 2005, 20.1924362, 0.9430651, 44.84765, 362, 180, 1.75002539, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2257, 3038, 2004, 20.9359264, 0.0237202253, 37.9808273, 10, 444, 1.603591, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2258, 3038, 2007, 22.2204723, 0.6541774, 46.8170929, 785, 117, 1.50617683, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2259, 3038, 2003, 25.2593479, 0.09144421, 32.41647, 251, 742, 2.14581633, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2260, 3038, 2, 17.0447636, 0.26121363, 30.60687, 512, 360, 2.15110588, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2261, 3039, 2007, 14.1417494, 0.429347873, 33.6313553, 65, 66, 2.07367516, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2262, 3039, 2002, 19.8045425, 0.701536834, 33.1404076, 329, 238, 2.09742427, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2263, 3039, 2, 20.54388, 0.326035917, 44.2221832, 344, 96, 1.59694183, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2264, 3039, 2004, 16.1649265, 0.3525127, 32.88583, 587, 3, 1.90075707, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2265, 3039, 2003, 17.2042236, 0.529774249, 33.4238052, 878, 328, 1.38381076, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2266, 3039, 2006, 24.6762142, 0.9394106, 29.6136723, 646, 456, 1.26282859, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2267, 3039, 2005, 19.8647652, 0.486184, 30.8614254, 511, 848, 1.9151181, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2268, 3040, 2003, 23.9895916, 0.7802343, 39.8779259, 915, 956, 2.201098, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2269, 3040, 2005, 15.2404232, 0.147424072, 46.72589, 51, 571, 1.99307156, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2270, 3040, 2004, 17.76683, 0.433522433, 28.0825443, 403, 563, 2.02388763, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2271, 3040, 2007, 14.5640411, 0.04504807, 37.63261, 903, 266, 1.48012924, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2272, 3040, 2002, 20.0323677, 0.9457745, 33.1485977, 409, 287, 2.273498, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2273, 3040, 2, 20.0323677, 0.8607659, 39.86402, 53, 505, 1.82356715, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2274, 3040, 2006, 19.3734436, 0.236803368, 30.63644, 648, 186, 1.28874207, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2275, 3041, 2004, 24.5801582, 0.7033391, 42.5356445, 289, 700, 1.54901206, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2276, 3041, 2006, 24.9310036, 0.230613545, 26.3885021, 729, 274, 1.92709517, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2277, 3041, 2005, 19.363287, 0.739227235, 27.1717968, 135, 900, 1.70634568, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2278, 3041, 2, 14.3359709, 0.320313781, 34.10855, 162, 734, 1.51529968, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2279, 3041, 2002, 22.7050724, 0.9128268, 41.5146027, 866, 225, 1.54835188, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2280, 3041, 2003, 18.6995163, 0.7358544, 39.7770729, 98, 464, 1.34765077, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2281, 3041, 2007, 17.1913261, 0.7088352, 31.7047958, 667, 686, 1.82459712, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2282, 3042, 2003, 18.72772, 0.607920766, 42.15532, 728, 303, 1.85133207, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2283, 3042, 2002, 18.1714211, 0.9904083, 26.7173748, 554, 560, 1.80659235, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2284, 3042, 2005, 20.9523, 0.3924581, 48.3857422, 473, 734, 1.8668611, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2285, 3042, 2, 21.147768, 0.638002932, 47.201973, 123, 883, 1.70382726, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2286, 3042, 2006, 24.7716637, 0.7955394, 44.9567146, 211, 311, 1.32174492, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2287, 3042, 2004, 16.25458, 0.329034448, 36.9992943, 315, 682, 2.16289973, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2288, 3042, 2007, 18.72772, 0.7948895, 42.15532, 728, 370, 1.32649386, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2289, 3043, 2005, 20.0373077, 0.8009663, 45.9486046, 583, 296, 1.2297498, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2290, 3043, 2, 18.38734, 0.0138666239, 44.8406, 189, 623, 1.72583556, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2291, 3043, 2004, 22.3202763, 0.3163771, 32.06905, 193, 951, 2.23273754, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2292, 3043, 2006, 22.4703922, 0.114764474, 38.91349, 240, 174, 1.814622, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2293, 3043, 2003, 22.4703922, 0.985383451, 30.40496, 240, 713, 2.12315, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2294, 3043, 2002, 20.59817, 0.705331564, 44.8406, 871, 400, 1.72583556, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2295, 3043, 2007, 15.3059235, 0.7277579, 47.30811, 692, 289, 2.128829, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2296, 3044, 2, 17.6543751, 0.736572862, 35.3245125, 378, 547, 2.09001946, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2297, 3044, 2007, 15.6858568, 0.6346121, 36.82002, 369, 999, 2.05469513, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2298, 3044, 2004, 17.414875, 0.550998, 47.07009, 609, 751, 2.10695314, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2299, 3044, 2005, 19.931839, 0.5805315, 40.61008, 633, 322, 1.67995727, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2300, 3044, 2006, 18.8327045, 0.903808951, 31.4697933, 780, 462, 1.8801502, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2301, 3044, 2002, 21.333992, 0.6124942, 46.4498062, 160, 891, 1.35120177, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2302, 3044, 2003, 16.1089573, 0.106753774, 27.0561275, 680, 971, 1.50836015, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2303, 3045, 2002, 16.3368244, 0.538732, 38.047245, 427, 289, 1.54255867, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2304, 3045, 2007, 19.6466446, 0.72218883, 36.03731, 826, 755, 1.91530621, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2305, 3045, 2004, 18.55916, 0.971212447, 39.42007, 974, 735, 2.12575316, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2306, 3045, 2, 14.4778748, 0.706268, 45.2033958, 700, 762, 1.901811, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2307, 3045, 2003, 16.4736977, 0.9327671, 41.7177124, 700, 536, 1.480158, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2308, 3045, 2005, 15.6223574, 0.038538713, 39.4219055, 236, 137, 2.12878752, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2309, 3045, 2006, 24.91477, 0.0779290348, 39.1601372, 360, 84, 1.46130931, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2310, 3046, 2, 19.4511147, 0.334837615, 45.5798454, 345, 883, 2.06939125, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2311, 3046, 2002, 19.4511147, 0.438355565, 45.5798454, 345, 804, 1.62019086, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2312, 3046, 2005, 18.03004, 0.219281867, 28.8362656, 549, 566, 2.174376, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2313, 3046, 2007, 14.8992214, 0.030310994, 34.4569626, 605, 369, 1.73042214, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2314, 3046, 2004, 18.0987873, 0.5326863, 42.9080048, 734, 121, 1.2734133, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2315, 3046, 2006, 23.9035778, 0.0532117747, 31.9406567, 224, 776, 1.78190207, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2316, 3046, 2003, 14.8992214, 0.997525632, 39.9596138, 605, 507, 1.82863247, 0)
GO
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2317, 3047, 2004, 21.1377964, 0.04027441, 34.747612, 146, 512, 2.023971, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2318, 3047, 2002, 16.0487938, 0.5925486, 28.3079643, 206, 668, 1.92287719, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2319, 3047, 2, 24.9070644, 0.242627844, 42.0671158, 173, 141, 2.2019465, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2320, 3047, 2003, 24.9070644, 0.110110529, 42.0671158, 173, 141, 1.78476179, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2321, 3047, 2006, 21.3973675, 0.8181491, 41.62276, 78, 428, 1.29256427, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2322, 3047, 2007, 20.4873, 0.6360154, 46.10591, 288, 905, 1.77335012, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2323, 3047, 2005, 18.256546, 0.227925822, 31.97108, 609, 290, 1.65360761, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2324, 3048, 2005, 14.1676188, 0.240750968, 35.70611, 855, 124, 1.312878, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2325, 3048, 2, 24.63012, 0.751544, 42.1539879, 241, 901, 1.35615456, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2326, 3048, 2006, 23.4580345, 0.9804038, 28.241396, 777, 101, 1.30620432, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2327, 3048, 2004, 21.8462868, 0.9240384, 32.8826866, 951, 142, 1.96262419, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2328, 3048, 2003, 19.8618679, 0.5136143, 26.8601742, 777, 318, 2.11382079, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2329, 3048, 2007, 20.4748478, 0.0315001719, 34.75472, 190, 285, 1.88674283, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2330, 3048, 2002, 25.6956177, 0.457953066, 35.0628662, 902, 744, 1.28035188, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2331, 3049, 2, 13.2051554, 0.982044339, 43.2666664, 736, 488, 2.69831848, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2332, 3049, 2004, 19.6315212, 0.492245764, 32.44738, 778, 807, 3.84133339, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2333, 3049, 2003, 16.12174, 0.569382668, 42.599678, 430, 55, 3.64487267, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2334, 3049, 2006, 17.2493649, 0.701352835, 38.59558, 372, 484, 2.59641171, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2335, 3049, 2007, 17.2493649, 0.701352835, 38.59558, 517, 484, 3.15248632, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2336, 3049, 2005, 14.4454155, 0.0973694846, 27.56724, 995, 163, 3.17854476, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2337, 3049, 2002, 17.88986, 0.84080106, 27.9941063, 965, 322, 2.6175952, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2338, 3050, 2007, 20.3907719, 0.0518667921, 31.8470955, 634, 593, 4.447564, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2339, 3050, 2006, 17.134449, 0.3454639, 47.05839, 850, 293, 3.770542, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2340, 3050, 2, 20.3907719, 0.229266509, 31.8470955, 634, 593, 2.65166831, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2341, 3050, 2003, 12.7774191, 0.415493965, 36.1472931, 291, 519, 3.47396922, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2342, 3050, 2002, 18.2143688, 0.182074383, 30.0275974, 16, 375, 3.838921, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2343, 3050, 2005, 13.58684, 0.439541072, 45.3773651, 461, 698, 4.711479, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2344, 3050, 2004, 18.988308, 0.749314845, 26.3345051, 380, 451, 3.48277283, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2345, 3051, 2004, 16.0054321, 0.143280849, 26.429409, 303, 922, 3.33090115, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2346, 3051, 2, 17.1309776, 0.476062238, 45.6592, 516, 801, 4.338511, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2347, 3051, 2002, 16.03635, 0.672477961, 41.4978943, 479, 987, 4.74444866, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2348, 3051, 2006, 18.8907013, 0.777943552, 32.7597237, 780, 784, 3.61038, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2349, 3051, 2007, 19.96486, 0.494341373, 41.98644, 103, 517, 4.108226, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2350, 3051, 2005, 19.9158249, 0.171461418, 36.6996346, 818, 842, 4.563978, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2351, 3051, 2003, 16.87002, 0.7138722, 43.032753, 654, 124, 4.72018337, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2352, 3052, 2002, 19.9382381, 0.5352341, 47.2259178, 52, 893, 4.065503, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2353, 3052, 2006, 17.7216263, 0.590964556, 28.23679, 349, 727, 4.08109045, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2354, 3052, 2003, 19.5426579, 0.5628162, 43.10444, 524, 557, 3.066512, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2355, 3052, 2007, 19.9382381, 0.5352341, 47.2259178, 52, 985, 3.02408671, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2356, 3052, 2004, 17.1291428, 0.173289821, 44.1382179, 909, 783, 3.29969525, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2357, 3052, 2, 14.2191763, 0.132002413, 30.4381847, 985, 451, 4.670986, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2358, 3052, 2005, 14.4160395, 0.726461053, 45.58313, 780, 286, 2.89000368, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2359, 3053, 2, 14.6462469, 0.5511415, 30.1793327, 170, 309, 2.88249755, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2360, 3053, 2004, 15.23243, 0.00523439748, 45.0748634, 566, 435, 2.99686384, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2361, 3053, 2002, 20.1409779, 0.187400728, 32.09545, 550, 924, 3.18423152, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2362, 3053, 2003, 19.9536686, 0.111447968, 40.1343842, 79, 720, 3.09788251, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2363, 3053, 2007, 14.3312111, 0.270122021, 28.830822, 69, 615, 2.963192, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2364, 3053, 2005, 16.33276, 0.304340273, 34.39142, 11, 541, 4.56481934, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2365, 3053, 2006, 18.0903587, 0.245565683, 47.1081467, 185, 937, 3.97798038, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2366, 3054, 2007, 18.6384087, 0.531242549, 28.2461586, 343, 605, 4.30445576, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2367, 3054, 2004, 12.3769751, 0.6465167, 43.9545, 167, 725, 2.60016441, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2368, 3054, 2, 14.5618258, 0.423069954, 32.3644371, 803, 243, 2.77590585, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2369, 3054, 2005, 12.637722, 0.271466672, 32.0511742, 443, 204, 3.19644451, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2370, 3054, 2006, 17.3421879, 0.342929542, 42.4294777, 381, 461, 2.73040938, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2371, 3054, 2003, 16.0096169, 0.6387496, 27.2742748, 325, 452, 2.79323936, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2372, 3054, 2002, 12.292943, 0.3027628, 26.9345665, 161, 224, 2.60754633, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2373, 3055, 2003, 15.9981842, 0.9407896, 40.9154854, 821, 434, 4.3037343, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2374, 3055, 2006, 15.8543072, 0.5155173, 32.9621124, 821, 434, 3.55608726, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2375, 3055, 2002, 17.1818237, 0.8245799, 44.8738976, 137, 413, 4.494049, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2376, 3055, 2, 15.1540136, 0.3376382, 45.0328636, 156, 71, 4.24621439, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2377, 3055, 2004, 12.7826176, 0.8781361, 31.3059711, 127, 673, 4.512019, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2378, 3055, 2007, 15.8480806, 0.7713373, 42.449913, 978, 448, 4.730112, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2379, 3055, 2005, 15.5249987, 0.1398472, 37.1278954, 834, 166, 4.146476, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2380, 3056, 2, 11.0488987, 0.273408026, 39.40559, 988, 327, 3.294789, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2381, 3056, 2002, 18.2110233, 0.5081356, 31.45994, 214, 934, 4.62274742, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2382, 3056, 2005, 18.2110233, 0.5081356, 31.45994, 214, 698, 2.568858, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2383, 3056, 2004, 20.0654163, 0.3351092, 27.13178, 572, 740, 2.76453519, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2384, 3056, 2007, 18.0151711, 0.138058528, 41.4097862, 113, 945, 2.56991267, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2385, 3056, 2006, 18.5522633, 0.79238385, 45.9026222, 441, 241, 4.650232, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2386, 3056, 2003, 16.7861652, 0.2509415, 33.8751945, 493, 255, 2.72137451, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2387, 3057, 2, 20.2917786, 0.5514778, 37.67126, 175, 844, 4.060431, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2388, 3057, 2002, 13.2643747, 0.01091148, 42.7009468, 746, 447, 4.010064, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2389, 3057, 2005, 13.2643747, 0.01091148, 42.7009468, 746, 716, 4.010064, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2390, 3057, 2006, 15.11048, 0.3880112, 36.21685, 999, 1, 3.96734, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2391, 3057, 2003, 15.2456951, 0.379195839, 31.1528053, 988, 690, 3.28780675, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2392, 3057, 2004, 14.0451555, 0.340757072, 41.12776, 539, 238, 4.524135, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (2393, 3057, 2007, 15.3854809, 0.555412948, 33.026268, 710, 585, 3.021033, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3066, 3058, 2, 38, 0.3, 0, 120, 100, 2, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3067, 3058, 2006, 31.7043533, 0.0297216922, 27.04973, 782, 747, 6.40608072, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3068, 3058, 2003, 33.35443, 0.441885859, 39.8118134, 130, 786, 6.637224, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3069, 3058, 2007, 43.30373, 0.8870113, 28.1945381, 835, 104, 5.39461327, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3070, 3058, 2005, 30.3097725, 0.7856465, 27.0889339, 404, 220, 6.3046627, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3071, 3058, 2004, 34.9539032, 0.420592338, 47.09822, 128, 642, 6.592148, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3072, 3058, 2002, 38.57918, 0.702092648, 42.6793938, 237, 894, 7.82994556, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3073, 4012, 2003, 38.0734024, 0.4813363, 47.3994827, 581, 541, 5.95607948, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3074, 4012, 2002, 34.11374, 0.6074156, 44.0262833, 897, 143, 6.355975, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3075, 4012, 2006, 29.98548, 0.648713, 34.28513, 150, 605, 4.798186, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3076, 4012, 2004, 28.8989925, 0.2876401, 32.9053879, 773, 212, 5.573664, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3077, 4012, 2007, 27.253603, 0.6915734, 47.9677925, 851, 450, 5.15530539, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3078, 4012, 2005, 39.10652, 0.431614667, 29.84959, 220, 704, 7.714191, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3079, 4012, 2, 46.14747, 0.665784955, 47.0621147, 703, 254, 4.936004, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3080, 4013, 2007, 36.96833, 0.27207005, 36.9585266, 797, 3, 7.65902567, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3081, 4013, 2003, 27.9795589, 0.031748306, 36.50155, 180, 168, 7.502427, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3082, 4013, 2006, 34.3884468, 0.6855884, 34.3520851, 811, 368, 5.81441975, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3083, 4013, 2004, 33.9167366, 0.6291205, 36.8384552, 546, 85, 5.501765, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3084, 4013, 2, 36.31048, 0.232557476, 38.1458855, 842, 821, 5.363067, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3085, 4013, 2005, 34.1588669, 0.684944451, 30.6081638, 427, 622, 7.719286, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3086, 4013, 2002, 33.8976364, 0.9027425, 48.52515, 592, 817, 7.85759163, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3087, 4014, 2007, 33.8707123, 0.8287048, 47.59166, 737, 717, 5.927059, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3088, 4014, 2, 38.38147, 0.9795257, 48.2988167, 744, 828, 7.082729, 0)
GO
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3089, 4014, 2003, 36.2448158, 0.9795257, 48.2988167, 826, 498, 7.082729, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3090, 4014, 2005, 27.030302, 0.276580185, 42.0602837, 767, 48, 8.073768, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3091, 4014, 2006, 47.0880241, 0.8931233, 46.8358574, 155, 862, 7.572461, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3092, 4014, 2002, 32.43266, 0.702721655, 47.70607, 710, 813, 5.60021448, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3093, 4014, 2004, 33.456913, 0.388331056, 32.21656, 595, 10, 5.49038, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3094, 4015, 2004, 30.5660686, 0.600440741, 40.05892, 530, 472, 5.269438, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3095, 4015, 2, 28.3293781, 0.4779272, 43.4881477, 690, 423, 4.4223156, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3096, 4015, 2007, 30.8393631, 0.8562397, 33.4180832, 904, 555, 5.294417, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3097, 4015, 2005, 28.8512859, 0.7411602, 41.9627533, 798, 910, 6.96273041, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3098, 4015, 2006, 36.87071, 0.222710386, 42.8099365, 403, 811, 5.84088, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3099, 4015, 2002, 36.2325249, 0.8421755, 37.65469, 709, 81, 5.5725913, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3100, 4015, 2003, 49.3352966, 0.6969162, 36.27332, 575, 873, 5.171429, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3101, 4016, 2, 34.81679, 0.7710531, 34.09191, 254, 422, 6.363784, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3102, 4016, 2003, 48.8875732, 0.486147046, 27.95149, 394, 150, 7.60637, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3103, 4016, 2005, 29.41225, 0.301476359, 31.9301, 339, 226, 6.80060434, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3104, 4016, 2004, 34.4709854, 0.264512569, 35.26566, 177, 454, 5.720584, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3105, 4016, 2002, 44.8703537, 0.893061459, 37.2029953, 146, 398, 6.243848, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3106, 4016, 2006, 26.7958755, 0.21766679, 45.9934464, 485, 916, 5.597001, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3107, 4016, 2007, 32.04055, 0.3089617, 39.876873, 126, 705, 4.47471142, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3108, 4017, 2, 40.00455, 0.613957, 29.4047565, 534, 649, 7.61292171, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3109, 4017, 2007, 39.9712639, 0.291173875, 30.83103, 734, 921, 6.44317, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3110, 4017, 2003, 40.5932426, 0.169269383, 47.25876, 44, 222, 4.795574, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3111, 4017, 2004, 48.2803268, 0.156561777, 28.805521, 364, 142, 7.9359026, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3112, 4017, 2006, 45.0104065, 0.803982139, 27.7584839, 725, 922, 6.480861, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3113, 4017, 2002, 38.13696, 0.271438748, 37.4240265, 550, 746, 4.92910576, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3114, 4017, 2005, 27.5587425, 0.87111634, 42.9483452, 606, 551, 6.99844742, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3115, 4018, 2007, 38.18072, 0.969880164, 36.4707031, 666, 547, 6.50800562, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3116, 4018, 2006, 34.9446068, 0.7095538, 42.64753, 317, 64, 7.62233448, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3117, 4018, 2003, 28.3530445, 0.992350459, 33.2731819, 713, 926, 7.39515829, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3118, 4018, 2004, 27.2632847, 0.6243232, 26.31608, 777, 780, 6.380758, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3119, 4018, 2002, 33.7709923, 0.08770558, 44.4688072, 143, 388, 6.12886047, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3120, 4018, 2, 30.1521587, 0.725661457, 44.9261742, 716, 198, 6.31731129, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3121, 4018, 2005, 28.3530445, 0.221580341, 40.9487076, 713, 533, 5.31665659, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3122, 4019, 2003, 38.9217148, 0.9825592, 34.4836464, 834, 401, 4.221646, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3123, 4019, 2006, 36.552948, 0.272318363, 37.5918236, 51, 919, 3.74138761, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3124, 4019, 2007, 33.80241, 0.6761244, 40.2415848, 478, 451, 4.2890625, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3125, 4019, 2005, 25.93295, 0.767267346, 34.025032, 125, 404, 6.576244, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3126, 4019, 2004, 43.16536, 0.5672999, 47.06689, 586, 347, 6.679242, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3127, 4019, 2002, 25.5047951, 0.269170284, 42.6555634, 348, 578, 5.0557003, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3128, 4019, 2, 45.6969948, 0.8801805, 46.6791878, 330, 425, 6.36228037, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3129, 4020, 2003, 41.9164619, 0.06666141, 33.66433, 73, 414, 6.854524, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3130, 4020, 2005, 41.99762, 0.9024921, 41.4738731, 481, 708, 6.47436857, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3131, 4020, 2004, 46.2590752, 0.9559252, 43.3040466, 98, 273, 5.82833242, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3132, 4020, 2006, 36.2726974, 0.00906245, 31.803978, 437, 423, 5.66365242, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3133, 4020, 2002, 30.6299114, 0.7012814, 47.8306541, 948, 488, 6.57458353, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3134, 4020, 2007, 45.5835228, 0.7593695, 33.7109642, 701, 720, 5.48774338, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3135, 4020, 2, 39.4202843, 0.654535651, 47.8117371, 498, 721, 6.670181, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3136, 4021, 2006, 35.13795, 0.486577481, 27.88415, 554, 826, 4.95221043, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3137, 4021, 2004, 37.0282364, 0.2119337, 47.1578445, 315, 627, 5.15181065, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3138, 4021, 2005, 42.7191353, 0.231633663, 47.98263, 297, 140, 4.75898647, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3139, 4021, 2003, 42.4071846, 0.6148921, 28.0051727, 989, 449, 4.5231986, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3140, 4021, 2, 32.8842545, 0.958810449, 30.2596931, 933, 189, 4.16440439, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3141, 4021, 2007, 27.8596516, 0.627212048, 30.2596931, 933, 949, 4.16440439, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3142, 4021, 2002, 37.0847, 0.6232738, 31.6286087, 701, 873, 4.02537155, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3143, 4022, 2003, 36.49734, 0.07982499, 44.1948547, 994, 211, 3.9113214, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3144, 4022, 2005, 32.59652, 0.5819068, 33.7002869, 117, 235, 5.19789171, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3145, 4022, 2002, 33.616333, 0.793474734, 37.6090622, 380, 488, 6.092315, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3146, 4022, 2004, 35.9794846, 0.491760164, 47.3456268, 811, 314, 6.58478975, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3147, 4022, 2, 40.994915, 0.8892022, 48.11389, 966, 682, 4.08340168, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3148, 4022, 2007, 45.0804558, 0.6617161, 48.65033, 76, 535, 4.5440526, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3149, 4022, 2006, 43.275238, 0.9635524, 34.5938034, 756, 771, 5.35572052, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3150, 4023, 2007, 41.91507, 0.196484864, 26.3765259, 396, 788, 6.11754, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3151, 4023, 2, 41.41279, 0.8107126, 39.5747681, 711, 482, 5.92754126, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3152, 4023, 2003, 41.41279, 0.382142872, 28.346138, 168, 135, 6.676154, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3153, 4023, 2004, 42.94972, 0.532595456, 32.3532066, 254, 117, 5.650948, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3154, 4023, 2005, 41.91507, 0.122364886, 26.3765259, 396, 169, 6.750682, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3155, 4023, 2006, 29.7591362, 0.633157551, 39.9865227, 733, 5, 4.60288143, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3156, 4023, 2002, 27.0199032, 0.2655656, 42.91836, 795, 620, 5.49142551, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3157, 4024, 2003, 35.7005539, 0.7980266, 45.7905579, 799, 856, 5.55579424, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3158, 4024, 2007, 44.05246, 0.151126161, 33.59116, 451, 124, 5.15013742, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3159, 4024, 2002, 38.67055, 0.562774658, 30.1029987, 395, 671, 3.93138385, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3160, 4024, 2006, 32.4835968, 0.6766052, 32.66631, 939, 352, 6.56186152, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3161, 4024, 2004, 27.6171989, 0.5747034, 34.9945641, 517, 151, 4.343088, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3162, 4024, 2, 30.6414413, 0.6469914, 39.48241, 230, 559, 5.76821041, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3163, 4024, 2005, 37.30823, 0.896006048, 35.94427, 843, 598, 6.301003, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3164, 4025, 2003, 35.3462143, 0.0198156126, 43.34702, 45, 97, 5.28743935, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3165, 4025, 2, 32.95321, 0.9756184, 39.5437622, 361, 192, 6.00936747, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3166, 4025, 2004, 32.95321, 0.9756184, 39.5437622, 361, 192, 6.00936747, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3167, 4025, 2006, 31.2954578, 0.882185459, 34.2405472, 720, 648, 4.29313, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3168, 4025, 2007, 44.2811661, 0.592159, 41.64776, 802, 909, 6.353446, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3169, 4025, 2005, 30.1431065, 0.298696041, 35.44696, 361, 346, 6.89891958, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3170, 4025, 2002, 37.17634, 0.457612067, 28.2980766, 409, 608, 4.36463261, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3171, 4026, 2002, 41.38839, 0.611061752, 28.74887, 55, 799, 4.55512762, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3172, 4026, 2007, 45.2410049, 0.321469754, 41.4742355, 764, 470, 5.658831, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3173, 4026, 2006, 39.26964, 0.8316038, 27.75428, 983, 806, 4.663986, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3174, 4026, 2003, 28.9955215, 0.8049805, 30.73234, 645, 141, 4.52673054, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3175, 4026, 2005, 28.9955215, 0.247937039, 30.73234, 804, 645, 4.494656, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3176, 4026, 2, 45.2410049, 0.321469754, 41.4742355, 764, 470, 5.658831, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3177, 4026, 2004, 46.737854, 0.291273654, 31.7363319, 542, 600, 6.38252831, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3178, 4027, 2006, 41.4335442, 0.118549868, 30.335392, 298, 548, 5.4802413, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3179, 4027, 2004, 41.4335442, 0.776932955, 30.335392, 298, 999, 4.98374844, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3180, 4027, 2005, 33.21083, 0.5619175, 41.6667633, 919, 348, 6.11822271, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3181, 4027, 2, 43.22479, 0.884444952, 42.9007339, 656, 637, 6.864623, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3182, 4027, 2002, 43.4022446, 0.04031046, 41.81654, 11, 491, 5.821785, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3183, 4027, 2003, 39.95216, 0.423867017, 37.1030121, 762, 847, 5.016114, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3184, 4027, 2007, 35.9218979, 0.750493765, 40.27667, 809, 899, 4.05325, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3185, 4028, 2007, 30.9446583, 0.7580874, 34.6038857, 59, 528, 4.35430241, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3186, 4028, 2005, 31.7532444, 0.7873029, 43.6365738, 267, 710, 6.18176937, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3187, 4028, 2002, 27.306076, 0.4418874, 38.88594, 789, 154, 6.246899, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3188, 4028, 2003, 43.1915932, 0.428250134, 45.3343048, 311, 498, 4.398315, 0)
GO
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3189, 4028, 2006, 37.3221245, 0.81969744, 39.2007751, 192, 803, 6.874627, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3190, 4028, 2004, 27.6791039, 0.904485345, 26.8422165, 800, 662, 6.438056, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3191, 4028, 2, 36.12093, 0.942451954, 34.79331, 558, 570, 5.739228, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3192, 4029, 2002, 45.8765526, 0.650363445, 43.01466, 760, 421, 4.381435, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3193, 4029, 2003, 43.72486, 0.96618396, 39.786274, 467, 705, 5.53724957, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3194, 4029, 2005, 26.5706787, 0.7125721, 38.00732, 513, 462, 4.506049, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3195, 4029, 2006, 44.1873131, 0.5943756, 36.07524, 370, 182, 4.0356555, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3196, 4029, 2007, 40.519455, 0.5633851, 33.3032875, 419, 316, 5.57799435, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3197, 4029, 2004, 42.6032524, 0.110303514, 26.9809017, 866, 526, 4.3262763, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3198, 4029, 2, 26.424099, 0.6328437, 36.97745, 182, 78, 4.425911, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3199, 4030, 2, 40.16755, 0.184802771, 30.6781044, 235, 296, 6.920864, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3200, 4030, 2002, 36.0969772, 0.1759486, 44.25511, 787, 6, 4.225204, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3201, 4030, 2007, 43.12266, 0.794334233, 41.92226, 537, 344, 4.96610641, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3202, 4030, 2006, 43.12266, 0.619046867, 34.3190727, 537, 428, 4.6322, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3203, 4030, 2004, 26.6800461, 0.828242, 42.60103, 696, 586, 4.94666147, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3204, 4030, 2005, 26.2530251, 0.11677444, 30.688303, 383, 487, 4.81460667, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3205, 4030, 2003, 26.2530251, 0.326501071, 30.688303, 383, 698, 5.19156, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3206, 4031, 2002, 45.1156578, 0.3665805, 33.1497459, 423, 132, 6.5605793, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3207, 4031, 2, 35.04395, 0.838050365, 39.81681, 460, 890, 5.96697569, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3208, 4031, 2005, 42.21269, 0.290898621, 47.228508, 268, 241, 6.1657114, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3209, 4031, 2006, 32.9734039, 0.832616448, 46.27785, 28, 902, 6.01154375, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3210, 4031, 2007, 31.881979, 0.824923, 42.20964, 345, 584, 4.68661928, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3211, 4031, 2003, 38.6316566, 0.269639015, 33.1098442, 109, 320, 6.08204031, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3212, 4031, 2004, 46.7381439, 0.463220835, 41.2745438, 974, 910, 5.194203, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3213, 4032, 2002, 30.6532726, 0.723063767, 28.8047142, 743, 463, 4.681354, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3214, 4032, 2003, 29.2188358, 0.329149961, 31.3961067, 451, 407, 3.751763, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3215, 4032, 2007, 29.2188358, 0.329149961, 31.3961067, 451, 727, 3.751763, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3216, 4032, 2004, 35.1642838, 0.65726316, 35.6236267, 125, 307, 5.570657, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3217, 4032, 2005, 27.17345, 0.22964716, 40.8384972, 330, 64, 4.553181, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3218, 4032, 2006, 42.76909, 0.177355736, 26.7350712, 972, 518, 5.357819, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3219, 4032, 2, 31.1651878, 0.6018816, 42.4520874, 652, 857, 6.091204, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3220, 4033, 2006, 46.533123, 0.02377653, 48.5061378, 726, 584, 5.16340446, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3221, 4033, 2005, 37.6428, 0.429654419, 29.5853748, 219, 229, 5.38378429, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3222, 4033, 2007, 43.735775, 0.697292149, 48.3438454, 328, 767, 6.583249, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3223, 4033, 2003, 32.4788437, 0.5576351, 35.08056, 336, 595, 3.73944926, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3224, 4033, 2004, 43.735775, 0.697292149, 48.3438454, 328, 767, 6.08072233, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3225, 4033, 2002, 40.5763741, 0.6093346, 29.3928623, 454, 560, 5.371593, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3226, 4033, 2, 27.772646, 0.08996435, 29.1680756, 818, 454, 5.475199, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3227, 4034, 2005, 30.7241688, 0.189711168, 41.6603279, 983, 71, 6.65316868, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3228, 4034, 2002, 25.3373146, 0.6753144, 45.5033836, 235, 573, 6.02461624, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3229, 4034, 2004, 39.8345375, 0.9955543, 37.90874, 701, 620, 6.73413467, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3230, 4034, 2007, 34.45524, 0.6144556, 43.4002266, 567, 784, 5.84741735, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3231, 4034, 2003, 37.70368, 0.264898032, 35.05889, 260, 855, 6.835239, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3232, 4034, 2, 40.0628, 0.033278957, 35.5477066, 306, 45, 6.70223045, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3233, 4034, 2006, 45.0696, 0.868023753, 43.17777, 632, 757, 5.063335, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3234, 4035, 2007, 32.0963936, 0.260017723, 33.3569565, 744, 307, 4.66974735, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3235, 4035, 2002, 45.91613, 0.291821122, 26.5036659, 746, 322, 5.67654276, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3236, 4035, 2006, 42.1585, 0.620415568, 41.633976, 478, 509, 5.18922, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3237, 4035, 2003, 43.7771835, 0.9925602, 38.7109375, 437, 567, 3.93901062, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3238, 4035, 2004, 26.0691013, 0.8562409, 29.3101368, 676, 891, 3.918627, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3239, 4035, 2, 34.303093, 0.287543058, 41.9919853, 913, 574, 6.3271637, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3240, 4035, 2005, 29.6013012, 0.6085034, 27.5060043, 717, 14, 5.83293533, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3241, 4036, 2006, 35.25014, 0.432574838, 39.13228, 706, 680, 6.91229057, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3242, 4036, 2002, 32.3946571, 0.4565458, 26.40315, 856, 25, 4.30196571, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3243, 4036, 2004, 31.997673, 0.8541406, 39.8145828, 800, 20, 5.55264139, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3244, 4036, 2007, 45.81141, 0.6792415, 37.06419, 291, 119, 5.33838367, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3245, 4036, 2, 45.2450562, 0.516311467, 26.2678185, 295, 891, 5.740147, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3246, 4036, 2005, 31.3027134, 0.0254567526, 42.0905647, 603, 458, 5.9271245, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3247, 4036, 2003, 42.6123772, 0.7311886, 38.9432449, 985, 259, 5.85321236, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3248, 4037, 2004, 39.35998, 0.344596684, 29.12085, 812, 610, 6.86992359, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3249, 4037, 2005, 43.12293, 0.104051337, 31.797945, 946, 600, 6.869445, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3250, 4037, 2007, 27.5960045, 0.1908417, 34.05782, 35, 723, 3.83191037, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3251, 4037, 2003, 45.70462, 0.457608074, 33.6514549, 507, 769, 4.83158, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3252, 4037, 2, 32.6687469, 0.187296018, 30.17074, 868, 43, 5.18275166, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3253, 4037, 2002, 43.90344, 0.223466009, 42.65869, 975, 355, 5.4891386, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3254, 4037, 2006, 37.76059, 0.14025785, 28.0256424, 17, 909, 3.74375081, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3255, 4038, 2002, 41.9401169, 0.796562731, 48.61145, 1, 437, 6.785472, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3256, 4038, 2006, 32.06576, 0.8460239, 47.32545, 551, 310, 5.55962133, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3257, 4038, 2, 25.9819622, 0.6850303, 29.2877483, 16, 583, 6.90723, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3258, 4038, 2005, 36.13606, 0.2308303, 46.93561, 286, 405, 4.145237, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3259, 4038, 2004, 34.9698372, 0.150419787, 29.58964, 98, 832, 4.87886953, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3260, 4038, 2003, 36.7925835, 0.0539678372, 42.2942619, 811, 674, 3.845914, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3261, 4038, 2007, 36.19758, 0.677938163, 44.1178627, 193, 914, 6.496247, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3262, 4039, 2, 37.0062828, 0.118408233, 37.6474876, 697, 193, 4.57283258, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3263, 4039, 2002, 44.74964, 0.7880088, 33.9728966, 106, 919, 6.288345, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3264, 4039, 2005, 27.0269852, 0.443341732, 44.54652, 390, 911, 5.8567276, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3265, 4039, 2006, 28.6651688, 0.447494775, 41.4821434, 877, 873, 4.25042439, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3266, 4039, 2007, 29.6546478, 0.482696116, 43.1120071, 890, 153, 6.06872845, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3267, 4039, 2003, 33.134594, 0.273397684, 29.168478, 337, 434, 5.759397, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3268, 4039, 2004, 30.5389347, 0.3070005, 47.5659256, 322, 515, 6.5873785, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3269, 4040, 2003, 33.60044, 0.582132936, 28.8448353, 724, 24, 3.758942, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3270, 4040, 2004, 31.3946953, 0.7029864, 44.50991, 0, 31, 5.654802, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3271, 4040, 2, 35.2902641, 0.685655951, 34.57434, 557, 592, 3.970712, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3272, 4040, 2006, 27.8643341, 0.988698065, 36.14986, 920, 823, 5.612181, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3273, 4040, 2005, 41.88993, 0.2910604, 33.7237968, 481, 724, 6.514769, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3274, 4040, 2007, 27.8643341, 0.06445015, 36.14986, 920, 242, 6.597337, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3275, 4040, 2002, 25.90388, 0.7584852, 30.60779, 100, 592, 5.845341, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3276, 4041, 2005, 32.35607, 0.233758658, 35.6558571, 809, 536, 3.83705163, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3277, 4041, 2002, 38.17206, 0.675974965, 28.0436573, 288, 522, 5.90049458, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3278, 4041, 2006, 34.76923, 0.4501893, 41.35075, 904, 311, 4.662752, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3279, 4041, 2003, 44.30853, 0.12943399, 29.4481316, 298, 747, 4.172164, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3280, 4041, 2, 42.11541, 0.149765745, 30.8331947, 361, 903, 6.39762259, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3281, 4041, 2007, 39.89209, 0.259180456, 46.04557, 134, 500, 6.16030836, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3282, 4041, 2004, 29.7319736, 0.261231929, 45.47317, 705, 407, 6.25725, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3283, 4042, 2006, 32.833046, 0.463891834, 28.1678352, 840, 145, 5.17900753, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3284, 4042, 2005, 26.3274536, 0.249291509, 30.667345, 30, 977, 6.66858435, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3285, 4042, 2, 38.0639458, 0.6102544, 29.5108471, 570, 145, 6.69026947, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3286, 4042, 2007, 33.56263, 0.4674247, 31.630106, 821, 500, 4.29807568, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3287, 4042, 2002, 37.437458, 0.984545, 37.13718, 148, 344, 6.32332945, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3288, 4042, 2004, 30.84975, 0.783753157, 30.2852268, 44, 846, 6.083916, 0)
GO
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3289, 4042, 2003, 31.31733, 0.19188112, 45.14184, 775, 325, 6.081678, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3290, 4043, 2, 44.0077171, 0.171187073, 35.2733955, 740, 382, 5.7016263, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3291, 4043, 2003, 31.5283985, 0.108063638, 27.7103729, 64, 224, 4.767958, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3292, 4043, 2004, 31.6499062, 0.836527944, 40.66875, 953, 276, 6.19930649, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3293, 4043, 2006, 36.3504944, 0.2370378, 40.51949, 4, 437, 6.48172855, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3294, 4043, 2005, 39.0116348, 0.06490549, 42.1951065, 788, 158, 6.09022, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3295, 4043, 2007, 39.5439034, 0.5478118, 41.5167046, 414, 551, 6.117131, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3296, 4043, 2002, 39.5439034, 0.80520153, 41.5167046, 414, 561, 6.881465, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3297, 4044, 2005, 33.65206, 0.9361332, 39.2160225, 781, 58, 5.206716, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3298, 4044, 2004, 42.54263, 0.1941779, 43.2343636, 387, 543, 5.20416832, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3299, 4044, 2006, 28.486084, 0.423059732, 34.5283775, 780, 850, 5.91667652, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3300, 4044, 2, 29.9484558, 0.706232369, 32.95058, 796, 522, 5.927513, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3301, 4044, 2003, 42.51707, 0.112940341, 46.12877, 763, 196, 6.382923, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3302, 4044, 2007, 38.8511353, 0.4139024, 31.20531, 838, 100, 4.85702753, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3303, 4044, 2002, 28.9922638, 0.7915142, 27.4951038, 777, 711, 5.16355228, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3304, 4045, 2006, 31.7646542, 0.03306954, 26.30578, 308, 568, 3.74707532, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3305, 4045, 2004, 38.48882, 0.5950493, 37.88029, 781, 506, 5.597011, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3306, 4045, 2, 35.6841431, 0.395256042, 28.3411617, 873, 54, 5.941379, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3307, 4045, 2002, 27.34725, 0.204814866, 40.5459137, 814, 571, 4.99732876, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3308, 4045, 2005, 44.0650826, 0.361136526, 41.46196, 25, 358, 3.7498126, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3309, 4045, 2007, 29.8259964, 0.247993782, 47.11459, 974, 101, 3.8028028, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3310, 4045, 2003, 34.4152565, 0.1971657, 32.93526, 70, 189, 4.9549, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3311, 4046, 2006, 40.25498, 0.313473761, 29.5786953, 6, 417, 4.62533236, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3312, 4046, 2004, 34.2597351, 0.916316867, 44.82207, 348, 409, 4.60986757, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3313, 4046, 2007, 37.649704, 0.6663082, 41.8182, 392, 13, 6.65844774, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3314, 4046, 2005, 39.8063049, 0.790983558, 48.3899, 433, 3, 5.290953, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3315, 4046, 2002, 42.58857, 0.417492121, 44.07763, 947, 200, 3.8837893, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3316, 4046, 2003, 27.5778561, 0.885127544, 33.6673431, 29, 254, 6.61730528, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3317, 4046, 2, 27.5778561, 0.885127544, 33.6673431, 29, 941, 4.71670771, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3318, 4047, 2, 30.97024, 0.556073844, 42.6134567, 60, 434, 5.158402, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3319, 4047, 2007, 46.489193, 0.5866571, 44.74584, 447, 102, 4.25759268, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3320, 4047, 2003, 31.7685127, 0.3294781, 29.2745819, 519, 626, 5.6614356, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3321, 4047, 2002, 45.0698166, 0.324290484, 43.4127769, 729, 224, 4.07176447, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3322, 4047, 2006, 27.2131557, 0.3793895, 31.1429462, 541, 654, 6.85514545, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3323, 4047, 2005, 29.45489, 0.5883515, 29.645031, 505, 436, 4.56257248, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3324, 4047, 2004, 26.8838654, 0.4048267, 46.2190857, 23, 771, 5.578815, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3325, 4048, 2005, 37.16666, 0.61177963, 26.3221588, 946, 153, 5.099785, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3326, 4048, 2006, 29.8560562, 0.9756537, 47.7233849, 967, 813, 4.39742, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3327, 4048, 2, 31.0620461, 0.5238429, 47.862484, 706, 558, 6.02107, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3328, 4048, 2004, 31.0211811, 0.2823015, 43.1407852, 246, 331, 6.371681, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3329, 4048, 2003, 31.65984, 0.5302791, 41.98088, 296, 929, 3.86861849, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3330, 4048, 2002, 37.16666, 0.0923281237, 36.74203, 466, 231, 5.099785, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3331, 4048, 2007, 31.65984, 0.2944441, 41.98088, 296, 471, 3.86983562, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3332, 4049, 2002, 25.4151649, 0.07804769, 37.060997, 398, 8, 3.38028526, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3333, 4049, 2005, 40.9589043, 0.61456, 45.6682854, 995, 920, 4.61204052, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3334, 4049, 2006, 32.1036758, 0.120344535, 32.2805672, 444, 351, 5.25554466, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3335, 4049, 2, 28.37937, 0.6207355, 47.4756622, 691, 137, 3.697882, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3336, 4049, 2004, 35.01725, 0.9935637, 46.9112549, 412, 261, 4.68926334, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3337, 4049, 2003, 25.14911, 0.140172213, 33.8988075, 108, 209, 4.50249243, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3338, 4049, 2007, 29.2670269, 0.06346733, 26.84523, 321, 834, 5.062647, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3339, 4050, 2004, 33.72877, 0.5543851, 29.9948864, 521, 304, 3.44884515, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3340, 4050, 2006, 28.2884521, 0.72369945, 42.66508, 942, 2, 3.71652317, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3341, 4050, 2007, 28.3846874, 0.132481322, 46.17354, 259, 334, 3.469178, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3342, 4050, 2003, 36.4664841, 0.351081282, 27.0089664, 13, 851, 3.70854688, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3343, 4050, 2, 43.5830269, 0.638529, 47.65061, 705, 61, 3.67387724, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3344, 4050, 2005, 38.8647156, 0.333434641, 40.29118, 140, 621, 5.19911051, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3345, 4050, 2002, 37.3422966, 0.6594314, 35.44316, 748, 358, 3.43415737, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3346, 4051, 2003, 28.481102, 0.385024756, 31.4504089, 267, 284, 5.19211626, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3347, 4051, 2004, 41.1962433, 0.880079, 30.0979271, 339, 449, 3.877752, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3348, 4051, 2007, 36.6283646, 0.312700957, 45.9970131, 459, 381, 4.930442, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3349, 4051, 2006, 43.3636322, 0.361740768, 29.3918724, 309, 346, 2.99450445, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3350, 4051, 2002, 29.61092, 0.9187745, 32.91331, 954, 452, 4.115423, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3351, 4051, 2, 41.1962433, 0.731953859, 30.0979271, 339, 449, 3.23647356, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3352, 4051, 2005, 28.481102, 0.849378169, 31.4504089, 267, 518, 3.234713, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3353, 4052, 2002, 35.7195358, 0.09588404, 36.5315247, 720, 278, 5.323187, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3354, 4052, 2006, 36.6207581, 0.617865562, 36.48022, 909, 614, 5.16252136, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3355, 4052, 2003, 33.7582855, 0.9885007, 47.2964, 70, 733, 5.432828, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3356, 4052, 2005, 42.0796967, 0.00456947554, 33.32185, 523, 368, 4.66011953, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3357, 4052, 2, 38.8349762, 0.5893975, 31.163559, 964, 292, 5.49938536, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3358, 4052, 2004, 40.14377, 0.128590018, 46.2752762, 221, 137, 5.25378942, 0)
INSERT [dbo].[MonitoringData] ([Id], [MonitoringScheduleId], [CellId], [Temperature], [Fire], [Humidity], [Smoke], [ToxicGas], [UvRadiation], [Deleted]) VALUES (3359, 4052, 2007, 28.13526, 0.8526494, 35.1007347, 425, 783, 4.89929247, 0)
SET IDENTITY_INSERT [dbo].[MonitoringData] OFF
GO
SET IDENTITY_INSERT [dbo].[MonitoringSchedules] ON 

INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (1, CAST(N'2020-10-03T21:21:55.2693228' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2, CAST(N'2020-10-03T21:28:25.1252481' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3, CAST(N'2020-10-03T21:40:24.1961298' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (1002, CAST(N'2020-10-09T15:28:12.4729705' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (1003, CAST(N'2020-10-09T15:29:11.5935021' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (1004, CAST(N'2020-10-09T15:33:14.0329114' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (1005, CAST(N'2020-10-09T15:35:13.7364524' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (1006, CAST(N'2020-10-09T15:37:13.7357003' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (1007, CAST(N'2020-10-09T15:39:13.7384333' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (1008, CAST(N'2020-10-09T15:41:13.7391590' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (1009, CAST(N'2020-10-09T15:49:28.6210875' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2002, CAST(N'2020-10-09T17:49:50.9801189' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2003, CAST(N'2020-10-09T17:55:42.4967818' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2004, CAST(N'2020-10-09T17:57:33.4245423' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2005, CAST(N'2020-10-09T18:12:37.4042741' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2006, CAST(N'2020-10-09T18:14:28.6132477' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2007, CAST(N'2020-10-09T18:16:28.6417729' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2008, CAST(N'2020-10-09T18:18:28.6681624' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2009, CAST(N'2020-10-09T18:20:28.6677005' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2010, CAST(N'2020-10-09T18:22:28.6938053' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2011, CAST(N'2020-10-09T18:24:28.7117657' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2012, CAST(N'2020-10-09T18:26:28.7117693' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2013, CAST(N'2020-10-09T18:28:28.7368389' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2014, CAST(N'2020-10-09T18:30:28.7385327' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (2015, CAST(N'2020-10-09T19:11:48.0679079' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3002, CAST(N'2020-10-09T23:27:59.1996022' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3003, CAST(N'2020-10-09T23:29:48.3937956' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3004, CAST(N'2020-10-09T23:31:48.3935343' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3005, CAST(N'2020-10-09T23:33:48.3933905' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3006, CAST(N'2020-10-09T23:35:48.4117210' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3007, CAST(N'2020-10-09T23:37:48.4115757' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3008, CAST(N'2020-10-09T23:39:48.4115187' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3009, CAST(N'2020-10-09T23:41:48.4115117' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3010, CAST(N'2020-10-09T23:43:48.4114905' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3011, CAST(N'2020-10-09T23:45:48.4114793' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3012, CAST(N'2020-10-09T23:47:48.4329170' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3013, CAST(N'2020-10-09T23:49:48.4377711' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3014, CAST(N'2020-10-09T23:51:48.4379372' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3015, CAST(N'2020-10-09T23:53:48.4379520' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3016, CAST(N'2020-10-09T23:55:48.4384151' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3017, CAST(N'2020-10-09T23:57:48.4378655' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3018, CAST(N'2020-10-09T23:59:48.4537080' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3019, CAST(N'2020-10-10T00:01:48.4801723' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3020, CAST(N'2020-10-10T00:03:48.4801594' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3021, CAST(N'2020-10-10T00:05:48.5042034' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3022, CAST(N'2020-10-10T00:07:48.5038175' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3023, CAST(N'2020-10-10T00:09:48.5039647' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3024, CAST(N'2020-10-10T00:11:48.5073664' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3025, CAST(N'2020-10-10T00:13:48.5389811' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3026, CAST(N'2020-10-10T00:15:48.5389832' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3027, CAST(N'2020-10-10T00:17:48.5389866' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3028, CAST(N'2020-10-10T00:19:48.5636147' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3029, CAST(N'2020-10-10T00:21:48.5949629' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3030, CAST(N'2020-10-10T00:23:48.5949776' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3031, CAST(N'2020-10-10T00:25:48.6008866' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3032, CAST(N'2020-10-10T00:27:48.6008024' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3033, CAST(N'2020-10-10T00:29:48.6177540' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3034, CAST(N'2020-10-10T00:31:48.6475938' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3035, CAST(N'2020-10-10T00:33:48.6861613' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3036, CAST(N'2020-10-10T00:35:48.6910441' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3037, CAST(N'2020-10-10T00:37:48.6910171' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3038, CAST(N'2020-10-10T00:39:48.6910138' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3039, CAST(N'2020-10-10T00:41:48.7005457' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3040, CAST(N'2020-10-10T00:43:48.7006022' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3041, CAST(N'2020-10-10T00:45:48.7040794' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3042, CAST(N'2020-10-10T00:47:48.7040579' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3043, CAST(N'2020-10-10T00:49:48.7040634' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3044, CAST(N'2020-10-10T00:51:48.7040368' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3045, CAST(N'2020-10-10T00:53:48.7040493' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3046, CAST(N'2020-10-10T00:55:48.7287935' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3047, CAST(N'2020-10-10T00:57:48.7287567' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3048, CAST(N'2020-10-10T00:59:48.7415067' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3049, CAST(N'2020-10-10T01:01:48.7543610' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3050, CAST(N'2020-10-10T01:03:48.7543926' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3051, CAST(N'2020-10-10T01:05:48.7563773' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3052, CAST(N'2020-10-10T01:07:48.7601492' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3053, CAST(N'2020-10-10T01:09:48.7601512' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3054, CAST(N'2020-10-10T01:11:48.7693465' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3055, CAST(N'2020-10-10T01:13:48.7700120' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3056, CAST(N'2020-10-10T01:15:48.7750715' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3057, CAST(N'2020-10-10T01:17:48.8102537' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (3058, CAST(N'2020-10-10T01:19:48.8202244' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4012, CAST(N'2020-10-18T18:47:28.7042604' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4013, CAST(N'2020-10-18T18:49:17.2686316' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4014, CAST(N'2020-10-18T18:51:17.2709961' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4015, CAST(N'2020-10-18T18:53:17.2728955' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4016, CAST(N'2020-10-18T18:55:17.2719263' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4017, CAST(N'2020-10-18T18:57:17.2815439' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4018, CAST(N'2020-10-18T18:59:17.2813867' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4019, CAST(N'2020-10-18T19:01:17.2813469' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4020, CAST(N'2020-10-18T19:03:17.2839637' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4021, CAST(N'2020-10-18T19:05:17.2839665' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4022, CAST(N'2020-10-18T19:07:17.2839571' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4023, CAST(N'2020-10-18T19:09:17.2838765' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4024, CAST(N'2020-10-18T19:11:17.2839046' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4025, CAST(N'2020-10-18T19:13:17.3015801' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4026, CAST(N'2020-10-18T19:15:17.3021605' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4027, CAST(N'2020-10-18T19:17:17.3022295' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4028, CAST(N'2020-10-18T19:19:17.3020495' AS DateTime2), 0)
GO
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4029, CAST(N'2020-10-18T19:21:17.3020450' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4030, CAST(N'2020-10-18T19:23:17.3067341' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4031, CAST(N'2020-10-18T19:25:17.3189202' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4032, CAST(N'2020-10-18T19:27:17.3308530' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4033, CAST(N'2020-10-18T19:29:17.3306635' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4034, CAST(N'2020-10-18T19:31:17.3306575' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4035, CAST(N'2020-10-18T19:33:17.3306333' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4036, CAST(N'2020-10-18T19:35:17.3479140' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4037, CAST(N'2020-10-18T19:37:17.3479287' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4038, CAST(N'2020-10-18T19:39:17.3581159' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4039, CAST(N'2020-10-18T19:41:17.3745188' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4040, CAST(N'2020-10-18T19:43:17.3984235' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4041, CAST(N'2020-10-18T19:45:17.3985523' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4042, CAST(N'2020-10-18T19:47:17.4057727' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4043, CAST(N'2020-10-18T19:49:17.4214491' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4044, CAST(N'2020-10-18T19:51:17.4232727' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4045, CAST(N'2020-10-18T19:53:17.4427272' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4046, CAST(N'2020-10-18T19:55:17.4427500' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4047, CAST(N'2020-10-18T19:57:17.4428712' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4048, CAST(N'2020-10-18T19:59:17.4428175' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4049, CAST(N'2020-10-18T20:01:17.4524406' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4050, CAST(N'2020-10-18T20:03:17.4656842' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4051, CAST(N'2020-10-18T20:05:17.4654684' AS DateTime2), 0)
INSERT [dbo].[MonitoringSchedules] ([Id], [MonitoringDateTime], [Deleted]) VALUES (4052, CAST(N'2020-10-18T20:07:17.4654570' AS DateTime2), 0)
SET IDENTITY_INSERT [dbo].[MonitoringSchedules] OFF
GO
/****** Object:  Index [IX_Alerts_AlertTypeId]    Script Date: 11/11/2020 07:52:52 ******/
CREATE NONCLUSTERED INDEX [IX_Alerts_AlertTypeId] ON [dbo].[Alerts]
(
	[AlertTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Alerts_MonitoringId]    Script Date: 11/11/2020 07:52:52 ******/
CREATE NONCLUSTERED INDEX [IX_Alerts_MonitoringId] ON [dbo].[Alerts]
(
	[MonitoringId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 11/11/2020 07:52:52 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 11/11/2020 07:52:52 ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 11/11/2020 07:52:52 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 11/11/2020 07:52:52 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 11/11/2020 07:52:52 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 11/11/2020 07:52:52 ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 11/11/2020 07:52:52 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_MonitoringData_CellId]    Script Date: 11/11/2020 07:52:52 ******/
CREATE NONCLUSTERED INDEX [IX_MonitoringData_CellId] ON [dbo].[MonitoringData]
(
	[CellId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_MonitoringData_MonitoringScheduleId]    Script Date: 11/11/2020 07:52:52 ******/
CREATE NONCLUSTERED INDEX [IX_MonitoringData_MonitoringScheduleId] ON [dbo].[MonitoringData]
(
	[MonitoringScheduleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Alerts] ADD  DEFAULT (CONVERT([bit],(0))) FOR [Deleted]
GO
ALTER TABLE [dbo].[AlertTypes] ADD  DEFAULT (CONVERT([bit],(0))) FOR [Deleted]
GO
ALTER TABLE [dbo].[Cells] ADD  DEFAULT (CONVERT([bit],(0))) FOR [Deleted]
GO
ALTER TABLE [dbo].[MonitoringData] ADD  DEFAULT (CONVERT([bit],(0))) FOR [Deleted]
GO
ALTER TABLE [dbo].[MonitoringSchedules] ADD  DEFAULT (CONVERT([bit],(0))) FOR [Deleted]
GO
ALTER TABLE [dbo].[Alerts]  WITH CHECK ADD  CONSTRAINT [FK_Alerts_AlertTypes_AlertTypeId] FOREIGN KEY([AlertTypeId])
REFERENCES [dbo].[AlertTypes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Alerts] CHECK CONSTRAINT [FK_Alerts_AlertTypes_AlertTypeId]
GO
ALTER TABLE [dbo].[Alerts]  WITH CHECK ADD  CONSTRAINT [FK_Alerts_MonitoringData_MonitoringId] FOREIGN KEY([MonitoringId])
REFERENCES [dbo].[MonitoringData] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Alerts] CHECK CONSTRAINT [FK_Alerts_MonitoringData_MonitoringId]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[MonitoringData]  WITH CHECK ADD  CONSTRAINT [FK_MonitoringData_Cells_CellId] FOREIGN KEY([CellId])
REFERENCES [dbo].[Cells] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MonitoringData] CHECK CONSTRAINT [FK_MonitoringData_Cells_CellId]
GO
ALTER TABLE [dbo].[MonitoringData]  WITH CHECK ADD  CONSTRAINT [FK_MonitoringData_MonitoringSchedules_MonitoringScheduleId] FOREIGN KEY([MonitoringScheduleId])
REFERENCES [dbo].[MonitoringSchedules] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MonitoringData] CHECK CONSTRAINT [FK_MonitoringData_MonitoringSchedules_MonitoringScheduleId]
GO
/****** Object:  StoredProcedure [dbo].[AverageByDayNight]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[AverageByDayNight]
as

select avg(Temperature) as AvgTemperature, avg(Fire) as AvgFire, avg(Humidity) as AvgHumidity, avg(Smoke) as AvgSmoke, avg(ToxicGas) AvgToxicGas, avg(UvRadiation) as AvgUv, 
dbo.DayNight(MonitoringDateTime) as DayNight 
from MonitoringData 
inner join MonitoringSchedules on MonitoringData.MonitoringScheduleId = MonitoringSchedules.Id
group by dbo.DayNight(MonitoringDateTime)
order by DayNight
GO
/****** Object:  StoredProcedure [dbo].[AverageByDayNightByCell]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[AverageByDayNightByCell]
@id int,
@start datetime,
@end datetime
as

select avg(Temperature) as AvgTemperature, avg(Fire) as AvgFire, avg(Humidity) as AvgHumidity, avg(Smoke) as AvgSmoke, avg(ToxicGas) AvgToxicGas, avg(UvRadiation) as AvgUv, 
dbo.DayNight(MonitoringDateTime) as DayNight 
from MonitoringData
inner join MonitoringSchedules on MonitoringData.MonitoringScheduleId = MonitoringSchedules.Id
where MonitoringData.CellId = @id and MonitoringSchedules.MonitoringDateTime between @start and @end
group by dbo.DayNight(MonitoringDateTime)
GO
/****** Object:  StoredProcedure [dbo].[CheckCellExistence]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[CheckCellExistence]
@id int
as

declare @total int
select @total = count(*)  from Cells where Id = @id;
if @total = 1
	select 1
else
	select 0
GO
/****** Object:  StoredProcedure [dbo].[GetAlerts]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GetAlerts]
@quantity int
as

select top (@quantity) AlertTypes.Name as AlertType, Cells.Address as Address from Alerts 
inner join AlertTypes on Alerts.AlertTypeId = AlertTypes.Id
inner join MonitoringData on Alerts.MonitoringId = MonitoringData.Id
inner join Cells on MonitoringData.CellId = Cells.Id
order by Alerts.MonitoringId desc
GO
/****** Object:  StoredProcedure [dbo].[GetLastMeasurement]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GetLastMeasurement]
@cellId int
as

select top 1 Temperature, Humidity, UvRadiation, Fire, Smoke, ToxicGas from MonitoringData
inner join MonitoringSchedules on MonitoringScheduleId = MonitoringSchedules.Id
where MonitoringData.CellId = @cellId
order by MonitoringDateTime desc
GO
/****** Object:  StoredProcedure [dbo].[GetTimeSeries]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[GetTimeSeries]
@cellId int,
@columnName varchar(100),
@start varchar(100),
@end varchar(100)
as

declare @beginDateTime DateTime, @endDateTime DateTime
set @beginDateTime = convert(DateTime, @start, 121)
set @endDateTime = convert(Datetime, @end, 121)

select 
	case @columnName
		when 'Temperature' then Temperature
		when 'Humidity' then Humidity
		when 'UvRadiation' then UvRadiation
		when 'Fire' then Fire
		when 'Smoke' then Smoke
		when 'ToxicGas' then ToxicGas
		else null
	end as 'Value', MonitoringSchedules.MonitoringDateTime as 'DateTime'
from MonitoringData
inner join MonitoringSchedules on MonitoringData.MonitoringScheduleId = MonitoringSchedules.Id
where (MonitoringSchedules.MonitoringDateTime between @beginDateTime and @endDateTime) and (MonitoringData.CellId = @cellId)
GO
/****** Object:  StoredProcedure [dbo].[OverallAverage]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[OverallAverage]
as
select avg(Temperature) as AvgTemperature, avg(Fire) as AvgFire, avg(Humidity) as AvgHumidity, avg(Smoke) as AvgSmoke, avg(ToxicGas) AvgToxicGas, avg(UvRadiation) as AvgUv
from MonitoringData
GO
/****** Object:  StoredProcedure [dbo].[OverallAverageByCell]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[OverallAverageByCell]
@id int,
@start datetime,
@end datetime
as

select avg(Temperature) as AvgTemperature, avg(Fire) as AvgFire, avg(Humidity) as AvgHumidity, avg(Smoke) as AvgSmoke, avg(ToxicGas) AvgToxicGas, 
avg(UvRadiation) as AvgUv
from MonitoringData
inner join MonitoringSchedules on MonitoringData.MonitoringScheduleId = MonitoringSchedules.Id
where CellId = @id and MonitoringSchedules.MonitoringDateTime between @start and @end
GO
/****** Object:  StoredProcedure [dbo].[SearchCell]    Script Date: 11/11/2020 07:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SearchCell]
@search varchar(100)
as

select Id, Address from Cells where Address like '%' + @search + '%' collate Latin1_General_CI_AS
GO
USE [master]
GO
ALTER DATABASE [WSN] SET  READ_WRITE 
GO
