use WSN

go
create procedure GetLastMeasurement
@cellId int
as

select top 1 Temperature, Humidity, UvRadiation, Fire, Smoke, ToxicGas from MonitoringData
inner join MonitoringSchedules on MonitoringScheduleId = MonitoringSchedules.Id
where MonitoringData.CellId = @cellId
order by MonitoringDateTime desc
