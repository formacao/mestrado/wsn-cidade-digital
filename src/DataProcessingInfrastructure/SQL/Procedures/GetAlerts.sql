use WSN

go
create procedure GetAlerts
@quantity int
as

select top (@quantity) AlertTypes.Name as AlertType, Cells.Address as Address from Alerts 
inner join AlertTypes on Alerts.AlertTypeId = AlertTypes.Id
inner join MonitoringData on Alerts.MonitoringId = MonitoringData.Id
inner join Cells on MonitoringData.CellId = Cells.Id
order by Alerts.MonitoringId desc