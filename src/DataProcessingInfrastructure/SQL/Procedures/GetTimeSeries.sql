use WSN
go

create procedure GetTimeSeries
@cellId int,
@columnName varchar(100),
@start varchar(100),
@end varchar(100)
as

declare @beginDateTime DateTime, @endDateTime DateTime
set @beginDateTime = convert(DateTime, @start, 121)
set @endDateTime = convert(Datetime, @end, 121)

select 
	case @columnName
		when 'Temperature' then Temperature
		when 'Humidity' then Humidity
		when 'UvRadiation' then UvRadiation
		when 'Fire' then Fire
		when 'Smoke' then Smoke
		when 'ToxicGas' then ToxicGas
		else null
	end as 'Value', MonitoringSchedules.MonitoringDateTime as 'DateTime'
from MonitoringData
inner join MonitoringSchedules on MonitoringData.MonitoringScheduleId = MonitoringSchedules.Id
where (MonitoringSchedules.MonitoringDateTime between @beginDateTime and @endDateTime) and (MonitoringData.CellId = @cellId)
