use WSN;
go

create procedure OverallAverage
as
select avg(Temperature) as AvgTemperature, avg(Fire) as AvgFire, avg(Humidity) as AvgHumidity, avg(Smoke) as AvgSmoke, avg(ToxicGas) AvgToxicGas, avg(UvRadiation) as AvgUv
from MonitoringData