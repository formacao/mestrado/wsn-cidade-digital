use WSN;
Go

Create Procedure AverageByDayNight
as

select avg(Temperature) as AvgTemperature, avg(Fire) as AvgFire, avg(Humidity) as AvgHumidity, avg(Smoke) as AvgSmoke, avg(ToxicGas) AvgToxicGas, avg(UvRadiation) as AvgUv, 
dbo.DayNight(MonitoringDateTime) as DayNight 
from MonitoringData 
inner join MonitoringSchedules on MonitoringData.MonitoringScheduleId = MonitoringSchedules.Id
group by dbo.DayNight(MonitoringDateTime)
order by DayNight
