use WSN;
go

create procedure OverallAverageByCell
@id int,
@start datetime,
@end datetime
as

select avg(Temperature) as AvgTemperature, avg(Fire) as AvgFire, avg(Humidity) as AvgHumidity, avg(Smoke) as AvgSmoke, avg(ToxicGas) AvgToxicGas, 
avg(UvRadiation) as AvgUv
from MonitoringData
inner join MonitoringSchedules on MonitoringData.MonitoringScheduleId = MonitoringSchedules.Id
where CellId = @id and MonitoringSchedules.MonitoringDateTime between @start and @end
