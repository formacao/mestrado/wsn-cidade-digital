use wsn;

go
create or alter procedure GetCsvData
@ids nvarchar(max),
@start datetime,
@end datetime
as

create table #ids (id int)
insert into #ids (id) select cast(value as int) from string_split(@ids, ',')

select Temperature, Fire, Humidity, Smoke, ToxicGas, UvRadiation, Name, Address, Latitude, Longitude, MonitoringDateTime as 'MonitoringMoment' from MonitoringData
inner join Cells on MonitoringData.CellId = Cells.Id
inner join MonitoringSchedules on MonitoringSchedules.Id = MonitoringData.MonitoringScheduleId
where (@ids = '' or Cells.Id in (select id from #ids)) and MonitoringSchedules.MonitoringDateTime between @start and @end;

