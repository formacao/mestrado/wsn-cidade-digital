use WSN
Go

Create Procedure AverageByDayNightByCell
@id int,
@start datetime,
@end datetime
as

select avg(Temperature) as AvgTemperature, avg(Fire) as AvgFire, avg(Humidity) as AvgHumidity, avg(Smoke) as AvgSmoke, avg(ToxicGas) AvgToxicGas, avg(UvRadiation) as AvgUv, 
dbo.DayNight(MonitoringDateTime) as DayNight 
from MonitoringData
inner join MonitoringSchedules on MonitoringData.MonitoringScheduleId = MonitoringSchedules.Id
where MonitoringData.CellId = @id and MonitoringSchedules.MonitoringDateTime between @start and @end
group by dbo.DayNight(MonitoringDateTime)