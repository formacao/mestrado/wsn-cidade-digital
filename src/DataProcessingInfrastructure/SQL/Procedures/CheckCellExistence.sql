use WSN
go

create procedure CheckCellExistence
@id int
as

declare @total int
select @total = count(*)  from Cells where Id = @id;
if @total = 1
	select 1
else
	select 0