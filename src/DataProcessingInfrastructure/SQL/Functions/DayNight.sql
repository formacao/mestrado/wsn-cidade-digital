create function DayNight(@datetime DateTime)
returns varchar(100)
begin
	declare @hour int
	set @hour = DATEPART(HOUR, @datetime)
	if(@hour < 9 or @hour > 21)
		return 'Night'
	return 'Day'
end