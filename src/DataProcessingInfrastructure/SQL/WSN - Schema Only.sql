USE [master]
GO
/****** Object:  Database [WSN]    Script Date: 11/11/2020 07:51:34 ******/
CREATE DATABASE [WSN]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'WSN', FILENAME = N'/var/opt/mssql/data/WSN.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'WSN_log', FILENAME = N'/var/opt/mssql/data/WSN_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [WSN] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [WSN].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [WSN] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [WSN] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [WSN] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [WSN] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [WSN] SET ARITHABORT OFF 
GO
ALTER DATABASE [WSN] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [WSN] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [WSN] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [WSN] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [WSN] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [WSN] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [WSN] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [WSN] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [WSN] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [WSN] SET  DISABLE_BROKER 
GO
ALTER DATABASE [WSN] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [WSN] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [WSN] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [WSN] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [WSN] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [WSN] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [WSN] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [WSN] SET RECOVERY FULL 
GO
ALTER DATABASE [WSN] SET  MULTI_USER 
GO
ALTER DATABASE [WSN] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [WSN] SET DB_CHAINING OFF 
GO
ALTER DATABASE [WSN] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [WSN] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [WSN] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [WSN] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [WSN] SET QUERY_STORE = OFF
GO
USE [WSN]
GO
/****** Object:  UserDefinedFunction [dbo].[DayNight]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[DayNight](@datetime DateTime)
returns varchar(100)
begin
	declare @hour int
	set @hour = DATEPART(HOUR, @datetime)
	if(@hour < 9 or @hour > 21)
		return 'Night'
	return 'Day'
end
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Alerts]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alerts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MonitoringId] [int] NOT NULL,
	[AlertTypeId] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Alerts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AlertTypes]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AlertTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_AlertTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cells]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cells](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Address] [nvarchar](300) NOT NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Cells] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MonitoringData]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonitoringData](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MonitoringScheduleId] [int] NOT NULL,
	[CellId] [int] NOT NULL,
	[Temperature] [real] NOT NULL,
	[Fire] [real] NOT NULL,
	[Humidity] [real] NOT NULL,
	[Smoke] [real] NOT NULL,
	[ToxicGas] [real] NOT NULL,
	[UvRadiation] [real] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_MonitoringData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MonitoringSchedules]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonitoringSchedules](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MonitoringDateTime] [datetime2](7) NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_MonitoringSchedules] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_Alerts_AlertTypeId]    Script Date: 11/11/2020 07:51:34 ******/
CREATE NONCLUSTERED INDEX [IX_Alerts_AlertTypeId] ON [dbo].[Alerts]
(
	[AlertTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Alerts_MonitoringId]    Script Date: 11/11/2020 07:51:34 ******/
CREATE NONCLUSTERED INDEX [IX_Alerts_MonitoringId] ON [dbo].[Alerts]
(
	[MonitoringId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 11/11/2020 07:51:34 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 11/11/2020 07:51:34 ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 11/11/2020 07:51:34 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 11/11/2020 07:51:34 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 11/11/2020 07:51:34 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 11/11/2020 07:51:34 ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 11/11/2020 07:51:34 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_MonitoringData_CellId]    Script Date: 11/11/2020 07:51:34 ******/
CREATE NONCLUSTERED INDEX [IX_MonitoringData_CellId] ON [dbo].[MonitoringData]
(
	[CellId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_MonitoringData_MonitoringScheduleId]    Script Date: 11/11/2020 07:51:34 ******/
CREATE NONCLUSTERED INDEX [IX_MonitoringData_MonitoringScheduleId] ON [dbo].[MonitoringData]
(
	[MonitoringScheduleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Alerts] ADD  DEFAULT (CONVERT([bit],(0))) FOR [Deleted]
GO
ALTER TABLE [dbo].[AlertTypes] ADD  DEFAULT (CONVERT([bit],(0))) FOR [Deleted]
GO
ALTER TABLE [dbo].[Cells] ADD  DEFAULT (CONVERT([bit],(0))) FOR [Deleted]
GO
ALTER TABLE [dbo].[MonitoringData] ADD  DEFAULT (CONVERT([bit],(0))) FOR [Deleted]
GO
ALTER TABLE [dbo].[MonitoringSchedules] ADD  DEFAULT (CONVERT([bit],(0))) FOR [Deleted]
GO
ALTER TABLE [dbo].[Alerts]  WITH CHECK ADD  CONSTRAINT [FK_Alerts_AlertTypes_AlertTypeId] FOREIGN KEY([AlertTypeId])
REFERENCES [dbo].[AlertTypes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Alerts] CHECK CONSTRAINT [FK_Alerts_AlertTypes_AlertTypeId]
GO
ALTER TABLE [dbo].[Alerts]  WITH CHECK ADD  CONSTRAINT [FK_Alerts_MonitoringData_MonitoringId] FOREIGN KEY([MonitoringId])
REFERENCES [dbo].[MonitoringData] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Alerts] CHECK CONSTRAINT [FK_Alerts_MonitoringData_MonitoringId]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[MonitoringData]  WITH CHECK ADD  CONSTRAINT [FK_MonitoringData_Cells_CellId] FOREIGN KEY([CellId])
REFERENCES [dbo].[Cells] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MonitoringData] CHECK CONSTRAINT [FK_MonitoringData_Cells_CellId]
GO
ALTER TABLE [dbo].[MonitoringData]  WITH CHECK ADD  CONSTRAINT [FK_MonitoringData_MonitoringSchedules_MonitoringScheduleId] FOREIGN KEY([MonitoringScheduleId])
REFERENCES [dbo].[MonitoringSchedules] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MonitoringData] CHECK CONSTRAINT [FK_MonitoringData_MonitoringSchedules_MonitoringScheduleId]
GO
/****** Object:  StoredProcedure [dbo].[AverageByDayNight]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[AverageByDayNight]
as

select avg(Temperature) as AvgTemperature, avg(Fire) as AvgFire, avg(Humidity) as AvgHumidity, avg(Smoke) as AvgSmoke, avg(ToxicGas) AvgToxicGas, avg(UvRadiation) as AvgUv, 
dbo.DayNight(MonitoringDateTime) as DayNight 
from MonitoringData 
inner join MonitoringSchedules on MonitoringData.MonitoringScheduleId = MonitoringSchedules.Id
group by dbo.DayNight(MonitoringDateTime)
order by DayNight
GO
/****** Object:  StoredProcedure [dbo].[AverageByDayNightByCell]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[AverageByDayNightByCell]
@id int,
@start datetime,
@end datetime
as

select avg(Temperature) as AvgTemperature, avg(Fire) as AvgFire, avg(Humidity) as AvgHumidity, avg(Smoke) as AvgSmoke, avg(ToxicGas) AvgToxicGas, avg(UvRadiation) as AvgUv, 
dbo.DayNight(MonitoringDateTime) as DayNight 
from MonitoringData
inner join MonitoringSchedules on MonitoringData.MonitoringScheduleId = MonitoringSchedules.Id
where MonitoringData.CellId = @id and MonitoringSchedules.MonitoringDateTime between @start and @end
group by dbo.DayNight(MonitoringDateTime)
GO
/****** Object:  StoredProcedure [dbo].[CheckCellExistence]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[CheckCellExistence]
@id int
as

declare @total int
select @total = count(*)  from Cells where Id = @id;
if @total = 1
	select 1
else
	select 0
GO
/****** Object:  StoredProcedure [dbo].[GetAlerts]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GetAlerts]
@quantity int
as

select top (@quantity) AlertTypes.Name as AlertType, Cells.Address as Address from Alerts 
inner join AlertTypes on Alerts.AlertTypeId = AlertTypes.Id
inner join MonitoringData on Alerts.MonitoringId = MonitoringData.Id
inner join Cells on MonitoringData.CellId = Cells.Id
order by Alerts.MonitoringId desc
GO
/****** Object:  StoredProcedure [dbo].[GetLastMeasurement]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GetLastMeasurement]
@cellId int
as

select top 1 Temperature, Humidity, UvRadiation, Fire, Smoke, ToxicGas from MonitoringData
inner join MonitoringSchedules on MonitoringScheduleId = MonitoringSchedules.Id
where MonitoringData.CellId = @cellId
order by MonitoringDateTime desc
GO
/****** Object:  StoredProcedure [dbo].[GetTimeSeries]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[GetTimeSeries]
@cellId int,
@columnName varchar(100),
@start varchar(100),
@end varchar(100)
as

declare @beginDateTime DateTime, @endDateTime DateTime
set @beginDateTime = convert(DateTime, @start, 121)
set @endDateTime = convert(Datetime, @end, 121)

select 
	case @columnName
		when 'Temperature' then Temperature
		when 'Humidity' then Humidity
		when 'UvRadiation' then UvRadiation
		when 'Fire' then Fire
		when 'Smoke' then Smoke
		when 'ToxicGas' then ToxicGas
		else null
	end as 'Value', MonitoringSchedules.MonitoringDateTime as 'DateTime'
from MonitoringData
inner join MonitoringSchedules on MonitoringData.MonitoringScheduleId = MonitoringSchedules.Id
where (MonitoringSchedules.MonitoringDateTime between @beginDateTime and @endDateTime) and (MonitoringData.CellId = @cellId)
GO
/****** Object:  StoredProcedure [dbo].[OverallAverage]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[OverallAverage]
as
select avg(Temperature) as AvgTemperature, avg(Fire) as AvgFire, avg(Humidity) as AvgHumidity, avg(Smoke) as AvgSmoke, avg(ToxicGas) AvgToxicGas, avg(UvRadiation) as AvgUv
from MonitoringData
GO
/****** Object:  StoredProcedure [dbo].[OverallAverageByCell]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[OverallAverageByCell]
@id int,
@start datetime,
@end datetime
as

select avg(Temperature) as AvgTemperature, avg(Fire) as AvgFire, avg(Humidity) as AvgHumidity, avg(Smoke) as AvgSmoke, avg(ToxicGas) AvgToxicGas, 
avg(UvRadiation) as AvgUv
from MonitoringData
inner join MonitoringSchedules on MonitoringData.MonitoringScheduleId = MonitoringSchedules.Id
where CellId = @id and MonitoringSchedules.MonitoringDateTime between @start and @end
GO
/****** Object:  StoredProcedure [dbo].[SearchCell]    Script Date: 11/11/2020 07:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SearchCell]
@search varchar(100)
as

select Id, Address from Cells where Address like '%' + @search + '%' collate Latin1_General_CI_AS
GO
USE [master]
GO
ALTER DATABASE [WSN] SET  READ_WRITE 
GO
