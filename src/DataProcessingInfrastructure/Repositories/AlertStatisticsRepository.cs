﻿using Dapper;
using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.ResponseModels;
using Infrastructure.Domain.Singletons.Interfaces;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace DapperInfrastructure.Repositories
{
    public class AlertStatisticsRepository : IAlertStatisticsRepository, IDisposable
    {
        private readonly IEnvironmentSingleton _environment;
        private readonly SqlConnection _connection;

        public AlertStatisticsRepository(IEnvironmentSingleton environment)
        {
            _environment = environment;
            _connection = new SqlConnection(_environment.ConnectionString);
        }

        public void Dispose() => _connection.Dispose();
        public async Task<IEnumerable<AlertDataModel>> GetOverall() => await _connection.QueryAsync<AlertDataModel>("GetAlerts", new {quantity = 5 }, commandType: CommandType.StoredProcedure);
    }
}
