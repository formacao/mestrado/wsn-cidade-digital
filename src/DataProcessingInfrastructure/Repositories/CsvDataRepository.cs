﻿using Dapper;
using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.RequestModels;
using Infrastructure.Domain.ResponseModels;
using Infrastructure.Domain.Singletons.Interfaces;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperInfrastructure.Repositories
{
    public class CsvDataRepository : ICsvDataRepository
    {

        private readonly SqlConnection _connection;
        private readonly IEnvironmentSingleton _environment;

        public CsvDataRepository(IEnvironmentSingleton environment)
        {
            _environment = environment;
            _connection = new SqlConnection(_environment.ConnectionString);
        }

        public async Task<IEnumerable<CsvRowModel>> GetCsvDataAsync(CsvDataRequestModel request) 
        {
            var idsString = string.Empty;
            if(request.Ids.Any())
            {
                var ids = request.Ids.Select(x => x.ToString());
                idsString = string.Join(",", ids);
            }
            var parameters = new { ids = idsString, start = request.Start, end = request.End };

            return await _connection.QueryAsync<CsvRowModel>("GetCsvData", parameters, commandType: CommandType.StoredProcedure);
        } 
    }
}
