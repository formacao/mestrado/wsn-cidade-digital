﻿using Dapper;
using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.ResponseModels;
using Infrastructure.Domain.Singletons.Interfaces;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace DapperInfrastructure.Repositories
{
    public class CityStatisticsRepository : ICityStatisticsRepository, IDisposable
    {
        private readonly SqlConnection _connection;

        private readonly IEnvironmentSingleton _environment;

        public CityStatisticsRepository(IEnvironmentSingleton environment)
        {
            _environment = environment;
            _connection = new SqlConnection(_environment.ConnectionString);
        }

        public void Dispose()
        {
            _connection.Dispose();
        }

        public async Task<IEnumerable<AverageByDayNightModel>> GetAverageByDayNightAsync() => await _connection.QueryAsync<AverageByDayNightModel>("AverageByDayNight", commandType: CommandType.StoredProcedure);
        public async Task<OverallAverageModel> GetOverallAverageAsync() => await _connection.QueryFirstAsync<OverallAverageModel>("OverallAverage", commandType: CommandType.StoredProcedure);
    }
}
