﻿using Dapper;
using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.ResponseModels;
using Infrastructure.Domain.Singletons.Interfaces;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DapperInfrastructure.Repositories
{
    public class CellDataRepository : ICellDataRepository, IDisposable
    {
        private readonly SqlConnection _connection;
        private readonly IEnvironmentSingleton _environment;

        public CellDataRepository(IEnvironmentSingleton environment)
        {
            _environment = environment;
            _connection = new SqlConnection(_environment.ConnectionString);
        }

        public async Task<bool> CheckExistence(int id) =>
            await _connection.QueryFirstAsync<bool>("CheckCellExistence", new { id }, commandType: CommandType.StoredProcedure);


        public void Dispose() => _connection.Dispose();

        public async Task<IEnumerable<AverageByDayNightModel>> GetAverageByDayNight(int cellId, DateTime start, DateTime end) =>
            await _connection.QueryAsync<AverageByDayNightModel>("AverageByDayNightByCell", new { id = cellId, start = ConvertToUtc(start), end = ConvertToUtc(end) }, commandType: CommandType.StoredProcedure);

        public async Task<RealTimeMeasurementsModel> GetLastMeasurementAsync(int id) =>
            await _connection.QueryFirstOrDefaultAsync<RealTimeMeasurementsModel>("GetLastMeasurement", new { CellId = id }, commandType: CommandType.StoredProcedure);

        public async Task<OverallAverageModel> GetOverallAverage(int cellId, DateTime start, DateTime end) =>
            await _connection.QueryFirstOrDefaultAsync<OverallAverageModel>("OverallAverageByCell", new { id = cellId, start = ConvertToUtc(start), end = ConvertToUtc(end) }, commandType: CommandType.StoredProcedure);

        public async Task<IEnumerable<SearchCellModel>> GetSearchResultAsync(string search) => 
            await _connection.QueryAsync<SearchCellModel>("SearchCell", new { Search = search }, commandType: CommandType.StoredProcedure);

        public async Task<IEnumerable<TimeSeriesModel>> GetTimeSeriesAsync(int cellId, DateTime start, DateTime end, string columnName)
        {
            var convertedStart = ConvertToUtc(start);
            var convertedEnd = ConvertToUtc(end);
            var parameters = new {
                cellId,
                columnName,
                start = convertedStart.ToString("yyyy-MM-dd HH:mm:ss"),
                end = convertedEnd.ToString("yyyy-MM-dd HH:mm:ss")
            };
            var result = await _connection.QueryAsync<TimeSeriesModel>("GetTimeSeries", parameters, commandType: CommandType.StoredProcedure);

            return result.Select(ts => ts.ConvertDateTimeFromUtcToLocal());
        }

        private DateTime ConvertToUtc(DateTime local) => TimeZoneInfo.ConvertTimeToUtc(local, TimeZoneInfo.Local);
    }
}
