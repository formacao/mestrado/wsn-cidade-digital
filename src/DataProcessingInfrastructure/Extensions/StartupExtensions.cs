﻿using DapperInfrastructure.Repositories;
using Infrastructure.Domain.Repositories.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace DapperInfrastructure.Extensions
{
    public static class StartupExtensions
    {
        public static IServiceCollection AddDataProcessingRepositories(this IServiceCollection services)
        {
            services
                .AddScoped<ICityStatisticsRepository, CityStatisticsRepository>()
                .AddScoped<IAlertStatisticsRepository, AlertStatisticsRepository>()
                .AddScoped<ICellDataRepository, CellDataRepository>()
                .AddScoped<ICsvDataRepository, CsvDataRepository>()
                .AddScoped<ICheckCellExistenceRepository>(sp => sp.GetRequiredService<ICellDataRepository>());

            return services;
        }
    }
}
