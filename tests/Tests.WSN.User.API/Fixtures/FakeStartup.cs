﻿using EFCoreInfrastructure.Extensions;
using Fixtures.DataProcessingInfrastructure.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Tests.WSN.User.API.Extensions;
using WSN.User.API;
using WSN.User.API.Extensions;

namespace Tests.WSN.User.API.Fixtures
{
    public class FakeStartup : Startup
    {
        public FakeStartup(IConfiguration configuration) : base(configuration)
        {
        }

        protected override void ConfigureApiServices(IServiceCollection services)
        {
            services
                .AddFakeDotEnv()
                .AddInfrastructureSingletons()
                .AddFixtureDataProcessingRepositories()
                .AddApiServices();
        }
    }
}
