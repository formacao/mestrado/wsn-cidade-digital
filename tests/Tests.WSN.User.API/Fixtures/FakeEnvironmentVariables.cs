﻿using System;
using WSN.User.API.Singleton.Interface;

namespace Tests.WSN.User.API.Fixtures
{
    public class FakeEnvironmentVariables : IUserApiEnvironmentSingleton
    {
        public string TempCsvDirectory => Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
        public string CsvUrl => throw new InvalidOperationException("Unit test should not cover this part");
    }
}