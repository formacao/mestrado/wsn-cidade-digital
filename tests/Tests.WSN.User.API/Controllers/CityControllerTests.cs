﻿using Infrastructure.Domain.ResponseModels;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Tests.WSN.User.API.Fixtures;
using Xunit;

namespace Tests.WSN.User.API.Controllers
{
    public class CityControllerTests : IClassFixture<UserAPIAppFactory>
    {
        private readonly UserAPIAppFactory _app;
        private readonly JsonSerializerOptions _jsonOptions = new JsonSerializerOptions();

        public CityControllerTests(UserAPIAppFactory app)
        {
            _app = app;
            _jsonOptions.PropertyNameCaseInsensitive = true;
        }

        [Fact]
        public async Task DayNightShouldReturnANonEmptyEnumerable()
        {
            var client = _app.CreateClient();
            var httpResult = await client.GetAsync("api/city/daynight");
            var stringResult = await httpResult.Content.ReadAsStringAsync();


            var desserializedObject = JsonSerializer.Deserialize<IEnumerable<AverageByDayNightModel>>(stringResult, _jsonOptions);

            Assert.NotEmpty(desserializedObject);
            Assert.True(desserializedObject.Count() == 2);
            Assert.True(desserializedObject.First().AvgTemperature > 0);
        }

        [Fact]
        public async Task OverallShouldReturnANonNullItem()
        {
            var client = _app.CreateClient();
            var httpResult = await client.GetAsync("api/city/overall");
            var stringResult = await httpResult.Content.ReadAsStringAsync();
            var desserializedObject = JsonSerializer.Deserialize<OverallAverageModel>(stringResult, _jsonOptions);

            Assert.NotNull(desserializedObject);
            Assert.True(desserializedObject.AvgTemperature > 0);
        }

        [Fact]
        public async Task AlertsShouldReturnFiveAlerts()
        {
            var client = _app.CreateClient();
            var httpResult = await client.GetAsync("api/city/alerts");
            var stringResult = await httpResult.Content.ReadAsStringAsync();
            var desserializedObject = JsonSerializer.Deserialize<IEnumerable<AlertDataModel>>(stringResult, _jsonOptions);
            var first = desserializedObject.First();

            Assert.Equal(5, desserializedObject.Count());
            Assert.NotEqual(string.Empty, first.AlertType);
        }
    }
}
