﻿using Infrastructure.Domain.ResponseModels;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Tests.WSN.User.API.Fixtures;
using Xunit;

namespace Tests.WSN.User.API.Controllers
{
    public class CellControllerTests : IClassFixture<UserAPIAppFactory>
    {
        private readonly UserAPIAppFactory _app;

        public CellControllerTests(UserAPIAppFactory app)
        {
            _app = app;
        }

        [Fact]
        public async Task SearchShouldReturnNonEmptyEnumerable()
        {
            var client = _app.CreateClient();
            var result = await client.GetAsync("api/cell/search/virtual");
            var stringResult = await result.Content.ReadAsStringAsync();
            result.EnsureSuccessStatusCode();

            var desserializedObject = JsonSerializer.Deserialize<IEnumerable<SearchCellModel>>(stringResult);
            var first = desserializedObject.First();

            Assert.NotEmpty(desserializedObject);
            Assert.NotEqual(string.Empty, first.Address);

        }

        [Fact]
        public async Task LastShouldReturnNotNullItem()
        {
            var client = _app.CreateClient();
            var result = await client.GetAsync("api/cell/last/2");
            var stringResult = await result.Content.ReadAsStringAsync();
            result.EnsureSuccessStatusCode();

            var options = new JsonSerializerOptions();
            options.PropertyNameCaseInsensitive = true;

            var desseriializedObject = JsonSerializer.Deserialize<RealTimeMeasurementsModel>(stringResult, options);

            Assert.NotNull(desseriializedObject);
            Assert.True(desseriializedObject.Temperature > 0);
        }

        [Fact]
        public async Task LastShouldReturn404()
        {
            var client = _app.CreateClient();
            var result = await client.GetAsync("api/cell/last/500");

            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }
    }
}
