﻿using Infrastructure.Domain.RequestModels;
using Infrastructure.Domain.ResponseModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Tests.WSN.User.API.Fixtures;
using Xunit;

namespace Tests.WSN.User.API.Controllers
{
    public class CsvControllerTests: IClassFixture<UserAPIAppFactory>
    {
        private readonly UserAPIAppFactory _app;

        public CsvControllerTests(UserAPIAppFactory app)
        {
            _app = app;
        }

        [Fact]
        public async Task DataShouldReturnAPathForAnExistingFile()
        {
            var client = _app.CreateClient();
            var request = new CsvDataRequestModel
            {
                Ids = new List<int>(),
                Start = DateTime.UtcNow.AddDays(-7),
                End = DateTime.UtcNow
            };

            var environment = new FakeEnvironmentVariables();

            var serializedString = JsonSerializer.Serialize(request);
            var encodedRequest = new StringContent(serializedString, Encoding.UTF8, "application/json");

            var result = await client.PostAsync("api/csv/data", encodedRequest);
            result.EnsureSuccessStatusCode();

            var serializedResult = await result.Content.ReadAsStringAsync();
            var jsonOptions = new JsonSerializerOptions {PropertyNameCaseInsensitive = true};
            var deserializedResult = JsonSerializer.Deserialize<CsvPathResponseModel>(serializedResult, jsonOptions);

            var csvPath = Path.Combine(environment.TempCsvDirectory, deserializedResult.FileName);
            Assert.True(File.Exists(csvPath));
            File.Delete(csvPath);
            Assert.False(File.Exists(csvPath));
        }
    }
}
