﻿using Microsoft.Extensions.DependencyInjection;
using Tests.WSN.User.API.Fixtures;
using WSN.User.API.Singleton.Interface;

namespace Tests.WSN.User.API.Extensions
{
    public static class FakeExtensions
    {
        public static IServiceCollection AddFakeDotEnv(this IServiceCollection services)
        {
            services.AddSingleton<IUserApiEnvironmentSingleton, FakeEnvironmentVariables>();

            return services;
        }
    }
}