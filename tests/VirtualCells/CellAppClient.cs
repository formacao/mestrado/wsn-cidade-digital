﻿using Infrastructure.Domain.Singletons;
using Infrastructure.Domain.Singletons.Interfaces;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using WSN.Cells.RequestModels;
using WSN.Cells.ResponseModels;

namespace VirtualCells
{
    public class CellAppClient
    {
        private readonly HttpClient _client;
        private readonly IEnvironmentSingleton _environment;

        public CellAppClient(IEnvironmentSingleton environment)
        {
            _environment = environment;

            _client = new HttpClient
            {
                BaseAddress = new Uri("http://localhost:5000")
            };

            Authenticate();
        }

        public async Task<MonitoringScheduleResponseModel> GetLastScheduleAsync(int cellId)
        {
            var result = await _client.GetAsync($"/api/monitoring/{cellId}");
            if (result.StatusCode != HttpStatusCode.OK) throw new InvalidOperationException("Could not get the last schedule");

            var resultString = await result.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<MonitoringScheduleResponseModel>(resultString);
        }

        public async Task SendMonitoringAsync(SendMonitoringRequestModel request)
        {
            var serializedJson = JsonSerializer.Serialize(request);
            var contentJson = new StringContent(serializedJson, Encoding.UTF8, "application/json");
            var result = await _client.PostAsync("/api/monitoring", contentJson);

            if (result.StatusCode != HttpStatusCode.Created) throw new InvalidOperationException("Could not send monitoring data");
        }

        private void Authenticate()
        {
            var loginRequest = new LoginRequestModel
            {
                User = UsersSingleton.Cell.ToLower(),
                Password = _environment.CellPassword
            };
            var serializedRequest = JsonSerializer.Serialize(loginRequest);
            var contentRequest = new StringContent(serializedRequest, Encoding.UTF8, "application/json");
            var result = _client.PostAsync("/api/login", contentRequest).Result;

            if (result.StatusCode != HttpStatusCode.OK) throw new InvalidOperationException("Unable to authenticate");

            var stringResult = result.Content.ReadAsStringAsync().Result;
            var desserializedResponse = JsonSerializer.Deserialize<TokenResponseModel>(stringResult);

            _client.DefaultRequestHeaders.Add("Authorization", $"Bearer {desserializedResponse.Token}");
        }
    }
}
