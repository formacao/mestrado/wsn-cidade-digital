﻿using Fixtures.Infrastructure.Singletons;
using Infrastructure.Domain.Singletons.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VirtualCells.Services;

namespace VirtualCells
{
    class Program
    {
        private static readonly IEnvironmentSingleton _environment = new InMemoryAppEnvironmentSingleton();

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the virtual cells system!");
            Console.WriteLine("Starting...");

            var cts = new CancellationTokenSource();

            var cellTasks = GetVirtualCells(cts.Token);
            var schedulerTask = GetVirtualScheduler(cts.Token);

            var tasks = new List<Task>();
            tasks.AddRange(cellTasks);
            tasks.Add(schedulerTask);

            _ = Console.ReadKey();

            Console.WriteLine("Canceling");
            cts.Cancel();
            Console.WriteLine("Waiting tasks...");
            Task.WaitAll(tasks.ToArray());
            cts.Dispose();

            Console.WriteLine("Finished");
        }

        private static IEnumerable<Task> GetVirtualCells(CancellationToken token)
        {
            var cells = new VirtualCellService[]
            {
                new VirtualCellService(2, _environment),
                new VirtualCellService(2002, _environment),
                new VirtualCellService(2003, _environment),
                new VirtualCellService(2004, _environment),
                new VirtualCellService(2005, _environment),
                new VirtualCellService(2006, _environment),
                new VirtualCellService(2007, _environment)
            };

            Console.WriteLine($"Started with {cells.Count()} cells");
            Console.WriteLine("");

            var tasks = cells.Select(c => Task.Factory.StartNew(() => c.StartAsync(token)));
            return tasks;
        }

        private static Task GetVirtualScheduler(CancellationToken token)
        {
            var service = new VirtualSchedulerService(_environment);
            return Task.Factory.StartNew(() => service.StartAsync(token));
        }
    }
}
