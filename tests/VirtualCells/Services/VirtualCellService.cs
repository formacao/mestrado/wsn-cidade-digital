﻿using Infrastructure.Domain.Singletons.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;
using VirtualCells.Singletons;
using WSN.Cells.RequestModels;

namespace VirtualCells.Services
{
    public class VirtualCellService
    {
        private readonly int _cellId;
        private readonly IEnvironmentSingleton _environment;

        public VirtualCellService(int cellId, IEnvironmentSingleton environment)
        {
            _cellId = cellId;
            _environment = environment;

            Console.WriteLine($"Cell created {_cellId}");
        }

        public async Task StartAsync(CancellationToken token = default)
        {
            Console.WriteLine($"Cell {_cellId} started!");
            try
            {
                var client = new CellAppClient(_environment);
                var timeBeginning = DateTime.Now;

                while (!token.IsCancellationRequested)
                {
                    if((DateTime.Now - timeBeginning).Minutes >= TimeSingleton.CellWaitTimeMinutes)
                    {
                        timeBeginning = DateTime.Now;
                        var lastSchedule = await client.GetLastScheduleAsync(_cellId);
                        if(!lastSchedule.DataSent)
                        {
                            var now = DateTime.Now;
                            var request = new SendMonitoringRequestModel
                            {
                                CellId = _cellId,
                                ScheduleId = lastSchedule.Id,
                                Temperature = MeasurementService.GetTemperature(now.Hour),
                                Humidity = MeasurementService.GetHumidity(now.Month),
                                UvRadiation = MeasurementService.GetUvRadiation(now.Hour),
                                Fire = MeasurementService.GetFire(),
                                Smoke = MeasurementService.GetSmokeOrToxicGas(),
                                ToxicGas = MeasurementService.GetSmokeOrToxicGas()
                            };

                            await client.SendMonitoringAsync(request);
                            Console.WriteLine($"Cell {_cellId} sent the data");
                        }
                        else
                        {
                            Console.WriteLine($"Data already sent for cell {_cellId}");
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                Console.WriteLine($"Virtual cell {_cellId} thrown an Exception:");
                Console.WriteLine(e.Message);
                Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
            }
        }
    }
}
