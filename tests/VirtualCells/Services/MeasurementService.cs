﻿using Microsoft.AspNetCore.Razor.Language;
using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualCells.Services
{
    public static class MeasurementService
    {
        public static readonly Random random = new Random();

        public static float GetTemperature(float hours) => (float)(Calculate24hParable(hours, 24, 42) * GetVariation());
        public static float GetHumidity(float month) => (float)(((5f / 4f) * Math.Pow(month, 2f) - 65f / 4f * month + 75f) * GetVariation());
        public static float GetUvRadiation(float hours) => (float)(Calculate24hParable(hours, 0, 8) * GetVariation());
        public static float GetFire() => (float)random.NextDouble();
        public static float GetSmokeOrToxicGas() => random.Next(0, 1000);

        private static float Calculate24hParable(float x, float yMin, float yMax)
        {
            var gradient = yMax - yMin;
            var a = (-3f/ 288f) * gradient;
            var b = (5f / 24f) * gradient;
            var c = yMin;

            return (float)(a * Math.Pow(x, 2) + b * x + c);
        }
        private static double GetVariation() => random.NextDouble() * 0.6 + 0.7; //30% upward or downwards
    }
}
