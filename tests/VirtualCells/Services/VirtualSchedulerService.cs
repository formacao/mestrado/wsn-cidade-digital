﻿using Domain.Models;
using EFCoreInfrastructure;
using Infrastructure.Domain.Singletons.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using VirtualCells.Singletons;

namespace VirtualCells.Services
{
    public class VirtualSchedulerService
    {
        private readonly IEnvironmentSingleton _environment;

        public VirtualSchedulerService(IEnvironmentSingleton environment)
        {
            _environment = environment;

            Console.WriteLine("Scheduler created");
        }

        public async Task StartAsync(CancellationToken token)
        {
            Console.WriteLine("Scheduler started");
            try
            {
                var timeBeginning = DateTime.Now;
                while (!token.IsCancellationRequested)
                {
                    if ((DateTime.Now - timeBeginning).Minutes >= TimeSingleton.SchedulerWaitTimeMinutes)
                    {
                        timeBeginning = DateTime.Now;
                        using var context = GetContext();
                        context.MonitoringSchedules.Add(new MonitoringScheduleModel
                        {
                            MonitoringDateTime = DateTime.UtcNow
                        });

                        if (await context.SaveChangesAsync() != 1) 
                            throw new InvalidOperationException("Unable to schedule monitoring");
                        Console.WriteLine("Schedule sent");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                Console.WriteLine("The scheduler thrown an Exception");
                Console.WriteLine(e.Message);
                Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
            }
        }

        private Context GetContext()
        {
            var options = new DbContextOptionsBuilder().UseSqlServer(_environment.ConnectionString);
            return new Context(options.Options);
        }
    }
}
