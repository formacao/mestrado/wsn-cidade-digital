﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualCells.Singletons
{
    public class TimeSingleton
    {
        public const int CellWaitTimeMinutes = 1;
        public const int SchedulerWaitTimeMinutes = 2;

    }
}
