﻿using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.ResponseModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fixtures.DataProcessingInfrastructure.Repositories
{
    public class CellDataRepositoryFixture : ICellDataRepository
    {
        public Task<bool> CheckExistence(int id) => Task.FromResult(id != 500);

        public async Task<IEnumerable<AverageByDayNightModel>> GetAverageByDayNight(int cellId, DateTime start, DateTime end) => await Task.Run(() => new List<AverageByDayNightModel>
        {
            new AverageByDayNightModel
            {
                AvgFire = 0.89f,
                AvgHumidity = 36,
                AvgSmoke = 201,
                AvgTemperature = 33,
                AvgToxicGas = 254,
                AvgUv = 5,
                DayNight = "Day"
            },

            new AverageByDayNightModel
            {
                AvgFire = 0.50f,
                AvgHumidity = 32,
                AvgSmoke = 217,
                AvgTemperature = 27,
                AvgToxicGas = 224,
                AvgUv = 1,
                DayNight = "Night"
            }
        });

        public Task<RealTimeMeasurementsModel> GetLastMeasurementAsync(int id) => Task.FromResult(new RealTimeMeasurementsModel
        {
            Temperature = 34,
            Humidity = 35, 
            Fire = 0.8f,
            Smoke = 443,
            ToxicGas = 787,
            UvRadiation = 5
        });

        public async Task<OverallAverageModel> GetOverallAverage(int cellId, DateTime start, DateTime end) => await Task.FromResult(new OverallAverageModel
        {
            AvgFire = 0.35f,
            AvgUv = 3.5f,
            AvgHumidity = 31,
            AvgSmoke = 200,
            AvgTemperature = 29,
            AvgToxicGas = 137
        });

        public async Task<IEnumerable<SearchCellModel>> GetSearchResultAsync(string search) => await Task.Run(() => new List<SearchCellModel>
        {
            new SearchCellModel {Id = 1, Address = "Test 01"},
            new SearchCellModel {Id = 2, Address = "Test 02"}
        });

        public async Task<IEnumerable<TimeSeriesModel>> GetTimeSeriesAsync(int cellId, DateTime start, DateTime end, string columnName) => await Task.Run(() => new List<TimeSeriesModel>
        {
            new TimeSeriesModel {Value = 80.2f, DateTime = DateTime.Now.AddMinutes(-5)},
            new TimeSeriesModel {Value = 2f, DateTime = DateTime.Now.AddMinutes(12)}
        });

    }
}
