﻿using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fixtures.DataProcessingInfrastructure.Repositories
{
    public class AlertStatisticsRepositoryFixture : IAlertStatisticsRepository
    {
        public async Task<IEnumerable<AlertDataModel>> GetOverall() =>
            await Task.Run(() => new List<AlertDataModel>
            {
                new AlertDataModel { Address = "Test 01", AlertType = "Test Alert"},
                new AlertDataModel { Address = "Test 02", AlertType = "Test Alert"},
                new AlertDataModel { Address = "Test 03", AlertType = "Test Alert"},
                new AlertDataModel { Address = "Test 04", AlertType = "Test Alert"},
                new AlertDataModel { Address = "Test 05", AlertType = "Test Alert"}
            });
    }
}
