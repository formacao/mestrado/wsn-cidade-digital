﻿using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fixtures.DataProcessingInfrastructure.Repositories
{
    public class CityStatisticsRepositoryFixture : ICityStatisticsRepository
    {
        public async Task<IEnumerable<AverageByDayNightModel>> GetAverageByDayNightAsync() => await Task.Run(() => new List<AverageByDayNightModel>
        {
            new AverageByDayNightModel {AvgTemperature = 41f, AvgFire = 0.3f, AvgHumidity = 35f, AvgSmoke=443f, AvgToxicGas = 854f, AvgUv = 5f, DayNight = "Day"},
            new AverageByDayNightModel {AvgTemperature = 29f, AvgFire = 0.3f, AvgHumidity = 37f, AvgSmoke=443f, AvgToxicGas = 854f, AvgUv = 1f, DayNight = "Night"}
        });

        public Task<OverallAverageModel> GetOverallAverageAsync() => Task.FromResult(new OverallAverageModel
        {
            AvgUv = 3f,
            AvgToxicGas = 597f,
            AvgSmoke = 447f,
            AvgHumidity = 38f,
            AvgFire = 0.3f,
            AvgTemperature = 33f
        });
    }
}
