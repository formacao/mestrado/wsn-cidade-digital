﻿using Infrastructure.Domain.Repositories.Interfaces;
using Infrastructure.Domain.RequestModels;
using Infrastructure.Domain.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Fixtures.DataProcessingInfrastructure.Repositories
{
    public class CsvDataRepositoryFixture : ICsvDataRepository
    {
        public async Task<IEnumerable<CsvRowModel>> GetCsvDataAsync(CsvDataRequestModel request) => await Task.FromResult(new List<CsvRowModel>
        {
            new CsvRowModel {Temperature = 38, Humidity = 23, Fire = 0.1f, Address = "Fixture", Latitude = 18, Longitude = -10, MonitoringMoment = DateTime.UtcNow, Name = "Fixture", Smoke = 384, ToxicGas = 227, UvRadiation = 4}
        });
    }
}
