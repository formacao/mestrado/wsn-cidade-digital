﻿using Fixtures.DataProcessingInfrastructure.Repositories;
using Infrastructure.Domain.Repositories.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Fixtures.DataProcessingInfrastructure.Extensions
{
    public static class FixtureDataProcessingExtensions
    {
        public static IServiceCollection AddFixtureDataProcessingRepositories(this IServiceCollection services)
        {
            services
                .AddScoped<IAlertStatisticsRepository, AlertStatisticsRepositoryFixture>()
                .AddScoped<ICityStatisticsRepository, CityStatisticsRepositoryFixture>()
                .AddScoped<ICellDataRepository, CellDataRepositoryFixture>()
                .AddScoped<ICsvDataRepository, CsvDataRepositoryFixture>()
                .AddScoped<ICheckCellExistenceRepository>(sp => sp.GetRequiredService<ICellDataRepository>());

            return services;
        }
    }
}
