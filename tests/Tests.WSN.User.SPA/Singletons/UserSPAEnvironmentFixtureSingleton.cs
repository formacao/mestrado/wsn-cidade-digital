﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WSN.User.SPA.Singletons.Interfaces;

namespace Tests.WSN.User.SPA.Singletons
{
    public class UserSPAEnvironmentFixtureSingleton : IUserSpaEnvironmentSingleton
    {
        public string ApiBaseAddress => throw new InvalidOperationException();
        public string CsvBaseAddress => throw new InvalidOperationException();
        public string MapBoxJSAccessToken => throw new InvalidOperationException();
    }
}
