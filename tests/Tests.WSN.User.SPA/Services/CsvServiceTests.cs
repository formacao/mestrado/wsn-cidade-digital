﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Infrastructure.Domain.RequestModels;
using Tests.WSN.User.API.Fixtures;
using WSN.User.SPA.Services;
using WSN.User.SPA.Services.Interfaces;
using Xunit;
using Xunit.Sdk;

namespace Tests.WSN.User.SPA.Services
{
    public class CsvServiceTests : IClassFixture<UserAPIAppFactory>
    {
        public CsvServiceTests(UserAPIAppFactory app)
        {
            _app = app;
        }

        private readonly UserAPIAppFactory _app;

        [Fact]
        public async Task GetCsvLocationAsyncShouldReturnAPathToExistingFile()
        {
            var client = _app.CreateClient();
            ICsvService service = new CsvService(client, null);
            var apiEnvironmentVariables = new FakeEnvironmentVariables();
            
            var request = new CsvDataRequestModel
            {
                Ids = new List<int>(),
                Start = DateTime.Now.AddDays(-2),
                End = DateTime.Now
            };

            var (hasFile, fileName) = await service.GetCsvLocationAsync(request);
            var filePath = Path.Combine(apiEnvironmentVariables.TempCsvDirectory, fileName);
            Assert.True(hasFile);
            Assert.True(File.Exists(filePath));
            
            File.Delete(filePath);
            Assert.False(File.Exists(filePath));
        }
    }
}