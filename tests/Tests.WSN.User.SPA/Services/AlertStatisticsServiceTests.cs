﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tests.WSN.User.API.Fixtures;
using WSN.User.SPA.Services;
using Xunit;

namespace Tests.WSN.User.SPA.Services
{
    public class AlertStatisticsServiceTests : IClassFixture<UserAPIAppFactory>
    {
        private readonly UserAPIAppFactory _app;

        public AlertStatisticsServiceTests(UserAPIAppFactory app)
        {
            _app = app;
        }

        [Fact]
        public async Task GetAlertAsyncShouldReturnNonEmptyEnumerable()
        {
            var client = _app.CreateClient();
            var service = new AlertsStatisticsService(client);
            var result = await service.GetAlertsAsync();
            var first = result.First();

            Assert.True(result.Count() > 0);
            Assert.NotEqual(string.Empty, first.AlertType);
        }
    }
}
