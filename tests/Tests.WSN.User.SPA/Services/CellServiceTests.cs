﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tests.WSN.User.API.Fixtures;
using WSN.User.SPA.Services;
using Xunit;

namespace Tests.WSN.User.SPA.Services
{
    public class CellServiceTests : IClassFixture<UserAPIAppFactory>
    {
        private readonly UserAPIAppFactory _app;

        public CellServiceTests(UserAPIAppFactory app)
        {
            _app = app;
        }

        [Fact]
        public async Task GetRealTimeMeasurementsAsyncShouldReturnANonNullItem()
        {
            var sut = GetSut();

            var result = await sut.GetRealTimeMeasurementsAsync(8745);
            Assert.NotNull(result);
        }

        [Fact]
        public async Task GetSearchResultAsyncShouldReturnANonNullItem()
        {
            var sut = GetSut();

            var result = await sut.GetSearchResultAsync("any");

            Assert.NotNull(result);
        }

        [Fact]
        public async Task GetTimeSeriesAsyncShouldReturnANonEmptyEnumerable()
        {
            var sut = GetSut();

            var result = await sut.GetTimeSeriesAsync(5, DateTime.Now, DateTime.UtcNow, "");

            Assert.True(result.Count() > 0);
        }

        [Fact]
        public async Task GetAverageByDayNightShouldReturnANonEmptyEnumerable()
        {
            var sut = GetSut();

            var result = await sut.GetAverageByDayNightAsync(2, DateTime.Now, DateTime.UtcNow);

            Assert.True(result.Count() > 0);
        }

        [Fact]
        public async Task GetOverallAverageShouldReturnANonNullItem()
        {
            var sut = GetSut();

            var result = await sut.GetOverallAverageAsync(2, DateTime.Now, DateTime.UtcNow);
            Assert.NotNull(result);
        }

        private CellService GetSut()
        {
            var client = _app.CreateClient();
            return new CellService(client);
        }
    }
}
