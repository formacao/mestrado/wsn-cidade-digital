﻿using System.Linq;
using System.Threading.Tasks;
using Tests.WSN.User.API.Fixtures;
using WSN.User.SPA.Services;
using Xunit;

namespace Tests.WSN.User.SPA.Services
{
    public class CityStatisticsServiceTests : IClassFixture<UserAPIAppFactory>
    {
        private readonly UserAPIAppFactory _api;

        public CityStatisticsServiceTests(UserAPIAppFactory api)
        {
            _api = api;
        }

        [Fact]
        public async Task GetByDayNightAsyncShouldReturnANonNullEnumerable()
        {
            var client = _api.CreateClient();

            var service = new CityStatisticsService(client);
            var data = await service.GetByDayNightAsync();
            var first = data.First();

            Assert.NotEmpty(data);
            Assert.True(data.Count() == 2);
            Assert.True(first.AvgSmoke > 0);
        }

        [Fact]
        public async Task GetOverallAsyncShouldReturnANonNullItem()
        {
            var client = _api.CreateClient();

            var service = new CityStatisticsService(client);
            var data = await service.GetOverallAsync();

            Assert.NotNull(data);
            Assert.True(data.AvgTemperature > 0);
        }
    }
}
