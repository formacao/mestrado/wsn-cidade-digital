﻿using Domain.Models;
using Fixtures.Infrastructure;
using EFCoreInfrastructure;
using EFCoreInfrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Infrastructure.Domain.Repositories.Interfaces;

namespace Tests.Infrastructure.Tests.Repositories
{
    public class ScheduleRepositoryTests
    {
        private readonly string _database = Guid.NewGuid().ToString();

        public ScheduleRepositoryTests()
        {
            var (context, _) = GetAccessLayer();

            var dummySchedule01 = new MonitoringScheduleModel
            {
                MonitoringDateTime = DateTime.UtcNow.AddHours(-2)
            };

            var dummySchedule02 = new MonitoringScheduleModel
            {
                MonitoringDateTime = new DateTime(2005, 03, 05)
            };

            context.AddRange(dummySchedule01, dummySchedule02);
            if (context.SaveChanges() != 2) throw new InvalidOperationException("Unable to save dummy data");
        }

        [Fact]
        public async Task GetAsyncShouldReturnAllItens()
        {
            var (_, repository) = GetAccessLayer();
            var schedules = await repository.GetAsync();
            Assert.True(schedules.Count() > 0);
        }

        [Fact]
        public async Task AddShouldAddAnItem()
        {
            var (_, repository) = GetAccessLayer();
            var schedule = new MonitoringScheduleModel
            {
                MonitoringDateTime = DateTime.UtcNow.AddHours(1)
            };
            repository.Add(schedule);
            Assert.Equal(1, await repository.UnitOfWork.SaveChangesAsync());
        }


        private (Context, IMonitoringScheduleRepository repository) GetAccessLayer()
        {
            var options = new DbContextOptionsBuilder().UseInMemoryDatabase(_database);
            var context = new InMemoryContext(options.Options);
            var repository = new MonitoringScheduleRepository(context, context);

            return (context, repository);
        }
    }
}
