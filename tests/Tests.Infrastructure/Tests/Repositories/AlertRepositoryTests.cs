﻿using Domain.Enums;
using Domain.Models;
using Fixtures.Infrastructure;
using EFCoreInfrastructure;
using EFCoreInfrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests.Infrastructure.Tests.Repositories
{
    public class AlertRepositoryTests
    {
        private readonly string _database = Guid.NewGuid().ToString();

        [Theory]
        [ClassData(typeof(AlertRepositoryTestData))]
        public async Task AddAsyncShouldAddAnAlertGivenMonitoringWithAlerts(MonitoringModel monitoring, IEnumerable<AlertTypesEnum> expectedAlerts)
        {
            using var context = GetInMemoryContext();
            var repository = new AlertRepository(context, context);
            var returnedAlerts = repository.Add(monitoring);

            Assert.True(await repository.UnitOfWork.SaveChangesAsync() > 0);
            Assert.Equal(expectedAlerts.Count(), returnedAlerts.Count());
            foreach (var alert in expectedAlerts)
            {
                Assert.Contains(alert, returnedAlerts);
            }

        }

        private Context GetInMemoryContext()
        {
            var options = new DbContextOptionsBuilder().UseInMemoryDatabase(_database);
            var context = new InMemoryContext(options.Options);

            return context;
        }
            
    }

    public class AlertRepositoryTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { GetMonitoringWithTemperatureAndHumidityAlerts(), new AlertTypesEnum[] { AlertTypesEnum.HighTemperature, AlertTypesEnum.LowHumidity } };
            yield return new object[]
            {
                GetMonitoringWithUvAndFireAndSmokeAndGasAlerts(),
                new AlertTypesEnum[] {AlertTypesEnum.FireDetected, AlertTypesEnum.HighUvRadiation, AlertTypesEnum.SmokeDetected, AlertTypesEnum.ToxicGasDetected}
            };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private MonitoringModel GetMonitoringWithTemperatureAndHumidityAlerts() => new MonitoringModel
        {
            Id = 12,
            MonitoringScheduleId = 3,
            Temperature = 40,
            Humidity = 12,
            Fire = 0.2f,
            Smoke = 400,
            ToxicGas = 400,
            UvRadiation = 2
        };

        private MonitoringModel GetMonitoringWithUvAndFireAndSmokeAndGasAlerts() => new MonitoringModel
        {
            Id = 10,
            MonitoringScheduleId = 2,
            Temperature = 20,
            Humidity = 45,
            Fire = 100f,
            Smoke = 1200,
            ToxicGas = 1200,
            UvRadiation = 8
        };
    }
}
