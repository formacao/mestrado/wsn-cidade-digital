﻿using Domain.Models;
using Fixtures.Infrastructure;
using EFCoreInfrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Infrastructure.Domain.Repositories.Interfaces;

namespace Tests.Infrastructure.Tests.Repositories
{
    public class MonitoringRepositoryTests
    {

        private readonly string _database;
        private MonitoringModel _dummyMonitoring;
        private CellModel _dummyCell;
        private MonitoringScheduleModel _dummySchedule;

        public MonitoringRepositoryTests()
        {
            _database = Guid.NewGuid().ToString();

            InsertDummyCell();
            _dummySchedule = InsertDummySchedule();
            InsertMonitoring();
        }

        [Fact]
        public async Task AddAsyncShouldAddNewItem()
        {
            var newDummySchedule = InsertDummySchedule();
            using var context = GetContext();
            var repository = GetRepository(context);

            var newMonitoring = new MonitoringModel
            {
                MonitoringScheduleId = newDummySchedule.Id,
                CellId = _dummyCell.Id,
                Temperature = 87,
                Fire = 12,
                Humidity = 25,
                Smoke = 488,
                ToxicGas = 789,
                UvRadiation = 12
            };

            var insertedMonitoring = repository.Add(newMonitoring);

            Assert.True(await repository.UnitOfWork.SaveChangesAsync() > 0);
            Assert.NotNull(insertedMonitoring);
        }

        [Fact]
        public async Task AddAsyncWithRepeatedScheduleAndCellShouldReturnNull()
        {
            using var context = GetContext();
            var repository = GetRepository(context);

            var newMonitoring = new MonitoringModel
            {
                MonitoringScheduleId = _dummySchedule.Id,
                CellId = _dummyCell.Id,
                Temperature = -2,
                Fire = 7,
                Humidity = 57,
                Smoke = 10,
                ToxicGas = 57,
                UvRadiation = 3
            };

            var insertedMonitoring = repository.Add(newMonitoring);
            Assert.True(await repository.UnitOfWork.SaveChangesAsync() == 0);
            Assert.Null(insertedMonitoring);
        }

        [Fact]
        public async Task GetAsyncShouldReturnAllItens()
        {
            using var context = GetContext();
            var repository = GetRepository(context);

            var items = await repository.GetAsync();
            Assert.True(items.Count() > 0);
        }

        [Fact]
        public async Task GetAsyncShouldReturnAnItem()
        {
            using var context = GetContext();
            var repository = GetRepository(context);

            Assert.NotNull(await repository.GetAsync(_dummyMonitoring.Id));
        }

        [Fact]
        public async Task GetAsyncShouldReturnNullGivenNonExistingId()
        {
            using var context = GetContext();
            var repository = GetRepository(context);

            Assert.Null(await repository.GetAsync(800));
        }


        private void InsertMonitoring()
        {
            using var context = GetContext();
            _dummyMonitoring = new MonitoringModel
            {
                MonitoringScheduleId = _dummySchedule.Id,
                CellId = _dummyCell.Id,
                Temperature = 42,
                Fire = 58,
                Humidity = 17,
                Smoke = 443,
                ToxicGas = 587,
                UvRadiation = 4
            };

            context.MonitoringData.Add(_dummyMonitoring);
            if (context.SaveChanges() != 1) throw new InvalidOperationException("Unable to save dummy monitoring");
        }

        private MonitoringScheduleModel InsertDummySchedule()
        {
            using var context = GetContext();
            var dummySchedule = new MonitoringScheduleModel
            {
                MonitoringDateTime = DateTime.UtcNow
            };

            context.MonitoringSchedules.Add(dummySchedule);
            if (context.SaveChanges() != 1) throw new InvalidOperationException("Unable to save dummy monitorin schedule");

            return dummySchedule;
        }

        private void InsertDummyCell()
        {
            using var context = GetContext();
            _dummyCell = new CellModel
            {
                Name = "Dummy Cell",
                Address = "Dummy Address",
                Latitude = 12.5,
                Longitude = -18.69
            };

            context.Cells.Add(_dummyCell);
            if (context.SaveChanges() != 1) throw new InvalidOperationException("Unable to save dummy cell");
        }

        private IMonitoringRepository GetRepository(InMemoryContext context) => new MonitoringRepository(context, context);

        private InMemoryContext GetContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder().UseInMemoryDatabase(_database);
            var context = new InMemoryContext(optionsBuilder.Options);
            return context;
        }
    }
}
