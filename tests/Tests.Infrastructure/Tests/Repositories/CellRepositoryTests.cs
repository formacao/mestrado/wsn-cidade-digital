﻿using Domain.Models;
using Fixtures.Infrastructure;
using EFCoreInfrastructure;
using EFCoreInfrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Infrastructure.Domain.Repositories.Interfaces;

namespace Tests.Infrastructure.Tests.Repositories
{
    public class CellRepositoryTests
    {
        private readonly (CellModel, CellModel) _dummyCells;
        private readonly string _database = Guid.NewGuid().ToString();

        public CellRepositoryTests()
        {
            _dummyCells = InsertDummyCells();
        }


        [Fact]
        public async Task GetAsyncShouldReturnAnIEnumerableWithItens()
        {
            var (_, repository) = GetAccessLayer();

            var itens = await repository.GetAsync();
            Assert.True(itens.Count() > 0);
        }

        [Fact]
        public async Task GetAsyncByIdShouldReturnAnItem()
        {
            var (_, repository) = GetAccessLayer();

            var item = await repository.GetAsync(_dummyCells.Item1.Id);
            Assert.NotNull(item);
        }

        [Fact]
        public void AddShouldAddAnItem()
        {
            var (context, repository) = GetAccessLayer();

            var dummyCell = new CellModel
            {
                Name = "New Cell",
                Address = "New Address",
                Latitude = 85.658,
                Longitude = -1.245
            };

            repository.Add(dummyCell);
            repository.UnitOfWork.SaveChanges();

            Assert.True(dummyCell.Id > 0);
        }

        [Fact]
        public async Task EdtiShouldEditItem()
        {
            var (context, repository) = GetAccessLayer();

            var editedCell = new CellModel
            {
                Id = _dummyCells.Item1.Id,
                Name = "Edited Name",
                Address = "Edited Address",
                Latitude = 21.587,
                Longitude = -.257
            };

            repository.Update(editedCell);
            await repository.UnitOfWork.SaveChangesAsync();

            var item = await repository.GetAsync(_dummyCells.Item1.Id);
            Assert.Equal(editedCell.Name, item.Name);
            Assert.Equal(editedCell.Address, item.Address);
            Assert.Equal(editedCell.Latitude, item.Latitude);
            Assert.Equal(editedCell.Longitude, item.Longitude);
        }

        [Fact]
        public async Task DeleteShouldMakeDeleteTrue()
        {
            var (context, repository) = GetAccessLayer();

            repository.Delete(_dummyCells.Item2);
            await repository.UnitOfWork.SaveChangesAsync();

            var item = await repository.GetAsync(_dummyCells.Item2.Id);
            Assert.Null(item);
        }

        private (CellModel, CellModel) InsertDummyCells()
        {
            (var context, _) = GetAccessLayer();

            var dummyCell01 = new CellModel
            {
                Name = "Dummy Cell 01",
                Address = "Dummy Address 01",
                Latitude = 12.35874,
                Longitude = -1.13587
            };

            var dummyCell02 = new CellModel
            {
                Name = "Dummy Cell 02",
                Address = "Dummy Address 02",
                Latitude = -89.6587,
                Longitude = -.587
            };

            context.Cells.AddRange(dummyCell01, dummyCell02);
            context.SaveChanges();
            context.Dispose();

            return (dummyCell01, dummyCell02);
        }

        private (Context, ICellRepository) GetAccessLayer()
        {
            var options = new DbContextOptionsBuilder().UseInMemoryDatabase(_database);
            var context = new InMemoryContext(options.Options);
            var repository = new CellRepository(context, context);

            return (context, repository);
        }
    }
}
