﻿using Fixtures.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Tests.WSN.Admin.Fixtures;
using WSN.Admin;
using Xunit;

namespace Tests.WSN.Admin.Tests.Controllers
{
    public class SchedulesControllerTests : IClassFixture<AdminAppFactory>
    {
        private readonly AdminAppFactory _app;

        public SchedulesControllerTests(AdminAppFactory app)
        {
            _app = app;
        }

        [Fact]
        public async Task GetIndexShouldReturnOk()
        {
            var client = await _app.GetAuthenticatedClient();
            var result = await client.GetAsync("/schedules");

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task NewShouldReturnCreated()
        {
            var client = await _app.GetAuthenticatedClient();
            var result = await client.GetAsync("/schedules/new");

            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
        }
    }
}
