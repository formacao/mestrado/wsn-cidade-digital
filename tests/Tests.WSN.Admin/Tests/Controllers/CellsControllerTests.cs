﻿using Domain.Models;
using Fixtures.Infrastructure.Seeders;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Tests.WSN.Admin.Fixtures;
using WSN.Admin.RequestModels;
using Xunit;

namespace Tests.WSN.Admin.Tests.Controllers
{
    public class CellsControllerTests : IClassFixture<AdminAppFactory>, IClassFixture<DummyDataSeeder>
    {
        private readonly AdminAppFactory _app;
        private readonly DummyDataSeeder _seeder;
        private readonly string _erro404 = "Erro 404";

        private CellModel _dummyCell;

        public CellsControllerTests(AdminAppFactory app, DummyDataSeeder seeder)
        {
            _app = app;
            _seeder = seeder;

            using var context = _app.GetContext();
           _dummyCell = _seeder.InsertDummyCell(context);
        }

        [Fact]
        public async Task AddCellShouldBeSuccessful()
        {
            var cellData = new AddCellRequestModel
            {
                Name = "Test Cell",
                Address = "Test Address",
                Latitude = 23.254787,
                Longitude = -18.654789
            };
            using var client = await _app.GetAuthenticatedClient();

            var result = await CreateCell(client, cellData);
            var context = _app.GetContext();

            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
            Assert.Equal(1, context.Cells.Where(c => c.Latitude == cellData.Latitude && c.Longitude == cellData.Longitude).Count());

        }

        [Fact]
        public async Task EditCellShouldReturn404CustomPage()
        {
            using var client = await _app.GetAuthenticatedClient();
            var formData = new MultipartFormDataContent
            {
                {new StringContent(8000.ToString()), nameof(EditCellRequestModel.CellId) }
            };
            var result = await client.PostAsync("/Cells/SaveEdit", formData);
            var stringContent = await result.Content.ReadAsStringAsync();

            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
            Assert.Contains(_erro404, stringContent);
        }

        [Fact]
        public async Task EditCellShouldBeSuccessful()
        {
            using var client = await _app.GetAuthenticatedClient();
            var cellData = new EditCellRequestModel
            {
                CellId = _dummyCell.Id,
                Name = _dummyCell.Name,
                Address = "Edited",
                Latitude = 8.58,
                Longitude = -.587
            };
            var formData = new MultipartFormDataContent
            {
                {new StringContent(cellData.CellId.ToString()), nameof(EditCellRequestModel.CellId) },
                {new StringContent(cellData.Name), nameof(EditCellRequestModel.Name) },
                {new StringContent(cellData.Address), nameof(EditCellRequestModel.Address) },
                {new StringContent(cellData.Latitude.ToString()), nameof(EditCellRequestModel.Latitude) },
                {new StringContent(cellData.Longitude.ToString()), nameof(EditCellRequestModel.Longitude) }
            };
            var result = await client.PostAsync("/cells/saveedit", formData);
            var context = _app.GetContext();

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Equal(1, context.Cells.Where(c => c.Address == "Edited").Count());
        }

        [Fact]
        public async Task DeleteShouldDeleteItem()
        {
            var clientToCreate = await _app.GetAuthenticatedClient();
            var newCell = new AddCellRequestModel
            {
                Name = "Delete this one",
                Address = "Delete",
                Latitude = 0,
                Longitude = -.877
            };
            var resultCreation = await CreateCell(clientToCreate, newCell);
            Assert.Equal(HttpStatusCode.Created, resultCreation.StatusCode);
            var context = _app.GetContext();
            var cellToDelete = context.Cells.Where(c => c.Name == newCell.Name).First();

            var clientToDelete = await _app.GetAuthenticatedClient();
            var resultDeletion = await clientToDelete.GetAsync($"/cells/delete/{cellToDelete.Id}");
            context = _app.GetContext();
            var deletedCell = context.Cells.Where(c => c.Id == cellToDelete.Id).First();

            Assert.Equal(HttpStatusCode.OK, resultDeletion.StatusCode);
            Assert.True(deletedCell.Deleted);
        }

        [Fact]
        public async Task DeleteShouldReturnCustom404Page()
        {
            var client = await _app.GetAuthenticatedClient();
            var result = await client.GetAsync("/cells/delete/8000");
            var stringResult = await result.Content.ReadAsStringAsync();

            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
            Assert.Contains(_erro404, stringResult);
        }

        private async Task<HttpResponseMessage> CreateCell(HttpClient client, AddCellRequestModel request)
        {
            var formData = new MultipartFormDataContent
            {
                {new StringContent(request.Name), nameof(AddCellRequestModel.Name) },
                {new StringContent(request.Address), nameof(AddCellRequestModel.Address) },
                {new StringContent(request.Latitude.ToString()), nameof(AddCellRequestModel.Latitude) },
                {new StringContent(request.Longitude.ToString()), nameof(AddCellRequestModel.Longitude) }
            };

            return await client.PostAsync("/cells/add", formData);
        }

    }
}
