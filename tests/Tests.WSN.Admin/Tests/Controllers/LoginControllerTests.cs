﻿using Fixtures.Infrastructure.Singletons;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Tests.WSN.Admin.Fixtures;
using WSN.Admin.RequestModels;
using Xunit;
using Infrastructure.Domain.Singletons.Interfaces;
using Infrastructure.Domain.Singletons;

namespace Tests.WSN.Admin.Tests.Controllers
{
    public class LoginControllerTests : IClassFixture<AdminAppFactory>
    {
        private readonly AdminAppFactory _app;
        private readonly IEnvironmentSingleton _environment;

        public LoginControllerTests(AdminAppFactory app)
        {
            _app = app;
            _environment = new InMemoryAppEnvironmentSingleton();
        }

        [Fact]
        public async Task LoginShouldBeSuccessful()
        {
            var client = _app.CreateClient();
            var result = await Login(client, UsersSingleton.Admin.ToLower(), _environment.AdminPassword);
            result.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task LoginShouldReturnNotFound()
        {
            var client = _app.CreateClient();
            var result = await Login(client, "wrong", "wrong");

            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task PanelShouldReturnUnauthorized()
        {
            var client = _app.CreateClient();
            await Login(client, UsersSingleton.Cell.ToLower(), _environment.CellPassword);

            var result = await client.GetAsync("/Panel");
            var stringContent = await result.Content.ReadAsStringAsync();

            Assert.Contains("Acesso negado!", stringContent);
            
        }

        private async Task<HttpResponseMessage> Login(HttpClient client, string user, string password)
        {
            var formData = new MultipartFormDataContent
            {
                { new StringContent(user), nameof(LoginRequestModel.User) },
                { new StringContent(password), nameof(LoginRequestModel.Password) }
            };

            return await client.PostAsync("/login/authenticate", formData);
        }
    }
}
