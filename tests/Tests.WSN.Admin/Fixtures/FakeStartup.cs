﻿using Fixtures.Infrastructure;
using Fixtures.Infrastructure.Extensions;
using EFCoreInfrastructure.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WSN.Admin;
using WSN.Admin.Extensions;

namespace Tests.WSN.Admin.Fixtures
{
    public class FakeStartup : Startup
    {
        public FakeStartup(IConfiguration configuration) : base(configuration)
        {
        }

        public override void AddInfrastructure(IServiceCollection services)
        {
            services
                .AddInfrastructureFixturesSingletons()
                .AddFixturesInMemoryDatabase(Guid.NewGuid().ToString())
                .AddUnitOfWork()
                .ConfigureDatabase()
                .AddRepositories();
        }

        public override void AddAdminApplication(IServiceCollection services)
        {
            services
                .AddAdminApplicationMappers()
                .AddAdminApplicationServices()
                .AddAdminApplicationAuthenticationConfigurations();
        }
    }
}
