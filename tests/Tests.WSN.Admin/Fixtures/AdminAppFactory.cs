﻿using Fixtures.Infrastructure;
using Infrastructure.Domain.RequestModels.Interfaces;
using Infrastructure.Domain.Singletons;
using System.Net.Http;
using System.Threading.Tasks;
using WSN.Admin;

namespace Tests.WSN.Admin.Fixtures
{
    public class AdminAppFactory : AppFactory<Program, FakeStartup>, IAuthenticableClient
    {
        public async Task<HttpClient> GetAuthenticatedClient()
        {
            var client = CreateClient();
            var formData = new MultipartFormDataContent
            {
                {new StringContent(UsersSingleton.Admin.ToLower()), nameof(ILoginRequestModel.User) },
                {new StringContent(_environment.AdminPassword), nameof(ILoginRequestModel.Password) }
            };
            await client.PostAsync("/login/authenticate", formData);

            return client;
        }
    }
}
