﻿using Fixtures.Infrastructure.Singletons;
using EFCoreInfrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Infrastructure.Domain.Singletons.Interfaces;

namespace Fixtures.Infrastructure
{
    public abstract class AppFactory<TEntryPoint, TStartup> : WebApplicationFactory<TEntryPoint> where TEntryPoint : class where TStartup: class
    {
        protected readonly IEnvironmentSingleton _environment;

        public AppFactory() : base()
        {
            _environment = new InMemoryAppEnvironmentSingleton();
        }

        protected override IHostBuilder CreateHostBuilder()
        {
            var builder = Host.CreateDefaultBuilder()
                .ConfigureWebHostDefaults(x =>
                {
                    x.UseTestServer();
                    x.UseEnvironment("Development");
                    x.UseStartup<TStartup>();
                });

            return builder;
        }

        public Context GetContext()
        {
            var scopedFactory = Services.GetRequiredService<IServiceScopeFactory>();
            var scope = scopedFactory.CreateScope();
            return scope.ServiceProvider.GetRequiredService<Context>();
        }
    }
}
