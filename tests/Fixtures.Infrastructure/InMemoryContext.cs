﻿using EFCoreInfrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fixtures.Infrastructure
{
    public class InMemoryContext : Context
    {

        public InMemoryContext(DbContextOptions options) : base(options)
        {
        }
    }
}
