﻿using Infrastructure.Domain.Singletons.Interfaces;
using System;

namespace Fixtures.Infrastructure.Singletons
{
    public class InMemoryAppEnvironmentSingleton : IEnvironmentSingleton
    {
        public string SQLServer => Environment.GetEnvironmentVariable("ServidorSQLServer", EnvironmentVariableTarget.User);
        public string SQLUser => Environment.GetEnvironmentVariable("UsuarioSQLServer", EnvironmentVariableTarget.User);
        public string SQLPassword => Environment.GetEnvironmentVariable("SenhaSQLServer", EnvironmentVariableTarget.User);

        public string CellPassword => "Pluto";

        public string AdminPassword => "admin";

        public string ConnectionString => $"Server={SQLServer};Database=WSN;User Id={SQLUser};Password={SQLPassword};";
    }
}
