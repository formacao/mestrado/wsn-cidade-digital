﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Fixtures.Infrastructure
{
    public interface IAuthenticableClient
    {
        Task<HttpClient> GetAuthenticatedClient();
    }
}
