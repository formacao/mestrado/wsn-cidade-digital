﻿using Fixtures.Infrastructure.Singletons;
using EFCoreInfrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Infrastructure.Domain.Singletons.Interfaces;

namespace Fixtures.Infrastructure.Extensions
{
    public static class InfrastructureFixturesExtensions
    {
        public static IServiceCollection AddInfrastructureFixturesSingletons(this IServiceCollection services)
        {
            services.AddSingleton<IEnvironmentSingleton, InMemoryAppEnvironmentSingleton>();
            return services;
        }

        public static IServiceCollection AddFixturesInMemoryDatabase(this IServiceCollection services, string name)
        {
            services.AddDbContext<InMemoryContext>(options => options.UseInMemoryDatabase(name));

            services.AddScoped<Context>(serviceProvider => serviceProvider.GetRequiredService<InMemoryContext>());
            return services;
        }
    }
}
