﻿using Domain.Models;
using EFCoreInfrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fixtures.Infrastructure.Seeders
{
    public class DummyDataSeeder
    {
        public MonitoringModel InsertDummyMonitoring(Context context, int cellId, int scheduleId)
        {
            var newMonitoring = new MonitoringModel
            {
                CellId = cellId,
                MonitoringScheduleId = scheduleId,
            };

            context.MonitoringData.Add(newMonitoring);

            if (context.SaveChanges() != 1) throw new InvalidOperationException("Unable to save dummy monitoring");

            return newMonitoring;
        }

        public MonitoringScheduleModel InsertDummySchedule(Context context)
        {
            var dummySchedule = new MonitoringScheduleModel
            {
                MonitoringDateTime = DateTime.UtcNow
            };

            context.MonitoringSchedules.Add(dummySchedule);
            if (context.SaveChanges() != 1) throw new InvalidOperationException("Unable to save dummy schedule");

            return dummySchedule;
        }

        public CellModel InsertDummyCell(Context context)
        {
            var newCell = new CellModel
            {
                Name = "Dummy Cell",
                Address = "Dummy Address",
            };

            context.Cells.Add(newCell);

            if (context.SaveChanges() != 1) throw new InvalidOperationException("Unable to save dummy cell");
            return newCell;
        }

    }
}
