﻿using Fixtures.Infrastructure;
using Fixtures.Infrastructure.Extensions;
using EFCoreInfrastructure.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tests.WSN.Cells.Extensions;
using WSN.Cells;
using WSN.Cells.Extensions;

namespace Tests.WSN.Cells.Fixtures
{
    public class FakeStartup : Startup
    {
        public FakeStartup(IConfiguration configuration) : base(configuration)
        {
        }

        public override void ConfigureInfrastructure(IServiceCollection services)
        {
            services
                .AddInfrastructureFixturesSingletons()
                .AddFixturesInMemoryDatabase(Guid.NewGuid().ToString())
                .AddUnitOfWork()
                .ConfigureDatabase()
                .AddRepositories();
        }

        public override void ConfigureCellApplication(IServiceCollection services)
        {
            services
                .AddCellApplicationTestSingletons()
                .AddCellApplicationMappers()
                .AddCellApplicationServices()
                .AddCellApplicationAuthenticationConfigurations();
        }
    }
}
