﻿using Fixtures.Infrastructure;
using Infrastructure.Domain.Singletons;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using WSN.Cells;
using WSN.Cells.RequestModels;
using WSN.Cells.ResponseModels;

namespace Tests.WSN.Cells.Fixtures
{
    public class CellAppFactory : AppFactory<Program, FakeStartup>, IAuthenticableClient
    {
        public async Task<HttpClient> GetAuthenticatedClient()
        {
            var client = CreateClient();
            var json = new LoginRequestModel
            {
                User = UsersSingleton.Cell.ToLower(),
                Password = _environment.CellPassword
            };

            var serializedJson = JsonSerializer.Serialize(json);
            var contentJson = new StringContent(serializedJson, Encoding.UTF8, "application/json");

            var result = await client.PostAsync("/api/login/", contentJson);
            var stringResult = await result.Content.ReadAsStringAsync();
            var desserializedJSon = JsonSerializer.Deserialize<TokenResponseModel>(stringResult);

            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {desserializedJSon.Token}");

            return client;
        }
    }
}
