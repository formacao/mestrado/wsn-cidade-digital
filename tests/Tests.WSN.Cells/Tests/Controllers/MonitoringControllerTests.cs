﻿using Domain.Enums;
using Domain.Models;
using Fixtures.Infrastructure.Seeders;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Tests.WSN.Cells.Fixtures;
using WSN.Cells.RequestModels;
using Xunit;

namespace Tests.WSN.Cells.Tests.Controllers
{
    public class MonitoringControllerTests : IClassFixture<CellAppFactory>, IClassFixture<DummyDataSeeder>
    {
        private readonly CellAppFactory _app;
        private readonly DummyDataSeeder _seeder;

        private readonly CellModel _dummyCell;
        private readonly MonitoringScheduleModel _dummySchedule;
        private readonly MonitoringModel _dummyMonitoring;

        public MonitoringControllerTests(CellAppFactory app, DummyDataSeeder seeder)
        {
            _app = app;
            _seeder = seeder;

            using var context = _app.GetContext();

            _dummyCell = _seeder.InsertDummyCell(context);
            _dummySchedule = _seeder.InsertDummySchedule(context);
            _dummyMonitoring = _seeder.InsertDummyMonitoring(context, _dummyCell.Id, _dummySchedule.Id);

            _dummySchedule.Monitorings = new MonitoringModel[] { _dummyMonitoring };
        }

        [Fact]
        public async Task LastShouldReturn404GivenNonExistingCell()
        {
            var client = await _app.GetAuthenticatedClient();
            var result = await client.GetAsync("/api/monitoring/8000");

            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task LastShouldReturnTrueGivenCellThatAlreadySentData()
        {
            var client = await _app.GetAuthenticatedClient();
            var result = await client.GetAsync($"/api/monitoring/{_dummyCell.Id}");

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task SendMonitoringShouldReturn404GivenNonExistingCell()
        {
            var client = await _app.GetAuthenticatedClient();
            var result = await SendMonitoring(client, 8000, _dummySchedule.Id);

            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task SendMonitoringShouldReturn404GivenNonExistingSchedule()
        {
            var client = await _app.GetAuthenticatedClient();
            var result = await SendMonitoring(client, _dummyCell.Id, 8000);

            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task SendMonitoringShouldReturn201GivenValidMonitoring()
        {
            var newCell = _seeder.InsertDummyCell(_app.GetContext());
            var client = await _app.GetAuthenticatedClient();
            var result = await SendMonitoring(client, newCell.Id, _dummySchedule.Id);

            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
        }

        [Fact]
        public async Task SendMonitoringWithHighTemperatureAndLowHumidityShouldStoreTheAlerts()
        {
            using var context = _app.GetContext();
            var newCell = _seeder.InsertDummyCell(context);
            context.Dispose();
            var client = await _app.GetAuthenticatedClient();

            var request = new SendMonitoringRequestModel
            {
                CellId = newCell.Id,
                ScheduleId = _dummySchedule.Id,
                Temperature = 38,
                Humidity = 12,
                UvRadiation = 3,
                Smoke = 400,
                ToxicGas = 400,
                Fire = 0.2f
            };

            var serialziedJSon = JsonSerializer.Serialize(request);
            var stringContent = new StringContent(serialziedJSon, Encoding.UTF8, "application/json");
            var result = await client.PostAsync("/api/monitoring", stringContent);
            var stringResult = await result.Content.ReadAsStringAsync();

            using var assertContext = _app.GetContext();
            var monitoring = assertContext.MonitoringData.Where(m => m.CellId == newCell.Id).Include(m => m.Alerts).Last();
            var alertsTypes = monitoring.Alerts.Select(a => (AlertTypesEnum)a.AlertTypeId);

            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
            Assert.Equal(2, alertsTypes.Count());
            Assert.Contains(AlertTypesEnum.HighTemperature, alertsTypes);
            Assert.Contains(AlertTypesEnum.LowHumidity, alertsTypes);
        }

        private async Task<HttpResponseMessage> SendMonitoring(HttpClient client, int cellId, int scheduleId)
        {
            var request = new SendMonitoringRequestModel
            {
                CellId = cellId,
                ScheduleId = scheduleId
            };
            var serializedJson = JsonSerializer.Serialize(request);
            var stringContent = new StringContent(serializedJson, Encoding.UTF8, "application/json");

            return await client.PostAsync("/api/monitoring", stringContent);
        }
    }
}
