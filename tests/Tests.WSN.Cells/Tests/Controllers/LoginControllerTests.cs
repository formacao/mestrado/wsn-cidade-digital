﻿using Fixtures.Infrastructure.Singletons;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Tests.WSN.Cells.Fixtures;
using WSN.Cells.RequestModels;
using WSN.Cells.ResponseModels;
using Xunit;
using Infrastructure.Domain.Singletons.Interfaces;
using Infrastructure.Domain.Singletons;

namespace Tests.WSN.Cells.Tests.Controllers
{
    public class LoginControllerTests : IClassFixture<CellAppFactory>
    {
        private readonly CellAppFactory _app;
        private readonly IEnvironmentSingleton _environment;

        public LoginControllerTests(CellAppFactory app)
        {
            _app = app;
            _environment = new InMemoryAppEnvironmentSingleton();
        }

        [Fact]
        public async Task LoginShouldBeSuccesful()
        {
            var client = _app.CreateClient();
            var result = await GetLoginResult(client, UsersSingleton.Cell.ToLower(), _environment.CellPassword);
            result.EnsureSuccessStatusCode();

            var response = await result.Content.ReadAsStringAsync();
            var responseModel = JsonSerializer.Deserialize<TokenResponseModel>(response);

            Assert.NotNull(responseModel.Token);
        }

        [Fact]
        public async Task LoginShouldReturnNotFound()
        {
            var client = _app.CreateClient();
            var result = await GetLoginResult(client, UsersSingleton.Cell.ToLower(), "wrongPassword");

            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        private async Task<HttpResponseMessage> GetLoginResult(HttpClient client, string user, string password)
        {
            var request = new LoginRequestModel { User = user, Password = password };
            var stringRequest = JsonSerializer.Serialize(request);
            var contentRequest = new StringContent(stringRequest, Encoding.UTF8, "application/json");

            return await client.PostAsync("/api/login", contentRequest);
        }
    }
}
