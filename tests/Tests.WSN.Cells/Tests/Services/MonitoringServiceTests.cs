﻿using Domain.Models;
using Fixtures.Infrastructure;
using Fixtures.Infrastructure.Seeders;
using EFCoreInfrastructure;
using EFCoreInfrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Threading.Tasks;
using WSN.Cells.Mappers;
using WSN.Cells.RequestModels;
using WSN.Cells.Services;
using WSN.Cells.Services.Interfaces;
using Xunit;
using Infrastructure.Domain.Repositories.Interfaces;

namespace Tests.WSN.Cells.Tests.Services
{
    public class MonitoringServiceTests : IClassFixture<DummyDataSeeder>
    {
        private readonly string _database;

        private readonly CellModel _dummyCell;
        private readonly MonitoringScheduleModel _dummySchedule;
        private readonly MonitoringModel _dummyMonitoring;
        private readonly DummyDataSeeder _seeder;

        public MonitoringServiceTests(DummyDataSeeder seeder)
        {
            _seeder = seeder;
            _database = Guid.NewGuid().ToString();

            var context = GetContext();

            _dummyCell = _seeder.InsertDummyCell(context);
            _dummySchedule = _seeder.InsertDummySchedule(context);
            _dummyMonitoring = _seeder.InsertDummyMonitoring(context, _dummyCell.Id, _dummySchedule.Id);

            _dummySchedule.Monitorings = new MonitoringModel[] { _dummyMonitoring };
        }

        [Fact]
        public async Task GetLastScheduleAsyncShouldReturnDataSentAsTrue()
        {
            var context = GetContext();

            var monitoringRepository = new MonitoringRepository(context, context);
            var mapper = new MonitoringMapper();
            var alertRepository = new AlertRepository(context, context);

            var monitoringService = new MonitoringService(monitoringRepository, GetDummyScheduleRepository(_dummySchedule), alertRepository, mapper);
            var lastSchedule = await monitoringService.GetLastScheduleAsync(_dummyCell.Id);

            Assert.True(lastSchedule.DataSent);
        }

        [Fact]
        public async Task GetLastScheduleAsyncShouldReturnDataSentAsFalseGivenScheduleWithoutMonitorings()
        {
            var newSchedule = _seeder.InsertDummySchedule(GetContext());
            var context = GetContext();

            var monitoringRepository = new MonitoringRepository(context,context);
            var mapper = new MonitoringMapper();
            var alertRepository = new AlertRepository(context, context);

            var monitoringService = new MonitoringService(monitoringRepository, GetDummyScheduleRepository(newSchedule), alertRepository, mapper);
            var lastSchedule = await monitoringService.GetLastScheduleAsync(_dummyCell.Id);

            Assert.False(lastSchedule.DataSent);
        }

        [Fact]
        public async Task GetLastScheduleAsyncShouldReturnDataSenteAsFalseGivenCellWithoutMonitorings()
        {
            var context = GetContext();

            var monitoringRepository = new MonitoringRepository(context, context);
            var mapper = new MonitoringMapper();
            var alertRepository = new AlertRepository(context, context);

            var monitoringService = new MonitoringService(monitoringRepository, GetDummyScheduleRepository(_dummySchedule),alertRepository, mapper);
            var newCell = _seeder.InsertDummyCell(context);
            var lastSchedule = await monitoringService.GetLastScheduleAsync(newCell.Id);

            Assert.False(lastSchedule.DataSent);
        }

        [Fact]
        public async Task PostAsyncShouldReturnFalseGivenAlreadySentMonitoring()
        {
            var service = GetDefaultService();

            var request = new SendMonitoringRequestModel
            {
                CellId = _dummyCell.Id,
                ScheduleId = _dummySchedule.Id
            };

            var result = await service.PostAsync(request);

            Assert.False(result);
        }

        [Fact]
        public async Task PostAsyncShouldReturnTrueGivenScheduleWithoutMonitoring()
        {
            var service = GetDefaultService();
            var newSchedule = _seeder.InsertDummySchedule(GetContext());

            var request = new SendMonitoringRequestModel
            {
                CellId = _dummyCell.Id,
                ScheduleId = newSchedule.Id
            };

            var result = await service.PostAsync(request);

            Assert.True(result);
        }

        [Fact]
        public async Task PostAsyncShouldReturnTrueGivenCellThatDidntSendMonitoring()
        {
            var service = GetDefaultService();
            var newCell = _seeder.InsertDummyCell(GetContext());

            var request = new SendMonitoringRequestModel
            {
                CellId = newCell.Id,
                ScheduleId = _dummySchedule.Id
            };

            var result = await service.PostAsync(request);

            Assert.True(result);
        }

        private IMonitoringService GetDefaultService()
        {
            var context = GetContext();
            var monitoringRepository = new MonitoringRepository(context, context);
            var monitoringScheduleRepository = new MonitoringScheduleRepository(context, context);
            var alertRepository = new AlertRepository(context, context);

            return new MonitoringService(monitoringRepository, monitoringScheduleRepository, alertRepository, new MonitoringMapper());
        }

        private Context GetContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder().UseInMemoryDatabase(_database);
            return new InMemoryContext(optionsBuilder.Options);
        }


        private IMonitoringScheduleRepository GetDummyScheduleRepository(MonitoringScheduleModel desiredSchedule)
        {
            var monitoringScheduleMoq = new Mock<IMonitoringScheduleRepository>();
            monitoringScheduleMoq.Setup(ms => ms.GetLastAsync()).Returns(Task.FromResult(desiredSchedule));

            return monitoringScheduleMoq.Object;
        }



    }
}
