﻿using System;
using System.Collections.Generic;
using System.Text;
using WSN.Cells.Singletons.Interfaces;

namespace Tests.WSN.Cells.Singletons
{
    public class FixtureAuthenticationSingleton : IAuthenticationSingleton
    {
        public int HoursExpiration => 4;

        public string LoginKey => "Crewmate-@There-&Is-*1-(Impostor-[]AmongUs";

        public string ValidAudience => "FakeCells";

        public string ValidIssuer => "FakeCells";
    }
}
