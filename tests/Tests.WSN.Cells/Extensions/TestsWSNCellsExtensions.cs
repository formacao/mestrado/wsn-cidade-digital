﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Tests.WSN.Cells.Singletons;
using WSN.Cells.Singletons.Interfaces;

namespace Tests.WSN.Cells.Extensions
{
    public static class TestsWSNCellsExtensions
    {
        public static IServiceCollection AddCellApplicationTestSingletons(this IServiceCollection services)
        {
            services.AddSingleton<IAuthenticationSingleton, FixtureAuthenticationSingleton>();

            return services;
        }
    }
}
