$projectsToPublish=@(
        "WSN.Admin"
        "WSN.Cells"
        "WSN.User.API"
        "WSN.User.SPA"        
)

function GetPublishDir($project) {
    return "publish/" + $project
}

function GetProjectPath($project) {
    return "src/" + $project + "/"
}

function GetProjectFullPath($project) {
    return $(GetProjectPath $project) +  $project + ".csproj"
}
    
function GetDockerImageName($project) {
    return $project.ToLower().Replace(".","-")
}

function GetFullDockerImageName($containerRegistry, $project, $tag) {
    return  $containerRegistry + ":" + $(GetDockerImageName $project)
}

function GetDockerFilePath($project) {
    return $(GetProjectPath $project) + "Dockerfile"
}


function Publish() {
    foreach($project in $projectsToPublish) {
        dotnet publish --configuration Release -o $(GetPublishDir $project) $(GetProjectFullPath $project)
    }
}

function Dockerize() {
    foreach($project in $projectsToPublish) {
        $imageName = $(GetFullDockerImageName "registry.gitlab.com/formacao/mestrado/wsn-cidade-digital" $project)
        
           docker build -f  $(GetDockerFilePath($project)) -t $imageName .
           docker push $imageName
    }
}

